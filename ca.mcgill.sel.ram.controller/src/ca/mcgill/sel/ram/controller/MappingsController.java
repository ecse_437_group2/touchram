package ca.mcgill.sel.ram.controller;

import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.command.DeleteCommand;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.core.COREMapping;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.ram.AttributeMapping;
import ca.mcgill.sel.ram.ClassifierMapping;
import ca.mcgill.sel.ram.EnumLiteralMapping;
import ca.mcgill.sel.ram.EnumMapping;
import ca.mcgill.sel.ram.MappableElement;
import ca.mcgill.sel.ram.OperationMapping;
import ca.mcgill.sel.ram.ParameterMapping;
import ca.mcgill.sel.ram.RamFactory;
import ca.mcgill.sel.ram.RamPackage;

/**
 * The controller for mappings in RAM.
 * 
 * @author mschoettle
 */
public class MappingsController extends BaseController {
    
    /**
     * Creates a new instance of {@link MappingsController}.
     */
    protected MappingsController() {
        // Prevent anyone outside this package to instantiate.
    }
    
    /**
     * Creates a new mapping between classifiers.
     * 
     * @param modelComposition the {@link COREModelComposition} the mapping should be added to
     * @return {@link ClassifierMapping}
     */
    public ClassifierMapping createClassifierMapping(COREModelComposition modelComposition) {
        // create mapping
        ClassifierMapping mapping = RamFactory.eINSTANCE.createClassifierMapping();
        
        doAdd(modelComposition, CorePackage.Literals.CORE_MODEL_COMPOSITION__COMPOSITIONS, mapping);
        
        return mapping;
    }
    
    /**
     * Creates a new mapping between enums.
     * 
     * @param modelComposition the {@link COREModelComposition} the mapping should be added to
     * @return {@link EnumMapping}
     */
    public EnumMapping createEnumMapping(COREModelComposition modelComposition) {
        // create mapping
        EnumMapping mapping = RamFactory.eINSTANCE.createEnumMapping();
        
        doAdd(modelComposition, CorePackage.Literals.CORE_MODEL_COMPOSITION__COMPOSITIONS, mapping);
        
        return mapping;
    }
    
 
    /**
     * Creates a new mapping between attributes.
     * 
     * @param classifierMapping the {@link ClassifierMapping} the mapping should be added to
     * @return the newly created {@link AttributeMapping}
     */
    public AttributeMapping createAttributeMapping(ClassifierMapping classifierMapping) {
        // create mapping
        AttributeMapping mapping = RamFactory.eINSTANCE.createAttributeMapping();
        
        doAdd(classifierMapping, RamPackage.Literals.CLASSIFIER_MAPPING__ATTRIBUTE_MAPPINGS, mapping);
        
        return mapping;
    }
    
    /**
     * Creates a new mapping between enum literals.
     * 
     * @param enumMapping the {@link EnumMapping} the mapping should be added to
     * @return the newly created {@link EnumLiteralMapping}
     */
    public EnumLiteralMapping createEnumLiteralMapping(EnumMapping enumMapping) {
        // create mapping
        EnumLiteralMapping mapping = RamFactory.eINSTANCE.createEnumLiteralMapping();
        
        doAdd(enumMapping, RamPackage.Literals.ENUM_MAPPING__ENUM_LITERAL_MAPPINGS, mapping);
        
        return mapping;
    }
    
    /**
     * Creates a new mapping between operations.
     * 
     * @param classifierMapping the {@link ClassifierMapping} the mapping should be added to
     * @return the newly created {@link OperationMapping}
     */
    public OperationMapping createOperationMapping(ClassifierMapping classifierMapping) {
        // create mapping
        OperationMapping mapping = RamFactory.eINSTANCE.createOperationMapping();
        
        doAdd(classifierMapping, RamPackage.Literals.CLASSIFIER_MAPPING__OPERATION_MAPPINGS, mapping);
        
        return mapping;
    }
    
    /**
     * Creates a new mapping between parameters.
     * 
     * @param operationMapping the {@link OperationMapping} the mapping should be added to
     */
    public void createParameterMapping(OperationMapping operationMapping) {
        // create mapping
        ParameterMapping mapping = RamFactory.eINSTANCE.createParameterMapping();
        
        doAdd(operationMapping, RamPackage.Literals.OPERATION_MAPPING__PARAMETER_MAPPINGS, mapping);
    }
    
    /**
     * Deletes the given mapping.
     * 
     * @param mapping the {@link Mapping} that should be deleted
     */
    public void deleteMapping(COREMapping<?> mapping) {
        EditingDomain domain = EMFEditUtil.getEditingDomain(mapping);
        CompoundCommand command = new CompoundCommand();
        
        // Unset the to element specifically to force a notification for the derived property.
        command.append(SetCommand.create(domain, mapping, CorePackage.Literals.CORE_LINK__TO, SetCommand.UNSET_VALUE));
        command.append(DeleteCommand.create(domain, mapping));
        
        doExecute(domain, command);
    }
    
    /**
     * Sets the to element of the given mapping to the given element.
     * 
     * @param mapping the mapping
     * @param feature the feature to be set
     * @param mappableElement the element to be set as the to element
     */
    public void setToElement(COREMapping<?> mapping, EStructuralFeature feature, MappableElement mappableElement) {
        doSet(mapping, feature, mappableElement);
    }
    
    /**
     * Sets the from element of the given mapping to the given element.
     * 
     * @param mapping the mapping
     * @param feature the feature to be set
     * @param mappableElement the element to be set as the from element
     */
    public void setFromElement(COREMapping<?> mapping, EStructuralFeature feature, MappableElement mappableElement) {
        // uses the same functionality right now
        setToElement(mapping, feature, mappableElement);
    }
    
}
