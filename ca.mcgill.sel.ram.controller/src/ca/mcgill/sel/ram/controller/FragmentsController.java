package ca.mcgill.sel.ram.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.edit.command.MoveCommand;
import org.eclipse.emf.edit.command.RemoveCommand;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.CORENamedElement;
import ca.mcgill.sel.core.util.COREModelUtil;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.AssignmentStatement;
import ca.mcgill.sel.ram.Attribute;
import ca.mcgill.sel.ram.Class;
import ca.mcgill.sel.ram.Classifier;
import ca.mcgill.sel.ram.CombinedFragment;
import ca.mcgill.sel.ram.ExecutionStatement;
import ca.mcgill.sel.ram.FragmentContainer;
import ca.mcgill.sel.ram.Gate;
import ca.mcgill.sel.ram.Interaction;
import ca.mcgill.sel.ram.InteractionFragment;
import ca.mcgill.sel.ram.InteractionOperand;
import ca.mcgill.sel.ram.InteractionOperatorKind;
import ca.mcgill.sel.ram.Lifeline;
import ca.mcgill.sel.ram.Message;
import ca.mcgill.sel.ram.MessageEnd;
import ca.mcgill.sel.ram.MessageOccurrenceSpecification;
import ca.mcgill.sel.ram.MessageSort;
import ca.mcgill.sel.ram.ObjectType;
import ca.mcgill.sel.ram.OpaqueExpression;
import ca.mcgill.sel.ram.PrimitiveType;
import ca.mcgill.sel.ram.RamFactory;
import ca.mcgill.sel.ram.RamPackage;
import ca.mcgill.sel.ram.Reference;
import ca.mcgill.sel.ram.StructuralFeature;
import ca.mcgill.sel.ram.TypedElement;
import ca.mcgill.sel.ram.provider.util.RAMReferenceUtil;
import ca.mcgill.sel.ram.util.MessageViewUtil;

/**
 * The controller for {@link InteractionFragment}s.
 *
 * @author mschoettle
 */
public class FragmentsController extends BaseController {

    /**
     * Creates a new instance of {@link FragmentsController}.
     */
    protected FragmentsController() {
        // Prevent anyone outside this package to instantiate.
    }

    /**
     * Removes the given interaction fragment from its container and removes the fragment from the list of
     * covered lifelines as well.
     * Note that if a {@link MessageOccurrenceSpecification} is passed, it needs to be the send event of the message.
     *
     * @param fragment the {@link InteractionFragment} to remove
     */
    public void removeInteractionFragment(InteractionFragment fragment) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(fragment);

        CompoundCommand compoundCommand = createRemoveInteractionFragmentCommand(editingDomain, fragment);

        doExecute(editingDomain, compoundCommand);
    }

    /**
     * Removes the given combined fragment, but keeps its content.
     * I.e., the content of all its operand is moved to the container
     * and position where the combined fragment is located at.
     * 
     * @param combinedFragment the {@link CombinedFragment} to remove
     */
    public void removeCombinedFragmentAndKeepContents(CombinedFragment combinedFragment) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(combinedFragment);

        List<InteractionFragment> fragments = new ArrayList<>();

        for (InteractionOperand operand : combinedFragment.getOperands()) {
            fragments.addAll(operand.getFragments());
        }

        FragmentContainer newContainer = combinedFragment.getContainer();
        int addIndex = newContainer.getFragments().indexOf(combinedFragment);

        CompoundCommand compoundCommand = new CompoundCommand();

        compoundCommand.append(RemoveCommand.create(editingDomain, fragments));
        compoundCommand.append(AddCommand.create(editingDomain, newContainer,
                RamPackage.Literals.FRAGMENT_CONTAINER__FRAGMENTS, fragments, addIndex));

        compoundCommand.append(RemoveCommand.create(editingDomain, combinedFragment));
        compoundCommand.append(RemoveCommand.create(editingDomain, combinedFragment,
                RamPackage.Literals.INTERACTION_FRAGMENT__COVERED, combinedFragment.getCovered()));

        doExecute(editingDomain, compoundCommand);
    }

    /**
     * Creates the appropriate removal commands to remove the given interaction fragment.
     * This also includes the removal of the fragment from the covered list of all covered lifelines.
     * If the fragment is a combined fragment, all messages related to the operands need to be removed.
     * If the fragment is part of a message, the related messages is removed as well.
     * This includes subsequent messages and fragments in case of nested behaviour.
     * Furthermore, unused temporary properties are deleted as well as lifelines that would be unused.
     *
     * @param editingDomain the {@link EditingDomain} to use for executing commands
     * @param fragment the {@link InteractionFragment} to delete
     * @return the commands to appropriately remove an interaction fragment
     */
    private CompoundCommand createRemoveInteractionFragmentCommand(EditingDomain editingDomain,
            InteractionFragment fragment) {
        CompoundCommand compoundCommand = new CompoundCommand();

        if (fragment instanceof CombinedFragment) {
            CombinedFragment combinedFragment = (CombinedFragment) fragment;

            // Delete from the bottom up.
            for (int index = combinedFragment.getOperands().size() - 1; index >= 0; index--) {
                InteractionOperand operand = combinedFragment.getOperands().get(index);

                compoundCommand.append(createRemoveOperandContentsCommand(editingDomain, operand));
            }

            /**
             * The lifelines need to be removed from the combined fragment first,
             * because empty lifelines might have not been removed previously
             * (these are not part of the deleted lifelines).
             * This is necessary, because empty lifelines (within a combined fragment)
             * are not removed from the combined fragment automatically.
             * It is difficult to distinguish between the target and other lifelines to be able to do that.
             *
             * Avoid duplicate remove commands for "covered" for already deleted lifelines.
             * It is possible that the covered link of the combined fragment
             * was removed when deleting an empty lifeline.
             */
            List<InteractionFragment> fragmentsToDelete = getAffectedFragments(Collections.singletonList(fragment));
            compoundCommand.append(RemoveCommand.create(editingDomain, fragment));
            compoundCommand.append(RemoveCommand.create(editingDomain, fragment,
                    RamPackage.Literals.INTERACTION_FRAGMENT__COVERED, fragment.getCovered()));
            handleUnusedCombinedFragments(editingDomain, compoundCommand, combinedFragment, fragmentsToDelete);
            
            Interaction owner = EMFModelUtil.getRootContainerOfType(combinedFragment, RamPackage.Literals.INTERACTION);
            removeUnusedLifelines(editingDomain, compoundCommand, owner, fragmentsToDelete, combinedFragment);
        } else if (fragment instanceof MessageOccurrenceSpecification) {
            FragmentContainer container = fragment.getContainer();
            Interaction owner = EMFModelUtil.getRootContainerOfType(fragment, RamPackage.Literals.INTERACTION);
            MessageOccurrenceSpecification sendEvent = (MessageOccurrenceSpecification) fragment;

            int fromIndex = container.getFragments().indexOf(sendEvent);
            int toIndex = findMessageBehaviourEnd(owner, sendEvent);

            List<InteractionFragment> messageBehaviour = container.getFragments().subList(fromIndex, toIndex + 1);
            List<InteractionFragment> fragmentsToDelete = getAffectedFragments(messageBehaviour);

            /**
             * Remove all messages belonging to the behaviour.
             */
            Set<Message> affectedMessages = getAffectedMessages(fragmentsToDelete);

            for (Message message : affectedMessages) {
                compoundCommand.append(RemoveCommand.create(editingDomain, message));
            }

            /**
             * Remove unused temporary properties.
             */
            removeUnusedTemporaryProperties(editingDomain, compoundCommand, fragmentsToDelete);

            /**
             * Remove fragments and ensure they are not covering the lifeline(s) anymore.
             * Remove the covered by separately after in order to ensure that when this is undone,
             * by the time the message ends are added, their covered by information is set.
             * This is required for the isSelfMessage operation to work.
             */
            for (InteractionFragment currentFragment : fragmentsToDelete) {
                compoundCommand.append(RemoveCommand.create(editingDomain, currentFragment));
            }

            for (InteractionFragment currentFragment : fragmentsToDelete) {
                compoundCommand.append(RemoveCommand.create(editingDomain, currentFragment,
                        RamPackage.Literals.INTERACTION_FRAGMENT__COVERED, currentFragment.getCovered()));
            }

            /**
             * If there the behaviour is located within a combined fragment, ensure that lifelines are uncovered,
             * if they are unused as a result after the deletion.
             */
            CombinedFragment outerCombinedFragment = null;

            if (container != owner) {
                outerCombinedFragment = (CombinedFragment) container.eContainer();
                handleUnusedCombinedFragments(editingDomain, compoundCommand, outerCombinedFragment, fragmentsToDelete);
            }

            removeUnusedLifelines(editingDomain, compoundCommand, owner, fragmentsToDelete, outerCombinedFragment);
        } else {
            /**
             * Check whether a temporary property needs to be removed for AssignmentStatements.
             * See issue #403.
             */
            if (fragment instanceof AssignmentStatement) {
                AssignmentStatement assignment = (AssignmentStatement) fragment;
                StructuralFeature structuralFeature = assignment.getAssignTo();

                appendRemoveTemporaryPropertyCommand(editingDomain, compoundCommand, assignment, structuralFeature,
                        Collections.singletonList(fragment));
            }

            compoundCommand.append(RemoveCommand.create(editingDomain, fragment));
            compoundCommand.append(RemoveCommand.create(editingDomain, fragment,
                    RamPackage.Literals.INTERACTION_FRAGMENT__COVERED, fragment.getCovered()));
        }

        return compoundCommand;
    }

    /**
     * Creates an execution statement at the given index.
     * The execution statement is using an {@link OpaqueExpression} with a default language as the specification.
     *
     * @param owner the container for the execution statement
     * @param lifeline the lifeline that the execution statement covers/is placed on
     * @param addAtIndex the index at which the statement should be added to inside the containers fragments
     */
    public void createExecutionStatement(FragmentContainer owner, Lifeline lifeline, int addAtIndex) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(owner);

        ExecutionStatement executionStatement = RamFactory.eINSTANCE.createExecutionStatement();
        OpaqueExpression specification = MessageViewUtil.createOpaqueExpression();
        executionStatement.setSpecification(specification);

        CompoundCommand compoundCommand = new CompoundCommand();

        compoundCommand.append(AddCommand.create(editingDomain, executionStatement,
                RamPackage.Literals.INTERACTION_FRAGMENT__COVERED, lifeline));
        compoundCommand.append(AddCommand.create(editingDomain, owner,
                RamPackage.Literals.FRAGMENT_CONTAINER__FRAGMENTS, executionStatement, addAtIndex));

        doExecute(editingDomain, compoundCommand);
    }

    /**
     * Creates a combined fragment that covers the given lifeline and adds at the given index inside the container.
     * The combined fragment has the default {@link ca.mcgill.sel.ram.InteractionOperatorKind} "alt".
     * Also, each operand's interaction constraint is an {@link OpaqueExpression} with a default language.
     *
     * @param owner the container for the combined fragment
     * @param lifeline the lifeline that the combined fragment covers/is placed on
     * @param addAtIndex the index at which the combined fragment should be added to inside the containers fragments
     */
    public void createCombinedFragment(FragmentContainer owner, Lifeline lifeline, int addAtIndex) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(owner);

        CombinedFragment combinedFragment = RamFactory.eINSTANCE.createCombinedFragment();
        combinedFragment.setInteractionOperator(InteractionOperatorKind.OPT);

        InteractionOperand operand = createInteractionOperand();

        combinedFragment.getOperands().add(operand);

        CompoundCommand compoundCommand = new CompoundCommand();

        compoundCommand.append(AddCommand.create(editingDomain, combinedFragment,
                RamPackage.Literals.INTERACTION_FRAGMENT__COVERED, lifeline));
        compoundCommand.append(AddCommand.create(editingDomain, owner,
                RamPackage.Literals.FRAGMENT_CONTAINER__FRAGMENTS, combinedFragment, addAtIndex));

        doExecute(editingDomain, compoundCommand);
    }

    /**
     * Creates a new empty operand with a constraint that is a {@link OpaqueExpression} with a default language.
     *
     * @param combinedFragment the {@link CombinedFragment} that the new operand should be added to
     * @param index the index at which to add the operand to
     */
    public void createInteractionOperand(CombinedFragment combinedFragment, int index) {
        InteractionOperand operand = createInteractionOperand();

        doAdd(combinedFragment, RamPackage.Literals.COMBINED_FRAGMENT__OPERANDS, operand, index);
    }

    /**
     * Creates a new empty operand with an empty constraint as an {@link OpaqueExpression}.
     *
     * @return a new empty {@link InteractionOperand}
     */
    private InteractionOperand createInteractionOperand() {
        InteractionOperand operand = RamFactory.eINSTANCE.createInteractionOperand();
        OpaqueExpression constraint = MessageViewUtil.createOpaqueExpression();
        operand.setInteractionConstraint(constraint);
        return operand;
    }

    /**
     * Removes the given operand and its contents.
     * The contents are messages and their events or other {@link InteractionFragment}s.
     *
     * @param operand the operand to remove
     */
    public void removeInteractionOperand(InteractionOperand operand) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(operand);

        CompoundCommand compoundCommand = new CompoundCommand();
        
        // Removal of operand happens inside createRemoveOperandContentsCommand,
        // because it needs to occur before the covered by is removed from all fragments.
        compoundCommand.append(createRemoveOperandContentsCommand(editingDomain, operand));

        /**
         * Handle covered combined fragments and unused lifelines.
         */
        CombinedFragment owner = (CombinedFragment) operand.eContainer();
        handleUnusedCombinedFragments(editingDomain, compoundCommand, owner, operand.getFragments());
        Interaction interaction = EMFModelUtil.getRootContainerOfType(owner, RamPackage.Literals.INTERACTION);
        removeUnusedLifelines(editingDomain, compoundCommand, interaction, operand.getFragments(), owner);

        doExecute(editingDomain, compoundCommand);
    }

    /**
     * Ensures that all unused temporary properties are deleted by adding the appropriate remove command.
     * A temporary property is unused if as a result of deleting the given fragments,
     * there is no other assignment for this property.
     * 
     * @param editingDomain the {@link EditingDomain}
     * @param compoundCommand the {@link CompoundCommand} to append the command to
     * @param fragmentsToDelete the list of fragments that will be deleted.
     */
    private static void removeUnusedTemporaryProperties(EditingDomain editingDomain, CompoundCommand compoundCommand,
            List<InteractionFragment> fragmentsToDelete) {
        /**
         * Collect all affected properties and ensure that the same is not added twice.
         * This is necessary to not create a RemoveCommand multiple times for the same property.
         * The fragment (value) might be replaced by another one, however,
         * this is not a problem since only one is required when checking.
         */
        Map<StructuralFeature, InteractionFragment> properties = new HashMap<>();

        for (InteractionFragment fragment : fragmentsToDelete) {
            if (fragment instanceof MessageOccurrenceSpecification) {
                MessageOccurrenceSpecification messageEnd = (MessageOccurrenceSpecification) fragment;

                if (messageEnd.getMessage().getSendEvent() == messageEnd) {
                    Message message = messageEnd.getMessage();

                    // Also delete temporary properties that were used as assign to.
                    // This assumes that there were created just for this purpose.
                    StructuralFeature structuralFeature = message.getAssignTo();
                    properties.put(structuralFeature, messageEnd);

                    // Also delete a gate if it is a reply message.
                    if (message.getMessageSort() == MessageSort.REPLY && message.getReceiveEvent() instanceof Gate) {
                        compoundCommand.append(RemoveCommand.create(editingDomain, message.getReceiveEvent()));
                    }
                }
            } else if (fragment instanceof AssignmentStatement) {
                /**
                 * Check whether a temporary property needs to be removed for AssignmentStatements.
                 * See issue #403.
                 */
                AssignmentStatement assignment = (AssignmentStatement) fragment;
                StructuralFeature structuralFeature = assignment.getAssignTo();

                properties.put(structuralFeature, assignment);
            }
        }

        for (Entry<StructuralFeature, InteractionFragment> entry : properties.entrySet()) {
            StructuralFeature structuralFeature = entry.getKey();
            InteractionFragment user = entry.getValue();

            appendRemoveTemporaryPropertyCommand(editingDomain, compoundCommand, user, structuralFeature,
                    fragmentsToDelete);
        }
    }

    /**
     * Returns the set of affected messages.
     * I.e., a message is considered affected, if a given fragment is part of a message.
     * 
     * @param fragments a list of all fragments to consider
     * @return the set of affected messages
     */
    private static Set<Message> getAffectedMessages(List<InteractionFragment> fragments) {
        Set<Message> result = new HashSet<>();

        for (InteractionFragment fragment : fragments) {
            if (fragment instanceof MessageOccurrenceSpecification) {
                MessageOccurrenceSpecification messageEnd = (MessageOccurrenceSpecification) fragment;
                result.add(messageEnd.getMessage());
            }
        }

        return result;
    }

    /**
     * Returns the list of affected fragments.
     * I.e., a fragment is affected, if itself or its container is part of the given list of fragments to delete.
     * For example, for a combined fragment, all fragments located within its operands will also be deleted
     * and are therefore considered as affected.
     * 
     * @param fragmentsToDelete the list of fragments to delete
     * @return the list of all affected fragments
     */
    private List<InteractionFragment> getAffectedFragments(List<InteractionFragment> fragmentsToDelete) {
        List<InteractionFragment> fragments = new ArrayList<>();

        for (InteractionFragment fragment : fragmentsToDelete) {
            if (fragment instanceof CombinedFragment) {
                CombinedFragment combinedFragment = (CombinedFragment) fragment;

                for (InteractionOperand operand : combinedFragment.getOperands()) {
                    List<InteractionFragment> nestedFragments = getAffectedFragments(operand.getFragments());
                    fragments.addAll(nestedFragments);
                }
            }

            fragments.add(fragment);
        }

        return fragments;
    }

    /**
     * Returns a list of all affected lifelines.
     * A lifeline is affected, if any of its property is modified by a command located
     * within the given compound command.
     * 
     * @param compoundCommand the {@link CompoundCommand} containing a collection of commands
     * @return the list of affected lifelines.
     */
    private Set<Lifeline> getAffectedLifelines(CompoundCommand compoundCommand) {
        Set<Lifeline> result = new HashSet<>();

        for (Command command : compoundCommand.getCommandList()) {
            if (command.getClass() == RemoveCommand.class) {
                RemoveCommand removeCommand = (RemoveCommand) command;
                if (removeCommand.getFeature() == RamPackage.Literals.INTERACTION_FRAGMENT__COVERED) {
                    @SuppressWarnings("unchecked")
                    Collection<Lifeline> lifelines = (Collection<Lifeline>) removeCommand.getCollection();
                    result.addAll(lifelines);
                }
            } else if (command.getClass() == CompoundCommand.class) {
                result.addAll(getAffectedLifelines((CompoundCommand) command));
            }
        }

        return result;
    }

    /**
     * Creates the commands to remove the contents of the given operand.
     * The contents are messages and their events or other {@link InteractionFragment}s.
     * Note that if the operand has no commands, a non-executable command will be returned.
     *
     * @param editingDomain the {@link EditingDomain} to use for executing commands
     * @param operand the {@link InteractionOperand} to remove
     * @return the commands to remove the contents of the operand
     */
    private CompoundCommand createRemoveOperandContentsCommand(EditingDomain editingDomain,
            InteractionOperand operand) {
        CompoundCommand compoundCommand = new CompoundCommand();

        List<InteractionFragment> fragmentsToDelete = getAffectedFragments(operand.getFragments());

        /**
         * Remove messages whose events will be deleted.
         */
        Set<Message> affectedMessages = getAffectedMessages(fragmentsToDelete);

        for (Message message : affectedMessages) {
            compoundCommand.append(RemoveCommand.create(editingDomain, message));
        }

        /**
         * Remove temporary properties who will be unused.
         */
        removeUnusedTemporaryProperties(editingDomain, compoundCommand, fragmentsToDelete);

        compoundCommand.append(RemoveCommand.create(editingDomain, operand));

        /**
         * Ensure all affected lifelines are not covered by the deleted fragments.
         */
        for (InteractionFragment fragment : fragmentsToDelete) {
            compoundCommand.append(RemoveCommand.create(editingDomain, fragment,
                    RamPackage.Literals.INTERACTION_FRAGMENT__COVERED, fragment.getCovered()));
        }

        return compoundCommand;
    }

    /**
     * Removes all combined fragments from covered lifelines if the combined fragment will not cover the lifeline
     * anymore as a result after command execution (removing all of the given fragments).
     * A lifeline is not covered anymore if there are no fragments on the lifeline that are part of a combined fragment.
     * 
     * @param editingDomain the {@link EditingDomain}
     * @param compoundCommand the {@link CompoundCommand} that commands should be appended to
     * @param combinedFragment the {@link CombinedFragment} that is deleted itself or contains fragments to be deleted
     * @param fragmentsToDelete the list of fragments to delete
     */
    private void handleUnusedCombinedFragments(EditingDomain editingDomain, CompoundCommand compoundCommand,
            CombinedFragment combinedFragment, List<InteractionFragment> fragmentsToDelete) {
        Interaction owner = EMFModelUtil.getRootContainerOfType(combinedFragment, RamPackage.Literals.INTERACTION);
        List<CombinedFragment> combinedFragments = getCoveredCombinedFragments(owner, combinedFragment);

        for (CombinedFragment currentCombinedFragment : combinedFragments) {
            // Assume that the first lifeline is the one that stuff is happening from.
            Lifeline firstLifeline = currentCombinedFragment.getCovered().get(0);

            if (!fragmentsToDelete.contains(currentCombinedFragment)) {
                for (Lifeline lifeline : currentCombinedFragment.getCovered()) {
                    if (lifeline != firstLifeline
                            && isLifelineUnusedInCombinedFragment(lifeline, currentCombinedFragment, combinedFragments,
                                    fragmentsToDelete)) {
                        compoundCommand.append(RemoveCommand.create(editingDomain, currentCombinedFragment,
                                RamPackage.Literals.INTERACTION_FRAGMENT__COVERED, lifeline));
                    }
                }
            }
        }
    }

    /**
     * Returns whether the given lifeline is unused within the given combined fragment
     * after deleting the given list of fragments.
     * 
     * @param lifeline the {@link Lifeline} in question
     * @param combinedFragment the {@link CombinedFragment} in question
     * @param combinedFragments the list of combined fragments that contain the given combined fragment
     * @param fragmentsToDelete the fragments to delete
     * @return true, if the lifeline will be unused as a result of deleting all fragments, false otherwise
     */
    private boolean isLifelineUnusedInCombinedFragment(Lifeline lifeline, CombinedFragment combinedFragment,
            List<CombinedFragment> combinedFragments, List<InteractionFragment> fragmentsToDelete) {
        List<InteractionFragment> remainingFragments =
                getAffectedFragments(Collections.<InteractionFragment>singletonList(combinedFragment));
        remainingFragments.removeAll(fragmentsToDelete);
        remainingFragments.removeAll(combinedFragments);

        for (InteractionFragment fragment : remainingFragments) {
            if (lifeline.getCoveredBy().contains(fragment)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Removes all lifelines that would be empty (not covered by any fragments) as a result.
     * 
     * @param editingDomain the {@link EditingDomain}
     * @param compoundCommand the {@link CompoundCommand} to append commands to
     * @param owner the {@link Interaction} defining the message view
     * @param fragmentsToDelete the fragments to be deleted
     * @param combinedFragment the {@link CombinedFragment} that the fragments are located in,
     *            <code>null</code> if the fragments are located in the {@link Interaction}
     */
    private void removeUnusedLifelines(EditingDomain editingDomain, CompoundCommand compoundCommand, Interaction owner,
            List<InteractionFragment> fragmentsToDelete, CombinedFragment combinedFragment) {
        List<CombinedFragment> combinedFragments;

        if (combinedFragment != null) {
            combinedFragments = getCoveredCombinedFragments(owner, combinedFragment);
        } else {
            combinedFragments = Collections.emptyList();
        }

        List<InteractionFragment> affectedFragments = new ArrayList<>(fragmentsToDelete);
        affectedFragments.addAll(combinedFragments);

        for (Lifeline lifeline : getAffectedLifelines(compoundCommand)) {
            List<InteractionFragment> coveredFragments = new ArrayList<>(lifeline.getCoveredBy());
            coveredFragments.removeAll(affectedFragments);

            if (coveredFragments.isEmpty()) {
                Command command = createRemoveLayoutElementCommand(editingDomain, owner.eContainer(), lifeline);
                compoundCommand.append(command);
                compoundCommand.append(RemoveCommand.create(editingDomain, lifeline));
                
                // Remove static reference that will otherwise be unused.
                if (lifeline.getRepresents().eContainer() == owner) {
                    compoundCommand.append(RemoveCommand.create(editingDomain, lifeline.getRepresents()));
                }
            }
        }
    }

    /**
     * Returns a list of all combined fragments that the given combined fragment and all its container "covers".
     * The order of the retrieved list is bottom down from the given combined fragment up to its ancestors.
     *
     * @param owner the {@link Interaction} that contains everything
     * @param combinedFragment the {@link CombinedFragment} for which to retrieve all combined fragments
     * @return a list of all combined fragments from the given combined fragment up the hierarchy
     */
    private List<CombinedFragment> getCoveredCombinedFragments(Interaction owner, CombinedFragment combinedFragment) {
        List<CombinedFragment> result =
                MessageViewUtil.getCoveredCombinedFragments(owner, combinedFragment.getContainer(), true);
        result.add(0, combinedFragment);

        return result;
    }

    /**
     * Creates an assignment statement at the given index.
     * The assignment statement is using an {@link OpaqueExpression} with a default language as the value.
     * The "assignTo" is unset.
     *
     * @param owner the container for the assignment statement
     * @param lifeline the lifeline that the assignment statement covers/is placed on
     * @param addAtIndex the index at which the statement should be added to inside the containers fragments
     */
    public void createAssignmentStatement(FragmentContainer owner, Lifeline lifeline, int addAtIndex) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(owner);

        AssignmentStatement assignmentStatement = RamFactory.eINSTANCE.createAssignmentStatement();
        OpaqueExpression specification = MessageViewUtil.createOpaqueExpression();
        assignmentStatement.setValue(specification);

        CompoundCommand compoundCommand = new CompoundCommand();

        compoundCommand.append(AddCommand.create(editingDomain, assignmentStatement,
                RamPackage.Literals.INTERACTION_FRAGMENT__COVERED, lifeline));
        compoundCommand.append(AddCommand.create(editingDomain, owner,
                RamPackage.Literals.FRAGMENT_CONTAINER__FRAGMENTS, assignmentStatement, addAtIndex));

        doExecute(editingDomain, compoundCommand);
    }

    /**
     * Creates a new temporary property and sets it as the "assignTo" of the assignment statement.
     * Depending on the given type, the temporary property will be either an {@link Attribute} (primitive type),
     * or {@link Reference} (object types).
     * Also adds the temporary property to the initial message.
     * If there is an existing temporary property, it is deleted in addition, if it is not referenced anywhere else.
     *
     * @param assignmentStatement the {@link AssignmentStatement} for which the "assignTo" to set for
     * @param name the name of the temporary property
     * @param type the {@link ObjectType} of the temporary property
     */
    public void createTemporaryAssignment(AssignmentStatement assignmentStatement, String name, ObjectType type) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(assignmentStatement);
        Message initialMessage = MessageViewUtil.findInitialMessage(assignmentStatement);

        StructuralFeature temporaryProperty = createTemporaryProperty(initialMessage, name, type);

        CompoundCommand compoundCommand = new CompoundCommand();

        StructuralFeature structuralFeature = assignmentStatement.getAssignTo();

        List<InteractionFragment> affectedFragments =
                Collections.<InteractionFragment>singletonList(assignmentStatement);
        appendRemoveTemporaryPropertyCommand(editingDomain, compoundCommand, assignmentStatement, structuralFeature,
                affectedFragments);

        compoundCommand.append(AddCommand.create(editingDomain, initialMessage,
                RamPackage.Literals.MESSAGE__LOCAL_PROPERTIES, temporaryProperty));
        compoundCommand.append(SetCommand.create(editingDomain, assignmentStatement,
                RamPackage.Literals.ASSIGNMENT_STATEMENT__ASSIGN_TO, temporaryProperty));

        doExecute(editingDomain, compoundCommand);
    }

    /**
     * Creates a new temporary property and adds it to the given message.
     * Depending on the given type, the temporary property will be either an {@link Attribute} (primitive type),
     * or {@link Reference} (object types).
     *
     * @param initialMessage the message the property should be added to
     * @param name the name of the temporary property
     * @param type the {@link ObjectType} of the temporary property
     */
    public void createAndAddTemporaryProperty(Message initialMessage, String name, ObjectType type) {
        StructuralFeature temporaryProperty = createTemporaryProperty(initialMessage, name, type);

        doAdd(initialMessage, RamPackage.Literals.MESSAGE__LOCAL_PROPERTIES, temporaryProperty);
    }

    /**
     * Creates a new temporary property with the given name and type.
     * Depending on the given type, the temporary property will be either an {@link Attribute} (primitive type),
     * or {@link Reference} (object types).
     * Ensures that the given name is unique.
     * 
     * @param owner the message the temporary property is intended to be added to
     * @param name the name of the temporary property
     * @param type the type of the property
     * @return the created temporary property
     */
    protected static StructuralFeature createTemporaryProperty(Message owner, String name, ObjectType type) {
        StructuralFeature temporaryProperty = null;

        // Ensure the name is unique.
        MessageOccurrenceSpecification receiveEvent = (MessageOccurrenceSpecification) owner.getReceiveEvent();
        TypedElement coveredLifeline = receiveEvent.getCovered().get(0).getRepresents();
        Classifier classifierType = (Classifier) coveredLifeline.getType();

        Collection<CORENamedElement> existingProperties = EMFModelUtil.collectElementsOfType(
                owner,
                RamPackage.Literals.MESSAGE__LOCAL_PROPERTIES,
                RamPackage.Literals.TEMPORARY_PROPERTY);
        existingProperties.addAll(classifierType.getAssociationEnds());

        String uniqueName = COREModelUtil.createUniqueNameFromElements(name, existingProperties);

        if (type instanceof PrimitiveType) {
            Attribute attribute = RamFactory.eINSTANCE.createAttribute();
            attribute.setType(type);
            attribute.setName(uniqueName);

            temporaryProperty = attribute;
        } else {
            Reference reference = RamFactory.eINSTANCE.createReference();
            reference.setType(type);
            reference.setName(uniqueName);
            reference.setLowerBound(1);

            temporaryProperty = reference;
        }

        return temporaryProperty;
    }

    /**
     * Changes the "assignTo" of the assignment statement to the given one.
     * It might not be set yet, if it is and it is a temporary property, the existing one is deleted if it is not
     * referenced anywhere else.
     *
     * @param assignmentStatement the {@link AssignmentStatement} the "assignTo" to change for
     * @param assignTo the new "assignTo" value
     */
    public void changeAssignmentAssignTo(AssignmentStatement assignmentStatement, StructuralFeature assignTo) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(assignmentStatement);

        CompoundCommand compoundCommand = new CompoundCommand();

        StructuralFeature structuralFeature = assignmentStatement.getAssignTo();

        List<InteractionFragment> affectedFragments =
                Collections.<InteractionFragment>singletonList(assignmentStatement);
        appendRemoveTemporaryPropertyCommand(editingDomain, compoundCommand, assignmentStatement, structuralFeature,
                affectedFragments);

        /**
         * Ensure that the structural feature to which we assigned, if from another model, is "localized" 
         * as a reference.
         */
        StructuralFeature localFeature = RAMReferenceUtil.localizeElement(editingDomain, compoundCommand,
                assignmentStatement, assignTo);

        compoundCommand.append(SetCommand.create(editingDomain, assignmentStatement,
                RamPackage.Literals.ASSIGNMENT_STATEMENT__ASSIGN_TO, localFeature));

        doExecute(editingDomain, compoundCommand);
    }

    /**
     * Determines whether the given structural feature is temporary and not used elsewhere, and if so,
     * appends a command to remove it to the given compound command.
     * I.e., the structural feature needs to be contained in a message.
     * The user refers to the element referencing the structural feature in one of its features.
     * Specifically, this could be an InteractionFragment, such as AssignmentStatement.
     * However, for message ends, the message the end is part of has the reference instead.
     * 
     * In any other case, no remove command is appended.
     *
     * @param editingDomain the {@link EditingDomain}
     * @param command the {@link CompoundCommand} a remove command should be added to
     * @param user the {@link InteractionFragment} that uses the structural feature
     * @param structuralFeature the {@link StructuralFeature} to check whether it can be removed
     * @param affectedFragments the fragments that are affected by changes (are deleted or changed)
     */
    protected static void appendRemoveTemporaryPropertyCommand(EditingDomain editingDomain, CompoundCommand command,
            InteractionFragment user, StructuralFeature structuralFeature,
            List<InteractionFragment> affectedFragments) {
        // If there is an existing assign to and it is a temporary property, we have to delete it first.
        // Assume that it was created for this purpose.
        Message initialMessage = MessageViewUtil.findInitialMessage(user);

        /**
         * The structural feature could be assigned within a message
         * or another statement (it may only be used by the current one to be safe to delete).
         * In those cases, it should not be deleted.
         * See issue #403.
         */
        if (structuralFeature != null && structuralFeature.eContainer() == initialMessage) {
            /**
             * Collect all fragments and messages that reference this property.
             * If only the affected fragments are referencing it, it can be safely removed.
             */
            List<EObject> referencingFragments = EMFModelUtil.findCrossReferences(structuralFeature,
                    RamPackage.Literals.MESSAGE__ASSIGN_TO);
            referencingFragments.addAll(EMFModelUtil.findCrossReferences(structuralFeature,
                    RamPackage.Literals.ASSIGNMENT_STATEMENT__ASSIGN_TO));

            referencingFragments.removeAll(affectedFragments);
            referencingFragments.removeAll(getAffectedMessages(affectedFragments));

            if (referencingFragments.isEmpty()) {
                command.append(RemoveCommand.create(editingDomain, structuralFeature));
            }
        }
    }

    /**
     * Moves a fragment up or down by one.
     * Prevents moving, if it is the first or last fragment within the initial and reply message
     * of its contained behaviour, i.e., either the message view or nested behaviour.
     * Prevents moving out of operands.
     * 
     * @param fragment the fragment to move
     * @param up true, if the fragment should be moved up, false if moved down
     */
    public void moveFragment(InteractionFragment fragment, boolean up) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(fragment);
        Aspect aspect = EMFModelUtil.getRootContainerOfType(fragment, RamPackage.Literals.ASPECT);

        List<InteractionFragment> fragments = new ArrayList<>();
        fragments.add(fragment);

        FragmentContainer container = fragment.getContainer();
        int oldIndex = container.getFragments().indexOf(fragment);

        // Support moving of nested behaviour.
        // Find all fragments that need to be moved.
        if (fragment instanceof MessageEnd) {
            MessageEnd sendEvent = (MessageEnd) fragment;
            Message theMessage = sendEvent.getMessage();
            int toIndex = oldIndex + 1;
            aspect = EMFModelUtil.getRootContainerOfType(theMessage.getSignature(), RamPackage.Literals.ASPECT);
            
            if (!MessageViewUtil.isMessageViewDefined(aspect, theMessage.getSignature())
                    && theMessage.getSignature().eContainer() instanceof Class) {
                toIndex = findUpperBound((InteractionFragment) theMessage.getReceiveEvent()) + 1;
            }

            fragments.addAll(container.getFragments().subList(oldIndex + 1, toIndex + 1));
        }

        int lowerBound = findLowerBound(fragment);
        int upperBound = findUpperBound(fragment);
        int newIndex = oldIndex;

        if (up) {
            newIndex--;

            // If before this message, there is another message, the message ends need to be moved further.
            if (newIndex >= 0 && container.getFragments().get(newIndex) instanceof MessageEnd) {
                MessageEnd otherMessageEnd = (MessageEnd) container.getFragments().get(newIndex);
                Message otherMessage = otherMessageEnd.getMessage();

                // If there's a message in front that is part of a nested behaviour,
                // we need to find the beginning of it.
                if (otherMessage.getMessageSort() == MessageSort.REPLY) {
                    newIndex = findLowerBound((InteractionFragment) otherMessage.getSendEvent());
                }

                newIndex--;
            }
        } else {
            // When moving down, all to be moved fragments need to be considered.
            newIndex += fragments.size() - 1;

            // If after this message, there is another message, the message ends need to be moved further.
            if (newIndex < upperBound
                    && container.getFragments().get(newIndex + 1) instanceof MessageEnd) {
                MessageEnd otherMessageEnd = (MessageEnd) container.getFragments().get(newIndex + 1);
                Message otherMessage = otherMessageEnd.getMessage();

                // If the message has nested behaviour (or at least a reply message),
                // we need to find the end of it.
                if (!MessageViewUtil.isMessageViewDefined(aspect, otherMessage.getSignature())) {
                    newIndex = findUpperBound((InteractionFragment) otherMessage.getReceiveEvent());
                } else {
                    newIndex++;
                }
            }
        }

        if (newIndex >= lowerBound && newIndex < upperBound) {
            CompoundCommand command = new CompoundCommand();

            if (up) {
                // Move in reverse order so that the same index can be used.
                Collections.reverse(fragments);

                for (InteractionFragment current : fragments) {
                    command.append(MoveCommand.create(editingDomain, container,
                            RamPackage.Literals.FRAGMENT_CONTAINER__FRAGMENTS, current, newIndex));
                }
            } else {
                // Increase index to ensure that other fragments are moved up.
                for (InteractionFragment current : fragments) {
                    command.append(MoveCommand.create(editingDomain, container,
                            RamPackage.Literals.FRAGMENT_CONTAINER__FRAGMENTS, current, newIndex + 1));
                }
            }

            doExecute(editingDomain, command);
        }
    }

    /**
     * Finds the lower boundary of where the given fragment can be moved to.
     * If it's within a regular message view, the lower boundary is the receive event of the initial message.
     * If the fragment is within nested behaviour, the lower boundary is the receive event of that initial message.
     * If the fragment is within an operand, the lower boundary is 0.
     * 
     * @param fragment the fragment to find the lower boundary for
     * @return the lowest index the fragment can be moved to
     */
    private int findLowerBound(InteractionFragment fragment) {
        Message initialMessage = MessageViewUtil.findInitialMessage(fragment);
        InteractionFragment firstEvent = (InteractionFragment) initialMessage.getReceiveEvent();
        FragmentContainer container = firstEvent.getContainer();

        // If the fragment is located within an operand, but the initial message is located outside,
        // stay inside the operand.
        if (fragment.getContainer() != container) {
            return 0;
        }

        return container.getFragments().indexOf(firstEvent);
    }

    /**
     * Finds the upper boundary of where the given fragment can be moved to.
     * If it's within a regular message view, the upper boundary is the send event of the reply message.
     * If it's within an operand, the upper boundary is the last index within the operand.
     * If the fragment is part of nested behaviour, the upper boundary is the send event of that reply message.
     * 
     * @param fragment the fragment to find the upper boundary for
     * @return the highest index the fragment can be moved to
     */
    private int findUpperBound(InteractionFragment fragment) {
        Message initialMessage = MessageViewUtil.findInitialMessage(fragment);
        InteractionFragment firstEvent = (InteractionFragment) initialMessage.getReceiveEvent();
        FragmentContainer container = firstEvent.getContainer();

        // If the fragment is located within an operand, but the initial message is located outside,
        // stay inside the operand.
        if (fragment.getContainer() != container) {
            return fragment.getContainer().getFragments().size() - 1;
        }

        // Search end of behaviour.
        int index = container.getFragments().indexOf(firstEvent);
        for ( ; index < container.getFragments().size(); index++) {
            InteractionFragment current = container.getFragments().get(index);

            if (current instanceof MessageEnd
                    && current.getCovered().containsAll(firstEvent.getCovered())) {
                MessageEnd otherMessageEnd = (MessageEnd) current;
                Message otherMessage = otherMessageEnd.getMessage();

                if (otherMessage.getMessageSort() == MessageSort.REPLY
                        && otherMessage.getSignature() == initialMessage.getSignature()) {
                    break;
                }
            }
        }

        return index;
    }

    /**
     * Returns the index of the last fragment belonging to the behaviour starting with the given message send event.
     * 
     * @param owner the {@link Interaction} containing the message
     * @param sendEvent the {@link MessageOccurrenceSpecification} (send event) of the initial message of the behaviour
     * @return the index of the last fragment belonging to the behaviour
     */
    private int findMessageBehaviourEnd(Interaction owner, MessageOccurrenceSpecification sendEvent) {
        FragmentContainer container = sendEvent.getContainer();
        int fromIndex = container.getFragments().indexOf(sendEvent);
        int toIndex = fromIndex + 1;

        /**
         * In case the message is a reply and the receiving end is a gate, only one fragment needs to be deleted,
         * so the toIndex needs to be fromIndex.
         */
        if (sendEvent.getMessage().getMessageSort() == MessageSort.REPLY
                && sendEvent.getMessage().getReceiveEvent().eClass() == RamPackage.Literals.GATE) {
            toIndex = fromIndex;
        }

        for (int index = fromIndex + 1; index < container.getFragments().size(); index++) {
            InteractionFragment currentFragment = container.getFragments().get(index);

            // containsAll can not be used, because a CombinedFragment might have many.
            // Since only the first one matters in case of a CombinedFragment, it is fine to just use that.
            if (sendEvent.getCovered().contains(currentFragment.getCovered().get(0))) {
                if (currentFragment instanceof MessageOccurrenceSpecification) {
                    MessageOccurrenceSpecification messageEnd = (MessageOccurrenceSpecification) currentFragment;

                    if (messageEnd.getMessage().getReceiveEvent() == messageEnd) {
                        toIndex = index;
                        break;
                    }
                }

                toIndex = index - 1;
                break;
            } else if (container != owner && currentFragment != sendEvent.getMessage().getReceiveEvent()) {
                /**
                 * Special case for nested behaviour without a reply (see issue #305).
                 * Since there could be no additional fragment inside the operand, the toIndex needs to be increased
                 * in this case for each additional fragment following the message to be deleted.
                 */
                toIndex++;
            }
        }

        return toIndex;
    }

}
