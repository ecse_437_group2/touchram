package ca.mcgill.sel.core.controller;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.edit.command.RemoveCommand;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.COREContribution;
import ca.mcgill.sel.core.COREImpactModel;
import ca.mcgill.sel.core.COREImpactNode;
import ca.mcgill.sel.core.CoreFactory;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.impl.LayoutContainerMapImpl;
import ca.mcgill.sel.core.util.COREImpactModelUtil;

/**
 * The controller for contributions.
 *
 * @author Romain
 * @author Berk
 */
public class ContributionController extends CoreBaseController {

    /**
     * Creates a new instance of {@link ContributionController}.
     */
    ContributionController() {
        // prevent anyone outside this package to instantiate
    }

    /**
     * Create a new {@link COREContribution}.
     *
     * @param impactModel The {@link COREImpactModel} that will contain it
     * @param source the source {@link COREImpactNode} of this {@link COREContribution}
     * @param impacts the impact {@link COREImpactNode} of this {@link COREContribution}
     * @param layoutContainerElement is the root (key) of the layout
     * @return the {@link COREContribution} created
     */
    public COREContribution createContribution(COREImpactModel impactModel, COREImpactNode source,
            COREImpactNode impacts, COREImpactNode layoutContainerElement) {
        COREContribution contribution = CoreFactory.eINSTANCE.createCOREContribution();

        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(impactModel);

        CompoundCommand compoundCommand = new CompoundCommand();

        Command setSourceContributionCommand =
                SetCommand.create(editingDomain, contribution, CorePackage.Literals.CORE_CONTRIBUTION__SOURCE, source);

        Command setImpactsContributionCommand =
                SetCommand
                .create(editingDomain, contribution, CorePackage.Literals.CORE_CONTRIBUTION__IMPACTS, impacts);

        Command addContributionCommand =
                AddCommand.create(editingDomain, impactModel, CorePackage.Literals.CORE_IMPACT_MODEL__CONTRIBUTIONS,
                        contribution);

        compoundCommand.append(setSourceContributionCommand);
        compoundCommand.append(setImpactsContributionCommand);
        compoundCommand.append(addContributionCommand);

        //check if the root (key) of the layout changes its status.
        //if an outgoing contribution is created from the root (key), new root (key) should be set.
        if (layoutContainerElement.equals(source)) {
            Command changeRootGoalCommand = createChangeRootGoalCommand(source, 
                    COREImpactModelUtil.getNewRootForLayout(source, impacts));
            compoundCommand.append(changeRootGoalCommand);
        }
        
        doExecute(editingDomain, compoundCommand);

        return contribution;
    }

    /**
     * Remove this {@link COREContribution}. It will also remove this {@link COREContribution} from his source and
     * impacts {@link COREImpactNode}.
     *
     * @param contribution the {@link COREContribution} to remove
     */
    public void removeContribution(COREContribution contribution) {
        COREImpactModel impactModel =
                EMFModelUtil.getRootContainerOfType(contribution, CorePackage.Literals.CORE_IMPACT_MODEL);

        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(impactModel);

        Command command = createRemoveContributionCommand(impactModel, contribution);

        doExecute(editingDomain, command);
    }

    /**
     * Create a command that will remove this {@link COREContribution}. It will also remove this
     * {@link COREContribution} from his source and impacts {@link COREImpactNode}.
     *
     * @param impactModel the {@link COREImpactModel} that contains the {@link COREContribution}
     * @param contribution the {@link COREContribution} to remove
     * @return the command created
     */
    public Command createRemoveContributionCommand(COREImpactModel impactModel, COREContribution contribution) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(impactModel);

        CompoundCommand compoundCommand = new CompoundCommand();

        compoundCommand.append(RemoveCommand.create(editingDomain, impactModel,
                CorePackage.Literals.CORE_IMPACT_MODEL__CONTRIBUTIONS, contribution));

        compoundCommand.append(SetCommand.create(editingDomain, contribution,
                CorePackage.Literals.CORE_CONTRIBUTION__IMPACTS, SetCommand.UNSET_VALUE));
        compoundCommand.append(SetCommand.create(editingDomain, contribution,
                CorePackage.Literals.CORE_CONTRIBUTION__SOURCE, SetCommand.UNSET_VALUE));

        return compoundCommand;
    }

    /**
     * Set the relative weight of this {@link COREContribution}.
     *
     * @param contribution the {@link COREContribution} to set
     * @param weight the new weight of the {@link COREContribution}
     */
    public void setContributionWeight(COREContribution contribution, int weight) {
        COREImpactModel impactModel =
                EMFModelUtil.getRootContainerOfType(contribution, CorePackage.Literals.CORE_IMPACT_MODEL);

        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(impactModel);

        Command command =
                SetCommand.create(editingDomain, contribution, CorePackage.Literals.CORE_CONTRIBUTION__RELATIVE_WEIGHT,
                        weight);

        doExecute(editingDomain, command);
    }
    
    /**
     * Create a command that will change the root {@link COREImpactNode}.
     *
     * @param oldRoot the old root {@link COREImpactNode} linked to the layout
     * @param newRoot the new root {@link COREImpactNode} that will be linked to the layout
     * @return the {@link Command} created
     */
    public Command createChangeRootGoalCommand(COREImpactNode oldRoot, COREImpactNode newRoot) {
        COREImpactModel impactModel = EMFModelUtil.getRootContainerOfType(newRoot, 
                CorePackage.Literals.CORE_IMPACT_MODEL);
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(impactModel);
        CompoundCommand setNewRootGoalCommand = new CompoundCommand();

        //If the new root (key) already has a layout, remove it
        LayoutContainerMapImpl newRootsLayout = EMFModelUtil.getEntryFromMap(impactModel.getLayouts(), newRoot);
        if (newRootsLayout != null) {
            setNewRootGoalCommand.append(RemoveCommand.create(editingDomain, 
                    impactModel, 
                    CorePackage.Literals.CORE_IMPACT_MODEL__LAYOUTS,
                    newRootsLayout));
        }

        //Then set the old one's layout key to the new root (key)
        setNewRootGoalCommand.append(SetCommand.create(editingDomain, 
                EMFModelUtil.getEntryFromMap(impactModel.getLayouts(), oldRoot), 
                CorePackage.Literals.LAYOUT_CONTAINER_MAP__KEY, 
                newRoot));

        return setNewRootGoalCommand;
    }

}
