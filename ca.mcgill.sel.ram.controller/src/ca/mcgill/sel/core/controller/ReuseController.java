package ca.mcgill.sel.core.controller;

import java.util.List;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.edit.domain.EditingDomain;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.COREModel;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.COREModelExtension;
import ca.mcgill.sel.core.COREModelReuse;
import ca.mcgill.sel.core.COREReuse;
import ca.mcgill.sel.core.COREReuseConfiguration;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.util.COREModelUtil;

/**
 * The controller for {link @COREReuse and @COREModelReuse}.
 *
 * @author oalam
 */
public class ReuseController extends CoreBaseController {
    
    /**
     * Creates a new instance of {@link ReuseController}.
     */
    protected ReuseController() {
        // prevent anyone outside this package to instantiate
    }

    /**
     * Creates a new {@link COREReuse} and {@link COREModelReuse} for the given models.
     *
     * @param owner the model that is reusing
     * @param reusingConcern the concern that the model belong to
     * @param concern the reused concern
     * @param externalModel the referenced model (external woven model for models of selected features)
     * @param configuration the configuration for the reuse
     */
    public void createModelReuse(COREModel owner, COREConcern reusingConcern, COREConcern concern,
            COREModel externalModel, COREReuseConfiguration configuration) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(owner);

        COREReuse reuse = COREModelUtil.createReuse(reusingConcern, concern);
        
        // Update the configuration.
        // TODO: Ensure this is not already done.
        configuration.setSource(concern.getFeatureModel());
        configuration.setReuse(reuse);
        reuse.getConfigurations().add(configuration);
        reuse.setSelectedConfiguration(configuration);
        
        String configurationName = concern.getName() + "_";
        for (COREFeature feature : configuration.getSelected()) {
            configurationName += feature.getName();
        }
        configuration.setName(configurationName);

        COREModelReuse modelReuse = COREModelUtil.createModelReuse(externalModel, reuse);

        CompoundCommand compoundCommandConcern = new CompoundCommand();
        
        Command addCOREModelReuseCommand = AddCommand.create(editingDomain, owner,
                CorePackage.Literals.CORE_MODEL__MODEL_REUSES, modelReuse);
        compoundCommandConcern.append(addCOREModelReuseCommand);
        
        // Add reuse to all features.
        List<COREFeature> features = owner.getRealizes();
        
        for (COREFeature feature : features) {
            compoundCommandConcern.append(
                    AddCommand.create(editingDomain, feature, CorePackage.Literals.CORE_FEATURE__REUSES, reuse));
        }
        
        doExecute(editingDomain, compoundCommandConcern);
    }
    
    /**
     * Creates a new {@link COREModelExtension} for the given model.
     *
     * @param owner the model that is reusing
     * @param extendedModel the referenced model
     */
    public void createModelExtension(COREModel owner, COREModel extendedModel) {
        COREModelExtension extension = COREModelUtil.createModelExtension(extendedModel);
        doAdd(owner, CorePackage.Literals.CORE_MODEL__MODEL_EXTENSIONS, extension);
    }
    
    /**
     * Removes a model composition.
     *
     * @param modelComposition the {@link COREModelComposition} to be removed
     */
    public void removeModelComposition(COREModelComposition modelComposition) {
        doRemove(modelComposition);
    }

    /**
     * Delete the given COREReuse.
     *
     * @param reuse the COREReuse to be deleted
     */
    public void deleteCOREReuse(COREReuse reuse) {
        doRemove(reuse);
    }

}
