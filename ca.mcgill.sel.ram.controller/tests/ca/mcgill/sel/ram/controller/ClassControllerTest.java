package ca.mcgill.sel.ram.controller;

import static org.assertj.core.api.Assertions.assertThat;

import org.eclipse.emf.ecore.EObject;
import org.junit.Rule;
import org.junit.Test;

import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.AspectMessageView;
import ca.mcgill.sel.ram.Classifier;
import ca.mcgill.sel.ram.MessageView;
import ca.mcgill.sel.ram.Operation;
import ca.mcgill.sel.ram.controller.util.MessageViewTestUtil;
import ca.mcgill.sel.ram.controller.util.Model;
import ca.mcgill.sel.ram.controller.util.ModelResource;
import ca.mcgill.sel.ram.util.MessageViewUtil;

public class ClassControllerTest {
    
    @Rule
    public final ModelResource modelResource = new ModelResource("tests/models/undefined.ram") {

        @Override
        public void modelLoaded(EObject model) {
            aspect = (Aspect) model;
        }
        
    };
    
    private static ClassController controller = ControllerFactory.INSTANCE.getClassController();
    
    private Aspect aspect;
    
    @Test
    @Model(fileName = "tests/models/MessageViews/MappedOperations/MappedOperations.ram")
    public void testRemoveOperation_OperationWithAffectedBy_AMVRemoved() {
        Classifier classifier = MessageViewTestUtil.getClassifierByName(aspect, "B");
        Operation operation = MessageViewTestUtil.getOperationByName(classifier, "isPartial");
        MessageView messageView = MessageViewUtil.getMessageViewFor(aspect, operation);
        assertThat(messageView).isNotNull();
        
        AspectMessageView existingAMV = MessageViewTestUtil.getAspectMessageView(aspect, operation);
        
        controller.removeOperation(operation);
        
        assertThat(messageView.eContainer()).isNull();
        assertThat(aspect.getLayout().getContainers().keySet()).doesNotContain(messageView);
        assertThat(existingAMV.eContainer()).isNull();
    }
    
}
