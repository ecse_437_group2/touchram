package ca.mcgill.sel.ram.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.junit.Rule;
import org.junit.Test;

import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.COREModelElementComposition;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.AssignmentStatement;
import ca.mcgill.sel.ram.Classifier;
import ca.mcgill.sel.ram.ClassifierMapping;
import ca.mcgill.sel.ram.CombinedFragment;
import ca.mcgill.sel.ram.FragmentContainer;
import ca.mcgill.sel.ram.Gate;
import ca.mcgill.sel.ram.Interaction;
import ca.mcgill.sel.ram.InteractionFragment;
import ca.mcgill.sel.ram.InteractionOperand;
import ca.mcgill.sel.ram.Lifeline;
import ca.mcgill.sel.ram.Message;
import ca.mcgill.sel.ram.MessageEnd;
import ca.mcgill.sel.ram.MessageOccurrenceSpecification;
import ca.mcgill.sel.ram.MessageSort;
import ca.mcgill.sel.ram.MessageView;
import ca.mcgill.sel.ram.Operation;
import ca.mcgill.sel.ram.OperationMapping;
import ca.mcgill.sel.ram.StructuralFeature;
import ca.mcgill.sel.ram.controller.util.MessageViewTestUtil;
import ca.mcgill.sel.ram.controller.util.Model;
import ca.mcgill.sel.ram.controller.util.ModelResource;
import ca.mcgill.sel.ram.controller.util.TestUtil;
import ca.mcgill.sel.ram.impl.ContainerMapImpl;

public class MessageViewControllerTest {
    
    private class StateHelper {
        
        private List<InteractionFragment> previousFragments;
        private List<Message> previousMessages;
        private List<Lifeline> previousLifelines;
        
        private void saveState(FragmentContainer container) {            
            previousFragments = new ArrayList<>(container.getFragments());
            previousMessages = new ArrayList<>(interaction.getMessages());
            previousLifelines = new ArrayList<>(interaction.getLifelines());
        }
        
    }
    
    private final StateHelper stateHelper = new StateHelper();
    
    @Rule
    public final ModelResource modelResource = new ModelResource("tests/models/MessageViews/Messages.ram") {
        
        @Override
        public void modelLoaded(EObject model) {
            aspect = (Aspect) model;
        }
        
    };
    
    private static MessageViewController controller = ControllerFactory.INSTANCE.getMessageViewController();
    
    private Aspect aspect;
    private MessageView messageView;
    private Interaction interaction;
    
    private void loadMessageView(String className, String operationName) {
        messageView = MessageViewTestUtil.loadMessageView(aspect, className, operationName);
        interaction = messageView.getSpecification();
    }
    
    @Test
    public void testCreateMessage_UndefinedBehaviour_ReplyCreated() {
        loadMessageView("Caller", "testMessageCreation");
        
        Lifeline lifelineFrom = MessageViewTestUtil.getLifelineByName(interaction, "target");
        Lifeline lifelineTo = MessageViewTestUtil.getLifelineByName(interaction, "callee");
        
        Classifier receiver = MessageViewTestUtil.getClassifierByName(aspect, "Callee");
        Operation signature = MessageViewTestUtil.getOperationByName(receiver, "undefined");
        
        int addIndex = 3;
        
        stateHelper.saveState(interaction);
        
        controller.createMessage(interaction, lifelineFrom, lifelineTo, interaction, signature, addIndex);
        
        assertMessageCreated(signature, MessageSort.SYNCH_CALL, lifelineFrom, lifelineTo, interaction.getFragments().subList(addIndex, addIndex + 2));
        assertFragmentsCreatedInOrder(interaction, addIndex, 2);
        assertLifelinesUnchanged();
    }
    
    @Test
    public void testCreateMessage_SelfMessage_UndefinedBehaviour_NoReplyCreated() {
        loadMessageView("Caller", "testMessageCreation");
        
        Lifeline lifelineFrom = MessageViewTestUtil.getLifelineByName(interaction, "callee");
        
        Classifier receiver = MessageViewTestUtil.getClassifierByName(aspect, "Callee");
        Operation signature = MessageViewTestUtil.getOperationByName(receiver, "undefined");
        
        int addIndex = 3;
        
        stateHelper.saveState(interaction);
        
        controller.createMessage(interaction, lifelineFrom, lifelineFrom, interaction, signature, addIndex);
        
        assertMessageCreated(signature, MessageSort.SYNCH_CALL, lifelineFrom, lifelineFrom, interaction.getFragments().subList(addIndex, addIndex + 2));
        assertFragmentsCreatedInOrder(interaction, addIndex, 2);
        assertLifelinesUnchanged();
    }
    
    /**
     * @issue #425
     */
    @Test
    public void testCreateMessage_VoidOperationUndefinedBehaviour_ReplyCreated() {
        loadMessageView("Caller", "testMessageCreation");
        
        Lifeline lifelineFrom = MessageViewTestUtil.getLifelineByName(interaction, "target");
        Lifeline lifelineTo = MessageViewTestUtil.getLifelineByName(interaction, "callee");
        
        Classifier receiver = MessageViewTestUtil.getClassifierByName(aspect, "Callee");
        Operation signature = MessageViewTestUtil.getOperationByName(receiver, "undefined2");
        
        int addIndex = 3;
        
        stateHelper.saveState(interaction);
        
        controller.createMessage(interaction, lifelineFrom, lifelineTo, interaction, signature, addIndex);
        
        assertMessageCreated(signature, MessageSort.SYNCH_CALL, lifelineFrom, lifelineTo, interaction.getFragments().subList(addIndex, addIndex + 2));
        assertFragmentsCreatedInOrder(interaction, addIndex, 2);
        assertLifelinesUnchanged();
    }
    
    @Test
    public void testCreateMessage_DefinedBehaviour_NoReplyCreated() {
        loadMessageView("Caller", "testMessageCreation");
        
        Lifeline lifelineFrom = MessageViewTestUtil.getLifelineByName(interaction, "target");
        Lifeline lifelineTo = MessageViewTestUtil.getLifelineByName(interaction, "callee");
        
        Classifier receiver = MessageViewTestUtil.getClassifierByName(aspect, "Callee");
        Operation signature = MessageViewTestUtil.getOperationByName(receiver, "defined");
        
        int addIndex = 3;
        
        stateHelper.saveState(interaction);
        
        controller.createMessage(interaction, lifelineFrom, lifelineTo, interaction, signature, addIndex);
        
        assertMessageCreated(signature, MessageSort.SYNCH_CALL, lifelineFrom, lifelineTo, interaction.getFragments().subList(addIndex, addIndex + 2));
        assertFragmentsCreatedInOrder(interaction, addIndex, 2);
        assertLifelinesUnchanged();
    }
    
    @Test
    public void testCreateMessage_InOperand_MessageCreated() {
        loadMessageView("Caller", "testMessageCreation");
        
        Lifeline lifelineFrom = MessageViewTestUtil.getLifelineByName(interaction, "target");
        Lifeline lifelineTo = MessageViewTestUtil.getLifelineByName(interaction, "callee");
        
        Classifier receiver = MessageViewTestUtil.getClassifierByName(aspect, "Callee");
        Operation signature = MessageViewTestUtil.getOperationByName(receiver, "defined");
        
        List<InteractionFragment> interactionFragments = new ArrayList<>(interaction.getFragments());
        CombinedFragment combinedFragment = (CombinedFragment) interaction.getFragments().get(5);
        FragmentContainer container = combinedFragment.getOperands().get(0);
        
        int addIndex = 0;
        
        stateHelper.saveState(container);
        
        controller.createMessage(interaction, lifelineFrom, lifelineTo, container, signature, addIndex);
        
        assertMessageCreated(signature, MessageSort.SYNCH_CALL, lifelineFrom, lifelineTo, container.getFragments().subList(addIndex, addIndex + 2));
        assertFragmentsCreatedInOrder(container, addIndex, 2);
        assertFragmentsExactlyInOrder(interaction, interactionFragments, Collections.<InteractionFragment> emptyList());
        assertLifelinesUnchanged();
    }
    
    @Test
    public void testCreateMessage_InOperand_MessageAndReplyCreated() {
        loadMessageView("Caller", "testMessageCreation");
        
        Lifeline lifelineFrom = MessageViewTestUtil.getLifelineByName(interaction, "target");
        Lifeline lifelineTo = MessageViewTestUtil.getLifelineByName(interaction, "callee");
        
        Classifier receiver = MessageViewTestUtil.getClassifierByName(aspect, "Callee");
        Operation signature = MessageViewTestUtil.getOperationByName(receiver, "undefined2");
        
        List<InteractionFragment> interactionFragments = new ArrayList<>(interaction.getFragments());
        CombinedFragment combinedFragment = (CombinedFragment) interaction.getFragments().get(5);
        FragmentContainer container = combinedFragment.getOperands().get(0);
        
        int addIndex = 0;
        
        stateHelper.saveState(container);
        
        controller.createMessage(interaction, lifelineFrom, lifelineTo, container, signature, addIndex);
        
        assertMessageCreated(signature, MessageSort.SYNCH_CALL, lifelineFrom, lifelineTo, container.getFragments().subList(addIndex, addIndex + 2));
        assertFragmentsCreatedInOrder(container, addIndex, 2);
        assertFragmentsExactlyInOrder(interaction, interactionFragments, Collections.<InteractionFragment> emptyList());
        assertLifelinesUnchanged();
    }
    
    /**
     * @issue #390
     */
    @Test
    public void testCreateMessage_ExtendedOperationUndefinedBehaviour_NoReplyCreated() {
        loadMessageView("Caller", "testMessageCreation");
        
        Lifeline lifelineFrom = MessageViewTestUtil.getLifelineByName(interaction, "target");
        Lifeline lifelineTo = MessageViewTestUtil.getLifelineByName(interaction, "superclass");
        
        COREModelComposition composition = aspect.getModelExtensions().get(0);
        Aspect extendedAspect = (Aspect) composition.getSource();
        Classifier receiver = MessageViewTestUtil.getClassifierByName(extendedAspect, "SuperClass");
        Operation signature = MessageViewTestUtil.getOperationByName(receiver, "methodWithNoMV");
        
        int addIndex = 5;
        
        stateHelper.saveState(interaction);
        
        controller.createMessage(interaction, lifelineFrom, lifelineTo, interaction, signature, addIndex);
        
        Operation localOperation = getLocalOperation(composition, receiver, signature);
        
        assertMessageCreated(localOperation, MessageSort.SYNCH_CALL, lifelineFrom, lifelineTo, interaction.getFragments().subList(addIndex, addIndex + 2));
        assertFragmentsCreatedInOrder(interaction, addIndex, 2);
        assertLifelinesUnchanged();
    }
    
    /**
     * @issue #390
     */
    @Test
    @Model(fileName = "tests/models/MessageViews/MappedOperations/Extendee.ram")
    public void testCreateMessage_ReusedMappedOperation_UndefinedBehaviour_NoReplyCreated() {
        loadMessageView("TheB", "testMessageCreationReply");
        
        Lifeline lifelineFrom = MessageViewTestUtil.getLifelineByName(interaction, "target");
        Lifeline lifelineTo = MessageViewTestUtil.getLifelineByName(interaction, "theA");
        
        Classifier receiver = MessageViewTestUtil.getClassifierByName(aspect, "TheA");
        Operation signature = MessageViewTestUtil.getOperationByName(receiver, "mappedAndNoMV");
        
        int addIndex = 3;
        
        stateHelper.saveState(interaction);
        
        controller.createMessage(interaction, lifelineFrom, lifelineTo, interaction, signature, addIndex);
        
        assertMessageCreated(signature, MessageSort.SYNCH_CALL, lifelineFrom, lifelineTo, interaction.getFragments().subList(addIndex, addIndex + 2));
        assertFragmentsCreatedInOrder(interaction, addIndex, 2);
        assertLifelinesUnchanged();
    }
    
    /**
     * @issue #390
     */
    @Test
    @Model(fileName = "tests/models/MessageViews/MappedOperations/Extendee.ram")
    public void testCreateMessage_ReusedOperation_UndefinedBehaviour_NoReplyCreated() {
        loadMessageView("TheB", "testMessageCreationReply");
        
        Lifeline lifelineFrom = MessageViewTestUtil.getLifelineByName(interaction, "target");
        Lifeline lifelineTo = MessageViewTestUtil.getLifelineByName(interaction, "theA");
        
        COREModelComposition composition = aspect.getModelReuses().get(0);
        Aspect externalAspect = (Aspect) composition.getSource();
        Classifier receiver = MessageViewTestUtil.getClassifierByName(externalAspect, "Subject");
        Operation signature = MessageViewTestUtil.getOperationByName(receiver, "isNoMVDefined");
        
        int addIndex = 3;
        
        stateHelper.saveState(interaction);
        
        controller.createMessage(interaction, lifelineFrom, lifelineTo, interaction, signature, addIndex);
        
        Operation localOperation = getLocalOperation(composition, receiver, signature);
                
        assertMessageCreated(localOperation, MessageSort.SYNCH_CALL, lifelineFrom, lifelineTo, interaction.getFragments().subList(addIndex, addIndex + 2));
        assertFragmentsCreatedInOrder(interaction, addIndex, 2);
        assertLifelinesUnchanged();
    }

    /**
     * @issue #390
     */
    @Test
    @Model(fileName = "tests/models/MessageViews/MappedOperations/Extendee.ram")
    public void testCreateMessage_ReusedOperation_DefinedBehaviour_NoReplyCreated() {
        loadMessageView("TheB", "testMessageCreationReply");
        
        Lifeline lifelineFrom = MessageViewTestUtil.getLifelineByName(interaction, "target");
        Lifeline lifelineTo = MessageViewTestUtil.getLifelineByName(interaction, "theA");
        
        COREModelComposition composition = aspect.getModelReuses().get(0);
        Aspect externalAspect = (Aspect) composition.getSource();
        Classifier receiver = MessageViewTestUtil.getClassifierByName(externalAspect, "Subject");
        Operation signature = MessageViewTestUtil.getOperationByName(receiver, "getMyObservers");
        
        int addIndex = 3;
        
        stateHelper.saveState(interaction);
        
        controller.createMessage(interaction, lifelineFrom, lifelineTo, interaction, signature, addIndex);
        
        Operation localOperation = getLocalOperation(composition, receiver, signature);
        
        assertMessageCreated(localOperation, MessageSort.SYNCH_CALL, lifelineFrom, lifelineTo, interaction.getFragments().subList(addIndex, addIndex + 2));
        assertFragmentsCreatedInOrder(interaction, addIndex, 2);
        assertLifelinesUnchanged();
    }
    
    private Operation getLocalOperation(COREModelComposition composition, Classifier receiver, Operation signature) {
        Operation localOperation = null;
        
        for (COREModelElementComposition<?> mapping : composition.getCompositions()) {
            ClassifierMapping classifierMapping = (ClassifierMapping) mapping;
            
            if (classifierMapping.getFrom() == receiver) {
                for (OperationMapping operationMapping : classifierMapping.getOperationMappings()) {
                    if (operationMapping.getFrom() == signature) {
                        assertThat(localOperation).isNull();
                        localOperation = operationMapping.getTo();
                    }
                }
            }
        }
        
        assertThat(localOperation).isNotNull();
        
        return localOperation;
    }

    private void assertMessageCreated(Operation signature, MessageSort messageSort, Lifeline from, Lifeline to, List<InteractionFragment> fragments) {
        List<Message> newMessages = new ArrayList<>(interaction.getMessages());
        newMessages.removeAll(stateHelper.previousMessages);
        
        Message message = null;
        /**
         * Reply messages are added first.
         */
        if (newMessages.size() > 1) {
            message = (messageSort != MessageSort.REPLY) ? newMessages.get(1) : newMessages.get(0);
        } else {
            message = newMessages.get(0);
        }
        
        assertThat(message.getSignature()).isEqualTo(signature);
        assertThat(message.getMessageSort()).isEqualTo(messageSort);
        
        MessageOccurrenceSpecification sendEvent = (MessageOccurrenceSpecification) message.getSendEvent();
        assertThat(sendEvent.getCovered()).containsOnly(from);
        
        MessageOccurrenceSpecification receiveEvent = (MessageOccurrenceSpecification) message.getReceiveEvent();
        assertThat(receiveEvent.getCovered()).containsOnly(to);
        
        if (signature.getParameters().isEmpty() || messageSort == MessageSort.REPLY) {
            assertThat(message.getArguments()).isEmpty();
        } else {
            assertThat(message.getArguments()).hasSameSizeAs(signature.getParameters());
            
            assertThat(message.getArguments()).extracting("parameter")
                                              .containsOnlyElementsOf(signature.getParameters())
                                              .containsExactlyElementsOf(signature.getParameters());
            assertThat(message.getArguments()).extracting("value")
                                              .containsOnly(new Object[] { null });
        }
        
        assertThat(message.getAssignTo()).isNull();
        assertThat(message.getLocalProperties()).isEmpty();
        assertThat(message.getReturns()).isNull();
        assertThat(message.isSelfMessage()).isEqualTo(from == to);
        
        InteractionFragment first = fragments.get(0);
        InteractionFragment second = fragments.get(1);
        assertThat(sendEvent).isEqualTo(first);
        assertThat(receiveEvent).isEqualTo(second);
        
        assertThat(from.getCoveredBy()).contains(first);
        assertThat(to.getCoveredBy()).contains(second);
    }

    private void assertFragmentsCreatedInOrder(FragmentContainer container, int addIndex, int numberNewFragments) {
        List<InteractionFragment> newFragments = new ArrayList<>(container.getFragments()).subList(addIndex, addIndex + numberNewFragments);
        List<InteractionFragment> expectedFragments = new ArrayList<>(stateHelper.previousFragments);
        expectedFragments.addAll(addIndex, newFragments);
        
        TestUtil.assertContainsOnlyElementsExactlyOf(container.getFragments(), expectedFragments);
    }
    
    @Test
    public void testRemoveMessages_SingleMessage() {
        loadMessageView("Caller", "testMessageRemoval");
        
        MessageOccurrenceSpecification sendEvent = (MessageOccurrenceSpecification) interaction.getFragments().get(1);        
        List<InteractionFragment> fragmentsToDelete = new ArrayList<>(interaction.getFragments()).subList(1, 3);
        Set<Message> messagesToDelete = MessageViewTestUtil.getAffectedMessages(fragmentsToDelete);
        FragmentContainer container = sendEvent.getContainer();
        
        stateHelper.saveState(container);
        
        controller.removeMessages(interaction, sendEvent.getMessage());
        
        assertFragmentsDeleted(container, fragmentsToDelete);
        assertFragmentsExactlyInOrder(container, stateHelper.previousFragments, fragmentsToDelete);
        assertMessagesDeleted(messagesToDelete);
        assertLifelinesUnchanged();
    }
    
    /**
     * Test that when removing a message with nested behaviour, all fragments and messages are removed properly.
     */
    @Test
    public void testRemoveMessages_NestedBehaviourWithReplyRemoved() {
        loadMessageView("Caller", "testMessageRemoval");
        
        // Get message with nested behaviour.
        MessageOccurrenceSpecification sendEvent = (MessageOccurrenceSpecification) interaction.getFragments().get(5);
        
        List<InteractionFragment> fragmentsToDelete = new ArrayList<>(interaction.getFragments().subList(5, 12));
        Set<Message> messagesToDelete = MessageViewTestUtil.getAffectedMessages(fragmentsToDelete);
        FragmentContainer container = sendEvent.getContainer();
        
        stateHelper.saveState(container);
        
        controller.removeMessages(interaction, sendEvent.getMessage());
        
        assertFragmentsDeleted(container, fragmentsToDelete);
        assertFragmentsExactlyInOrder(container, stateHelper.previousFragments, fragmentsToDelete);
        assertMessagesDeleted(messagesToDelete);
        assertLifelinesUnchanged();
    }
    
    /**
     * Test nested behaviour where defined message has reply.
     */
    @Test
    public void testRemoveMessages_NestedBehaviourWithReply_ContentsAndEmptyLifelineRemoved() {
        loadMessageView("Caller", "testNestedMessageRemoval");
        
        MessageOccurrenceSpecification sendEvent = (MessageOccurrenceSpecification) interaction.getFragments().get(1);
        
        List<InteractionFragment> fragmentsToDelete = new ArrayList<>(interaction.getFragments().subList(1, 9));
        Set<Message> messagesToDelete = MessageViewTestUtil.getAffectedMessages(fragmentsToDelete);
        List<Lifeline> lifelinesToDelete = Collections.singletonList(MessageViewTestUtil.getLifelineByName(interaction, "caller"));
        FragmentContainer container = sendEvent.getContainer();
        
        stateHelper.saveState(container);
        
        controller.removeMessages(interaction, sendEvent.getMessage());
        
        assertFragmentsDeleted(container, fragmentsToDelete);
        assertFragmentsExactlyInOrder(container, stateHelper.previousFragments, fragmentsToDelete);
        assertMessagesDeleted(messagesToDelete);
        assertLifelinesDeleted(lifelinesToDelete);
    }
    
    /**
     * Test nested behaviour where defined messages have no reply and there's a message and/or fragment
     * on the originating lifeline after (i.e., to make sure those are not deleted).
     */
    @Test
    public void testRemoveMessages_NestedBehaviourWithoutReply_ContentsAndEmptyLifelineRemoved() {
        loadMessageView("Caller", "testNestedMessageRemoval");
        
        MessageOccurrenceSpecification sendEvent = (MessageOccurrenceSpecification) interaction.getFragments().get(9);
        List<InteractionFragment> fragmentsToDelete = new ArrayList<>(interaction.getFragments().subList(9, 17));
        Set<Message> messagesToDelete = MessageViewTestUtil.getAffectedMessages(fragmentsToDelete);
        List<Lifeline> lifelinesToDelete = Collections.singletonList(MessageViewTestUtil.getLifelineByName(interaction, "newCallee"));
        FragmentContainer container = sendEvent.getContainer();
        
        stateHelper.saveState(container);
        
        controller.removeMessages(interaction, sendEvent.getMessage());
        
        assertFragmentsDeleted(container, fragmentsToDelete);
        assertFragmentsExactlyInOrder(container, stateHelper.previousFragments, fragmentsToDelete);
        assertMessagesDeleted(messagesToDelete);
        assertLifelinesDeleted(lifelinesToDelete);
    }
    
    /**
     * Test nested behaviour where defined messages have no reply and there's a message and/or fragment
     * on the originating lifeline after (i.e., to make sure those are not deleted).
     */
    @Test
    public void testRemoveMessages_NestedNestedBehaviour_ContentsAndEmptyLifelineRemoved() {
        loadMessageView("Caller", "testNestedMessageRemoval");
        
        MessageOccurrenceSpecification sendEvent = (MessageOccurrenceSpecification) interaction.getFragments().get(11);
        List<InteractionFragment> fragmentsToDelete = new ArrayList<>(interaction.getFragments().subList(11, 17));
        Set<Message> messagesToDelete = MessageViewTestUtil.getAffectedMessages(fragmentsToDelete);
        List<Lifeline> lifelinesToDelete = Collections.singletonList(MessageViewTestUtil.getLifelineByName(interaction, "newCallee"));
        FragmentContainer container = sendEvent.getContainer();
        
        stateHelper.saveState(container);
        
        controller.removeMessages(interaction, sendEvent.getMessage());
        
        assertFragmentsDeleted(container, fragmentsToDelete);
        assertFragmentsExactlyInOrder(container, stateHelper.previousFragments, fragmentsToDelete);
        assertMessagesDeleted(messagesToDelete);
        assertLifelinesDeleted(lifelinesToDelete);
    }
    
    /**
     * Test nested behaviour where defined messages have no reply and there's no message and/or fragment
     * on the originating lifeline after.
     */
    @Test
    public void testRemoveMessages_NestedNestedBehaviour_LastBehaviour_ContentsAndEmptyLifelineRemoved() {
        loadMessageView("Caller", "testNestedMessageRemoval");
        
        MessageOccurrenceSpecification sendEvent = (MessageOccurrenceSpecification) interaction.getFragments().get(19);
        List<InteractionFragment> fragmentsToDelete = new ArrayList<>(interaction.getFragments().subList(19, 25));
        Set<Message> messagesToDelete = MessageViewTestUtil.getAffectedMessages(fragmentsToDelete);
        List<Lifeline> lifelinesToDelete = Collections.singletonList(MessageViewTestUtil.getLifelineByName(interaction, "newCallee2"));
        FragmentContainer container = sendEvent.getContainer();
        
        stateHelper.saveState(container);
        
        controller.removeMessages(interaction, sendEvent.getMessage());
        
        assertFragmentsDeleted(container, fragmentsToDelete);
        assertFragmentsExactlyInOrder(container, stateHelper.previousFragments, fragmentsToDelete);
        assertMessagesDeleted(messagesToDelete);
        assertLifelinesDeleted(lifelinesToDelete);
    }
    
    /**
     * Test that when removing a message with nested behaviour that includes a combined fragment, 
     * all fragments and messages are removed properly.
     * 
     * See also issue #438.
     */
    @Test
    public void testRemoveMessages_NestedBehaviourWithCombinedFragment_OperandContentsRemoved() {
        loadMessageView("Caller", "testMessageRemoval");
        
        MessageOccurrenceSpecification sendEvent = (MessageOccurrenceSpecification) interaction.getFragments().get(5);
        
        CombinedFragment combinedFragment = (CombinedFragment) interaction.getFragments().get(9);
        InteractionOperand operand = combinedFragment.getOperands().get(0);
        
        List<InteractionFragment> fragmentsToDelete = new ArrayList<>(interaction.getFragments()).subList(5, 12);
        Set<Message> messagesToDelete = MessageViewTestUtil.getAffectedMessages(fragmentsToDelete);
        FragmentContainer container = sendEvent.getContainer();
        
        List<InteractionFragment> nestedFragmentsToDelete = new ArrayList<>(operand.getFragments());
        Set<Message> nestedMessagesToDelete = MessageViewTestUtil.getAffectedMessages(nestedFragmentsToDelete);
        FragmentContainer nestedContainer = nestedFragmentsToDelete.get(0).getContainer();
        
        stateHelper.saveState(container);
        
        controller.removeMessages(interaction, sendEvent.getMessage());
        
        assertFragmentsDeleted(container, fragmentsToDelete);
        assertFragmentsExactlyInOrder(container, stateHelper.previousFragments, fragmentsToDelete);
        assertMessagesDeleted(messagesToDelete);
        assertLifelinesUnchanged();
        
        // Make sure all the operand fragments and the operand were removed.
        assertFragmentsDeleted(nestedContainer, nestedFragmentsToDelete);
        assertMessagesDeleted(nestedMessagesToDelete);
        assertThat(operand.eResource()).isNull();
    }
    
    /**
     * Test that when removing a message with nested behaviour inside a combined fragment,
     * which has no reply and there are no messages after this on the same lifeline, 
     * all fragments and messages are removed properly.
     * See issue #305.
     */
    @Test
    public void testRemoveMessages_CombinedFragmentWithNestedBehaviourWithNoReply_ContentsAndCoveredLifelinesRemoved() {
        loadMessageView("Caller", "testMessageRemoval");
        
        CombinedFragment combinedFragment = (CombinedFragment) interaction.getFragments().get(17);
        InteractionOperand operand = combinedFragment.getOperands().get(0);
        MessageOccurrenceSpecification sendEvent = (MessageOccurrenceSpecification) operand.getFragments().get(0);
        
        List<InteractionFragment> fragmentsToDelete = new ArrayList<>(operand.getFragments());
        Set<Message> messagesToDelete = MessageViewTestUtil.getAffectedMessages(fragmentsToDelete);
        FragmentContainer container = sendEvent.getContainer();
        
        stateHelper.saveState(container);
        
        controller.removeMessages(interaction, sendEvent.getMessage());
        
        assertFragmentsDeleted(container, fragmentsToDelete);
        assertFragmentsExactlyInOrder(container, stateHelper.previousFragments, fragmentsToDelete);
        assertMessagesDeleted(messagesToDelete);
        assertLifelinesUnchanged();
        
        // Check operand.
        assertThat(operand.getFragments()).isEmpty();
    }

    /**
     * Test that when removing a message with nested behaviour,
     * which has no reply and there are messages after this on the same lifeline, 
     * all fragments and messages are removed properly.
     */
    @Test
    public void testRemoveMessages_NestedBehaviourWithNoReply_OnlyNestedBehaviourDeleted() {
        loadMessageView("Caller", "testMessageRemoval");
        
        MessageOccurrenceSpecification sendEvent = (MessageOccurrenceSpecification) interaction.getFragments().get(13);
        
        List<InteractionFragment> fragmentsToDelete = new ArrayList<>(interaction.getFragments()).subList(13, 17);
        Set<Message> messagesToDelete = MessageViewTestUtil.getAffectedMessages(fragmentsToDelete);
        FragmentContainer container = sendEvent.getContainer();
        
        stateHelper.saveState(container);
        
        controller.removeMessages(interaction, sendEvent.getMessage());
        
        assertFragmentsDeleted(container, fragmentsToDelete);
        assertFragmentsExactlyInOrder(container, stateHelper.previousFragments, fragmentsToDelete);
        assertMessagesDeleted(messagesToDelete);
        assertLifelinesUnchanged();
    }

    /**
     * Test removal of empty lifeline with regular message.
     */
    @Test
    public void testRemoveMessages_SingleMessage_EmptyLifelineRemoved() {
        loadMessageView("Caller", "testEmptyLifelines");
        
        MessageOccurrenceSpecification sendEvent = (MessageOccurrenceSpecification) interaction.getFragments().get(1);
        
        List<InteractionFragment> fragmentsToDelete = new ArrayList<>(interaction.getFragments()).subList(1, 3);
        List<Lifeline> lifelinesToDelete = Collections.singletonList(MessageViewTestUtil.getLifelineByName(interaction, "callee"));
        Set<Message> messagesToDelete = MessageViewTestUtil.getAffectedMessages(fragmentsToDelete);
        FragmentContainer container = sendEvent.getContainer();
        
        stateHelper.saveState(container);
        
        controller.removeMessages(interaction, sendEvent.getMessage());
        
        assertFragmentsDeleted(container, fragmentsToDelete);
        assertFragmentsExactlyInOrder(container, stateHelper.previousFragments, fragmentsToDelete);
        assertMessagesDeleted(messagesToDelete);
        assertLifelinesDeleted(lifelinesToDelete);
    }

    /**
     * Test removal of empty lifeline with message with reply (undefined behaviour).
     */
    @Test
    public void testRemoveMessages_MessageWithReply_EmptyLifelineRemoved() {
        loadMessageView("Caller", "testEmptyLifelines");
        
        MessageOccurrenceSpecification sendEvent = (MessageOccurrenceSpecification) interaction.getFragments().get(3);
        
        List<InteractionFragment> fragmentsToDelete = new ArrayList<>(interaction.getFragments()).subList(3, 7);
        List<Lifeline> lifelinesToDelete = Collections.singletonList(MessageViewTestUtil.getLifelineByName(interaction, "callee2"));
        Set<Message> messagesToDelete = MessageViewTestUtil.getAffectedMessages(fragmentsToDelete);
        FragmentContainer container = sendEvent.getContainer();
        
        stateHelper.saveState(container);
        
        controller.removeMessages(interaction, sendEvent.getMessage());
        
        assertFragmentsDeleted(container, fragmentsToDelete);
        assertFragmentsExactlyInOrder(container, stateHelper.previousFragments, fragmentsToDelete);
        assertMessagesDeleted(messagesToDelete);
        assertLifelinesDeleted(lifelinesToDelete);
    }

    /**
     * Test removal of empty lifelines with message with defined nested behaviour.
     */
    @Test
    public void testRemoveMessages_MessageWithBehaviour_EmptyLifelinesRemoved() {
        loadMessageView("Caller", "testEmptyLifelines");
        
        MessageOccurrenceSpecification sendEvent = (MessageOccurrenceSpecification) interaction.getFragments().get(7);
        
        List<InteractionFragment> fragmentsToDelete = new ArrayList<>(interaction.getFragments()).subList(7, 15);
        List<Lifeline> lifelinesToDelete = new ArrayList<>();
        lifelinesToDelete.add(MessageViewTestUtil.getLifelineByName(interaction, "callee3"));
        lifelinesToDelete.add(MessageViewTestUtil.getLifelineByName(interaction, "caller"));
        Set<Message> messagesToDelete = MessageViewTestUtil.getAffectedMessages(fragmentsToDelete);
        FragmentContainer container = sendEvent.getContainer();
        
        stateHelper.saveState(container);
        
        controller.removeMessages(interaction, sendEvent.getMessage());
        
        assertFragmentsDeleted(container, fragmentsToDelete);
        assertFragmentsExactlyInOrder(container, stateHelper.previousFragments, fragmentsToDelete);
        assertMessagesDeleted(messagesToDelete);
        assertLifelinesDeleted(lifelinesToDelete);
    }

    /**
     * Test removal of empty lifeline with create message.
     */
    @Test
    public void testRemoveMessages_CreateMessage_LifelineRemoved() {
        loadMessageView("Caller", "testEmptyLifelines");
        
        MessageOccurrenceSpecification sendEvent = (MessageOccurrenceSpecification) interaction.getFragments().get(15);
        
        List<InteractionFragment> fragmentsToDelete = new ArrayList<>(interaction.getFragments()).subList(15, 17);
        List<Lifeline> lifelinesToDelete = Collections.singletonList(MessageViewTestUtil.getLifelineByName(interaction, "calleee"));
        Set<Message> messagesToDelete = MessageViewTestUtil.getAffectedMessages(fragmentsToDelete);
        FragmentContainer container = sendEvent.getContainer();
        
        stateHelper.saveState(container);
        
        controller.removeMessages(interaction, sendEvent.getMessage());
        
        assertFragmentsDeleted(container, fragmentsToDelete);
        assertFragmentsExactlyInOrder(container, stateHelper.previousFragments, fragmentsToDelete);
        assertMessagesDeleted(messagesToDelete);
        assertLifelinesDeleted(lifelinesToDelete);
    }

    /**
     * Test removal of empty lifeline with create message with defined behaviour.
     */
    @Test
    public void testRemoveMessages_CreateMessageWithBehaviour_BehaviourAndLifelineRemoved() {
        loadMessageView("Caller", "testEmptyLifelines");
        
        MessageOccurrenceSpecification sendEvent = (MessageOccurrenceSpecification) interaction.getFragments().get(17);
        
        List<InteractionFragment> fragmentsToDelete = new ArrayList<>(interaction.getFragments()).subList(17, 20);
        List<Lifeline> lifelinesToDelete = Collections.singletonList(MessageViewTestUtil.getLifelineByName(interaction, "calleee2"));
        Set<Message> messagesToDelete = MessageViewTestUtil.getAffectedMessages(fragmentsToDelete);
        FragmentContainer container = sendEvent.getContainer();
        
        stateHelper.saveState(container);
        
        controller.removeMessages(interaction, sendEvent.getMessage());
        
        assertFragmentsDeleted(container, fragmentsToDelete);
        assertFragmentsExactlyInOrder(container, stateHelper.previousFragments, fragmentsToDelete);
        assertMessagesDeleted(messagesToDelete);
        assertLifelinesDeleted(lifelinesToDelete);
    }

    @Test
    public void testRemoveMessages_ReplyMessage_GateEndAndMessageRemoved() {
        loadMessageView("Callee", "defined");
        
        CombinedFragment combinedFragment = (CombinedFragment) interaction.getFragments().get(1);
        InteractionOperand operand = combinedFragment.getOperands().get(0);
        MessageOccurrenceSpecification sendEvent = (MessageOccurrenceSpecification) operand.getFragments().get(0);
        Message message = sendEvent.getMessage();
        
        assertThat(message.getMessageSort()).isEqualTo(MessageSort.REPLY);
        
        List<InteractionFragment> fragmentsToDelete = new ArrayList<>(operand.getFragments());
        List<Gate> expectedGates = new ArrayList<>(interaction.getFormalGates());
        expectedGates.remove(message.getReceiveEvent());
        Set<Message> messagesToDelete = MessageViewTestUtil.getAffectedMessages(fragmentsToDelete);
        FragmentContainer container = sendEvent.getContainer();
        
        stateHelper.saveState(container);
        
        controller.removeMessages(interaction, sendEvent.getMessage());
        
        assertFragmentsDeleted(container, fragmentsToDelete);
        assertFragmentsExactlyInOrder(container, stateHelper.previousFragments, fragmentsToDelete);
        assertMessagesDeleted(messagesToDelete);
        assertLifelinesUnchanged();
        // Check that the correct Gate was removed.
        assertThat(interaction.getFormalGates()).containsOnlyElementsOf(expectedGates);
    }

    /**
     * Test message removal with new lifeline 
     * in CombinedFragment (lifeline not used anywhere outside of CombinedFragment).
     */
    @Test
    public void testRemoveMessages_MessageInOperand_EmptyLifelineRemovedAndCoveredCombinedFragmentUpdated() {
        loadMessageView("Caller", "testCombinedFragmentMessages");
        
        CombinedFragment combinedFragment = (CombinedFragment) interaction.getFragments().get(1);
        InteractionOperand operand = combinedFragment.getOperands().get(0);
        MessageOccurrenceSpecification sendEvent = (MessageOccurrenceSpecification) operand.getFragments().get(0);
        
        List<InteractionFragment> fragmentsToDelete = new ArrayList<>(operand.getFragments());
        List<Lifeline> lifelinesToDelete = Collections.singletonList(MessageViewTestUtil.getLifelineByName(interaction, "callee2"));
        List<Lifeline> expectedCoveredLifelines = new ArrayList<>(combinedFragment.getCovered());
        expectedCoveredLifelines.removeAll(lifelinesToDelete);
        Set<Message> messagesToDelete = MessageViewTestUtil.getAffectedMessages(fragmentsToDelete);
        FragmentContainer container = sendEvent.getContainer();
        
        stateHelper.saveState(container);
        
        controller.removeMessages(interaction, sendEvent.getMessage());
        
        assertFragmentsDeleted(container, fragmentsToDelete);
        assertFragmentsExactlyInOrder(container, stateHelper.previousFragments, fragmentsToDelete);
        assertMessagesDeleted(messagesToDelete);
        assertLifelinesDeleted(lifelinesToDelete);
        
        assertThat(operand.getFragments()).isEmpty();
        assertThat(combinedFragment.getCovered()).containsOnlyElementsOf(expectedCoveredLifelines);
    }

    /**
     * Test message removal with lifeline in CombinedFragment, where lifeline is used somewhere else
     * and cannot be removed.
     */
    @Test
    public void testRemoveMessages_MessageInOperand_NonEmptyLifelineNotRemovedAndCoveredCombinedFragmentUpdated() {
        loadMessageView("Caller", "testCombinedFragmentMessages");
        
        CombinedFragment combinedFragment = (CombinedFragment) interaction.getFragments().get(2);
        InteractionOperand operand = combinedFragment.getOperands().get(0);
        MessageOccurrenceSpecification sendEvent = (MessageOccurrenceSpecification) operand.getFragments().get(0);
        
        List<InteractionFragment> fragmentsToDelete = new ArrayList<>(operand.getFragments());
        List<Lifeline> expectedCoveredLifelines = new ArrayList<>(combinedFragment.getCovered());
        expectedCoveredLifelines.remove(MessageViewTestUtil.getLifelineByName(interaction, "callee"));
        Set<Message> messagesToDelete = MessageViewTestUtil.getAffectedMessages(fragmentsToDelete);
        FragmentContainer container = sendEvent.getContainer();
        
        stateHelper.saveState(container);
        
        controller.removeMessages(interaction, sendEvent.getMessage());
        
        assertFragmentsDeleted(container, fragmentsToDelete);
        assertFragmentsExactlyInOrder(container, stateHelper.previousFragments, fragmentsToDelete);
        assertMessagesDeleted(messagesToDelete);
        assertLifelinesUnchanged();
        
        assertThat(operand.getFragments()).isEmpty();
        assertThat(combinedFragment.getCovered()).containsOnlyElementsOf(expectedCoveredLifelines);
    }

    /**
     * Test message removal with new lifeline 
     * in CombinedFragment (lifeline used in another operand of CombinedFragment).
     */
    @Test
    public void testRemoveMessages_MessageInOperand_UsedLifelineNotRemovedAndCoveredCombinedFragmentNotUpdated() {
        loadMessageView("Caller", "testCombinedFragmentMessages");
        
        CombinedFragment combinedFragment = (CombinedFragment) interaction.getFragments().get(3);
        InteractionOperand operand = combinedFragment.getOperands().get(0);
        MessageOccurrenceSpecification sendEvent = (MessageOccurrenceSpecification) operand.getFragments().get(0);
        
        List<InteractionFragment> fragmentsToDelete = new ArrayList<>(operand.getFragments());
        List<Lifeline> expectedCoveredLifelines = new ArrayList<>(combinedFragment.getCovered());
        Set<Message> messagesToDelete = MessageViewTestUtil.getAffectedMessages(fragmentsToDelete);
        FragmentContainer container = sendEvent.getContainer();
        
        stateHelper.saveState(container);
        
        controller.removeMessages(interaction, sendEvent.getMessage());
        
        assertFragmentsDeleted(container, fragmentsToDelete);
        assertFragmentsExactlyInOrder(container, stateHelper.previousFragments, fragmentsToDelete);
        assertMessagesDeleted(messagesToDelete);
        assertLifelinesUnchanged();
        
        assertThat(operand.getFragments()).isEmpty();
        assertThat(combinedFragment.getCovered()).containsOnlyElementsOf(expectedCoveredLifelines);
    }

    /**
     * Test message removal with lifeline in CombinedFragment, where lifeline is used somewhere else
     * and cannot be removed.
     */
    @Test
    public void testRemoveMessages_MessageWithNestedBehaviourInOperand_NestedBehaviourAndEmptyLifelinesRemoved() {
        loadMessageView("Caller", "testCombinedFragmentMessages");
        
        CombinedFragment combinedFragment = (CombinedFragment) interaction.getFragments().get(4);
        InteractionOperand operand = combinedFragment.getOperands().get(0);
        MessageOccurrenceSpecification sendEvent = (MessageOccurrenceSpecification) operand.getFragments().get(0);
        
        List<InteractionFragment> fragmentsToDelete = new ArrayList<>(operand.getFragments());
        List<Lifeline> lifelinesToDelete = new ArrayList<>();
        lifelinesToDelete.add(MessageViewTestUtil.getLifelineByName(interaction, "caller"));
        lifelinesToDelete.add(MessageViewTestUtil.getLifelineByName(interaction, "newCallee"));
        
        List<Lifeline> expectedCoveredLifelines = new ArrayList<>(combinedFragment.getCovered());
        expectedCoveredLifelines.removeAll(lifelinesToDelete);
        expectedCoveredLifelines.remove(MessageViewTestUtil.getLifelineByName(interaction, "callee"));
        
        Set<Message> messagesToDelete = MessageViewTestUtil.getAffectedMessages(fragmentsToDelete);
        FragmentContainer container = sendEvent.getContainer();
        
        stateHelper.saveState(container);
        
        controller.removeMessages(interaction, sendEvent.getMessage());
        
        assertFragmentsDeleted(container, fragmentsToDelete);
        assertFragmentsExactlyInOrder(container, stateHelper.previousFragments, fragmentsToDelete);
        assertMessagesDeleted(messagesToDelete);
        assertLifelinesDeleted(lifelinesToDelete);
        
        assertThat(operand.getFragments()).isEmpty();
        assertThat(combinedFragment.getCovered()).containsOnlyElementsOf(expectedCoveredLifelines);
    }

    /**
     * Test message removal with lifeline in nested CombinedFragment, where lifeline is not used somewhere else
     * and should be removed.
     */
    @Test
    public void testRemoveMessages_MessageWithNestedBehaviourInOperand_NestedBehaviourAndEmptyLifelinesRemoved2() {
        loadMessageView("Caller", "testCombinedFragmentMessages");
        
        CombinedFragment combinedFragment = (CombinedFragment) interaction.getFragments().get(5);
        InteractionOperand operand = combinedFragment.getOperands().get(0);
        CombinedFragment nestedCombinedFragment = (CombinedFragment) operand.getFragments().get(2);
        InteractionOperand nestedOperand = nestedCombinedFragment.getOperands().get(0);
        
        MessageOccurrenceSpecification sendEvent = (MessageOccurrenceSpecification) nestedOperand.getFragments().get(0);
        
        List<InteractionFragment> fragmentsToDelete = new ArrayList<>(nestedOperand.getFragments());
        List<Lifeline> lifelinesToDelete = Collections.singletonList(MessageViewTestUtil.getLifelineByName(interaction, "caller2"));
        
        List<Lifeline> expectedCoveredLifelines = new ArrayList<>(combinedFragment.getCovered());
        expectedCoveredLifelines.removeAll(lifelinesToDelete);
        
        List<Lifeline> expectedNestedCoveredLifelines = new ArrayList<>(nestedCombinedFragment.getCovered());
        expectedNestedCoveredLifelines.removeAll(lifelinesToDelete);
        
        Set<Message> messagesToDelete = MessageViewTestUtil.getAffectedMessages(fragmentsToDelete);
        FragmentContainer container = sendEvent.getContainer();
        
        stateHelper.saveState(container);
        
        controller.removeMessages(interaction, sendEvent.getMessage());
        
        assertFragmentsDeleted(container, fragmentsToDelete);
        assertFragmentsExactlyInOrder(container, stateHelper.previousFragments, fragmentsToDelete);
        assertMessagesDeleted(messagesToDelete);
        assertLifelinesDeleted(lifelinesToDelete);
        
        assertThat(nestedOperand.getFragments()).isEmpty();
        assertThat(combinedFragment.getCovered()).containsOnlyElementsOf(expectedCoveredLifelines);
        assertThat(nestedCombinedFragment.getCovered()).containsOnlyElementsOf(expectedNestedCoveredLifelines);
    }

    /**
     * Test message removal with lifeline in nested CombinedFragment, where lifeline is used somewhere else (outside)
     * and cannot be removed, but should be removed as being covered from the CombinedFragments.
     */
    @Test
    public void testRemoveMessages_MessageWithNestedBehaviourInOperand_NestedCombinedFragment_CoveredByUpdated() {
        loadMessageView("Caller", "testCombinedFragmentMessages");
        
        CombinedFragment combinedFragment = (CombinedFragment) interaction.getFragments().get(5);
        InteractionOperand operand = combinedFragment.getOperands().get(0);
        CombinedFragment nestedCombinedFragment = (CombinedFragment) operand.getFragments().get(2);
        InteractionOperand nestedOperand = nestedCombinedFragment.getOperands().get(1);
        
        MessageOccurrenceSpecification sendEvent = (MessageOccurrenceSpecification) nestedOperand.getFragments().get(0);
        
        List<InteractionFragment> fragmentsToDelete = new ArrayList<>(nestedOperand.getFragments());
        Lifeline coveredLifelineToRemove = MessageViewTestUtil.getLifelineByName(interaction, "callee3");
        
        List<Lifeline> expectedCoveredLifelines = new ArrayList<>(combinedFragment.getCovered());
        expectedCoveredLifelines.remove(coveredLifelineToRemove);
        
        List<Lifeline> expectedNestedCoveredLifelines = new ArrayList<>(nestedCombinedFragment.getCovered());
        expectedNestedCoveredLifelines.remove(coveredLifelineToRemove);
        
        Set<Message> messagesToDelete = MessageViewTestUtil.getAffectedMessages(fragmentsToDelete);
        FragmentContainer container = sendEvent.getContainer();
        
        stateHelper.saveState(container);
        
        controller.removeMessages(interaction, sendEvent.getMessage());
        
        assertFragmentsDeleted(container, fragmentsToDelete);
        assertFragmentsExactlyInOrder(container, stateHelper.previousFragments, fragmentsToDelete);
        assertMessagesDeleted(messagesToDelete);
        assertLifelinesUnchanged();
        
        assertThat(nestedOperand.getFragments()).isEmpty();
        assertThat(combinedFragment.getCovered()).containsOnlyElementsOf(expectedCoveredLifelines);
        assertThat(nestedCombinedFragment.getCovered()).containsOnlyElementsOf(expectedNestedCoveredLifelines);
    }

    /**
     * Test message removal with lifeline in outer CombinedFragment of nested ones.
     * 
     * Related to issues #304 and #438.
     */
    @Test
    public void testRemoveMessages_MessageWithNestedBehaviourInOperand_NestedCombinedFragment_OuterCombinedFragmentCoveredByUpdated() {
        loadMessageView("Caller", "testCombinedFragmentMessages");
        
        CombinedFragment combinedFragment = (CombinedFragment) interaction.getFragments().get(5);
        InteractionOperand operand = combinedFragment.getOperands().get(0);
        CombinedFragment nestedCombinedFragment = (CombinedFragment) operand.getFragments().get(2);
        
        MessageOccurrenceSpecification sendEvent = (MessageOccurrenceSpecification) operand.getFragments().get(0);
        
        List<InteractionFragment> fragmentsToDelete = new ArrayList<>(operand.getFragments());
        List<Lifeline> lifelinesToDelete = Collections.singletonList(MessageViewTestUtil.getLifelineByName(interaction, "caller2"));
        
        List<Lifeline> expectedCoveredLifelines = new ArrayList<>(combinedFragment.getCovered());
        expectedCoveredLifelines.removeAll(lifelinesToDelete);
        expectedCoveredLifelines.removeAll(nestedCombinedFragment.getCovered());
        
        List<InteractionFragment> nestedFragmentsToDelete = new ArrayList<>();
        nestedFragmentsToDelete.addAll(nestedCombinedFragment.getOperands().get(0).getFragments());
        nestedFragmentsToDelete.addAll(nestedCombinedFragment.getOperands().get(1).getFragments());
        
        Set<Message> messagesToDelete = MessageViewTestUtil.getAffectedMessages(fragmentsToDelete);
        FragmentContainer container = sendEvent.getContainer();
        
        stateHelper.saveState(container);
        
        controller.removeMessages(interaction, sendEvent.getMessage());
        
        assertFragmentsDeleted(container, fragmentsToDelete);
        assertFragmentsExactlyInOrder(container, stateHelper.previousFragments, fragmentsToDelete);
        assertMessagesDeleted(messagesToDelete);
        assertLifelinesDeleted(lifelinesToDelete);
        
        assertThat(operand.getFragments()).isEmpty();
        assertThat(combinedFragment.getCovered()).containsOnlyElementsOf(expectedCoveredLifelines);
        assertThat(nestedCombinedFragment.getCovered()).isEmpty();
    }

    /**
     * TODO: Move these two to FragmentsControllerTest?
     */
    @Test
    public void testRemoveCombinedFragment_ReplyMessageRemoved() {
        loadMessageView("Callee", "defined");
        
        CombinedFragment combinedFragment = (CombinedFragment) interaction.getFragments().get(1);
        InteractionOperand operand = combinedFragment.getOperands().get(0);
        MessageOccurrenceSpecification sendEvent = (MessageOccurrenceSpecification) operand.getFragments().get(0);
        Message message = sendEvent.getMessage();
        
        assertThat(message.getMessageSort()).isEqualTo(MessageSort.REPLY);
        
        InteractionOperand operand2 = combinedFragment.getOperands().get(1);
        Message message2 = ((MessageEnd) operand2.getFragments().get(0)).getMessage();
        
        assertThat(message2.getMessageSort()).isEqualTo(MessageSort.REPLY);
        
        List<InteractionFragment> fragmentsToDelete = new ArrayList<>();
        fragmentsToDelete.add(combinedFragment);
        Set<Message> messagesToDelete = MessageViewTestUtil.getAffectedMessages(fragmentsToDelete);
        FragmentContainer container = combinedFragment.getContainer();
        
        List<Gate> expectedGates = new ArrayList<>(interaction.getFormalGates());
        expectedGates.remove(message.getReceiveEvent());
        expectedGates.remove(message2.getReceiveEvent());
        
        List<InteractionFragment> nestedFragmentsToDelete1 = new ArrayList<>(operand.getFragments());
        List<InteractionFragment> nestedFragmentsToDelete2 = new ArrayList<>(operand2.getFragments());
        Set<Message> nestedMessagesToDelete = MessageViewTestUtil.getAffectedMessages(nestedFragmentsToDelete1);
        nestedMessagesToDelete.addAll(MessageViewTestUtil.getAffectedMessages(nestedFragmentsToDelete2));
        
        stateHelper.saveState(container);
        
        ControllerFactory.INSTANCE.getFragmentsController().removeInteractionFragment(combinedFragment);
        
        assertFragmentsDeleted(container, fragmentsToDelete);
        assertFragmentsExactlyInOrder(container, stateHelper.previousFragments, fragmentsToDelete);
        assertMessagesDeleted(messagesToDelete);
        assertLifelinesUnchanged();
        
        assertFragmentsDeleted(operand, nestedFragmentsToDelete1);
        assertFragmentsDeleted(operand2, nestedFragmentsToDelete2);
        assertMessagesDeleted(nestedMessagesToDelete);
        
        // Check that the correct Gates were removed.
        assertThat(interaction.getFormalGates()).containsOnlyElementsOf(expectedGates);       
    }

    @Test
    public void testRemoveOperand_ReplyMessageRemoved() {
        loadMessageView("Callee", "defined");
        
        CombinedFragment combinedFragment = (CombinedFragment) interaction.getFragments().get(1);
        InteractionOperand operand = combinedFragment.getOperands().get(1);
        MessageOccurrenceSpecification sendEvent = (MessageOccurrenceSpecification) operand.getFragments().get(0);
        Message message = sendEvent.getMessage();
        
        assertThat(message.getMessageSort()).isEqualTo(MessageSort.REPLY);
        
        List<InteractionFragment> fragmentsToDelete = new ArrayList<>(operand.getFragments());
        
        List<Gate> expectedGates = new ArrayList<>(interaction.getFormalGates());
        expectedGates.remove(message.getReceiveEvent());
        
        Set<Message> messagesToDelete = MessageViewTestUtil.getAffectedMessages(fragmentsToDelete);
        messagesToDelete.addAll(MessageViewTestUtil.getAffectedMessages(fragmentsToDelete));
        
        stateHelper.saveState(operand);
        
        ControllerFactory.INSTANCE.getFragmentsController().removeInteractionOperand(operand);
        
        assertFragmentsDeleted(operand, fragmentsToDelete);
        assertFragmentsRemovedFromLifelines(fragmentsToDelete);
        assertMessagesDeleted(messagesToDelete);
        assertLifelinesUnchanged();
        
        // Check that the correct Gates were removed.
        assertThat(interaction.getFormalGates()).containsOnlyElementsOf(expectedGates);
        assertThat(operand.eContainer()).isNull();
        assertThat(combinedFragment.getOperands()).doesNotContain(operand);
    }

    private void assertLifelinesUnchanged() {
        assertThat(interaction.getLifelines()).containsOnlyElementsOf(stateHelper.previousLifelines);
    }

    private void assertLifelinesDeleted(List<Lifeline> lifelinesToDelete) {
        List<Lifeline> expectedLifelines = new ArrayList<>(interaction.getLifelines());
        expectedLifelines.removeAll(lifelinesToDelete);
        
        assertThat(interaction.getLifelines()).containsOnlyElementsOf(expectedLifelines);
        
        ContainerMapImpl layout = EMFModelUtil.getEntryFromMap(aspect.getLayout().getContainers(), messageView);
        assertThat(layout.getValue().keySet()).as("Lifeline's layout not removed").containsOnlyElementsOf(expectedLifelines);
    }

    private void assertFragmentsDeleted(FragmentContainer container, List<InteractionFragment> fragmentsToDelete) {
        assertSameContainer(fragmentsToDelete);
        
        for (InteractionFragment fragment : fragmentsToDelete) {
            /**
             * The eContainer() could be not null (if the container was deleted too),
             * therefore check against eResource(), which is null if it's not contained anymore.                    
             */
            assertThat(fragment.eResource()).as("Fragment or its container was not removed").isNull();
        }
        
        assertFragmentsRemovedFromLifelines(fragmentsToDelete);
    }

    private void assertFragmentsExactlyInOrder(FragmentContainer container, 
            List<InteractionFragment> previousFragments,
            List<InteractionFragment> fragmentsToDelete) {
        List<InteractionFragment> expectedFragments = new ArrayList<>(previousFragments);
        expectedFragments.removeAll(fragmentsToDelete);
        
        TestUtil.assertContainsOnlyElementsExactlyOf(container.getFragments(), expectedFragments);
    }

    private void assertFragmentsRemovedFromLifelines(List<InteractionFragment> fragmentsToDelete) {
        for (Lifeline lifeline : interaction.getLifelines()) {
            assertThat(lifeline.getCoveredBy()).as("Fragments not removed from lifeline's coveredBy").doesNotContainAnyElementsOf(fragmentsToDelete);
       }
    }

    private void assertSameContainer(List<InteractionFragment> fragmentsToDelete) {
        FragmentContainer container = null;
        
        for (InteractionFragment fragment : fragmentsToDelete) {
            if (container == null) {
                container = fragment.getContainer();
            } else if (container != fragment.getContainer()) {
                fail("Fragments from multiple containers supplied, which is unable to be checked.");
            }
        }
    }

    private void assertMessagesDeleted(Set<Message> messagesToDelete) {
        List<Message> expectedMessages = new ArrayList<>(interaction.getMessages());
        expectedMessages.removeAll(messagesToDelete);
        
        assertThat(interaction.getMessages()).containsOnlyElementsOf(expectedMessages);
        
        for (Message message : messagesToDelete) {
            assertThat(message.eContainer()).as("Message was not removed").isNull();
        }
    }

    /**
     * Test that temporary property is not deleted, because it is used by an assignment statement.
     */
    @Test
    public void testRemoveMessages_NestedBehaviourWithAssignmentStatement_UsedAssignToNotDeleted() {
        loadMessageView("Caller", "testMessageRemoval");
        
        MessageOccurrenceSpecification sendEvent = (MessageOccurrenceSpecification) interaction.getFragments().get(5);
        
        Message message = sendEvent.getMessage();
        StructuralFeature messageAssignTo = message.getAssignTo();
        assertThat(messageAssignTo).as("Message needs an assignTo").isNotNull();
        
        controller.removeMessages(interaction, sendEvent.getMessage());
        
        // Check structural feature of message for existence.
        assertThat(messageAssignTo.eContainer()).isNotNull();
    }
    
    /**
     * Test that temporary property is deleted, because it is not used by an assignment statement.
     */
    @Test
    public void testRemoveMessages_MessageWithAssignment_UnusedAssignToDeleted() {
        loadMessageView("Caller", "testMessageRemoval");
        
        MessageOccurrenceSpecification sendEvent = (MessageOccurrenceSpecification) interaction.getFragments().get(1);
        
        Message message = sendEvent.getMessage();
        StructuralFeature messageAssignTo = message.getAssignTo();
        assertThat(messageAssignTo).as("Message needs an assignTo").isNotNull();
        
        controller.removeMessages(interaction, sendEvent.getMessage());
        
        // Check structural feature of message for existence.
        assertThat(messageAssignTo.eContainer()).isNull();
    }
    
    /**
     * Test that assignTo is not deleted, because it is a structural feature.
     */
    @Test
    public void testRemoveMessages_MessageWithAssignTo_StructuralFeatureNotDeleted() {
        loadMessageView("Caller", "testMessageRemoval");
        
        MessageOccurrenceSpecification sendEvent = (MessageOccurrenceSpecification) interaction.getFragments().get(18);
        
        Message message = sendEvent.getMessage();
        StructuralFeature messageAssignTo = message.getAssignTo();
        assertThat(messageAssignTo).as("Message needs an assignTo").isNotNull();
        
        controller.removeMessages(interaction, sendEvent.getMessage());
        
        // Check structural feature of message for existence.
        assertThat(messageAssignTo.eContainer()).isNotNull();
    }
    
    /**
     * Test that temporary property is deleted, because it is only used by the message itself.
     */
    @Test
    public void testRemoveMessages_NestedMessageWithAssignment_UnusedAssignToDeleted() {
        loadMessageView("Caller", "testMessageRemoval");
        
        MessageOccurrenceSpecification sendEvent = (MessageOccurrenceSpecification) interaction.getFragments().get(7);
        
        Message message = sendEvent.getMessage();
        StructuralFeature messageAssignTo = message.getAssignTo();
        assertThat(messageAssignTo).as("Message needs an assignTo").isNotNull();
        
        controller.removeMessages(interaction, sendEvent.getMessage());
        
        // Check structural feature of message for existence.
        assertThat(messageAssignTo.eContainer()).isNull();
    }

    /**
     * Test that assignment statement inside a combined fragment (inside nested behaviour) is deleted properly.
     */
    @Test
    public void testRemoveMessages_NestedCombinedFragmentWithAssignmentStatement_StatementAndUnusedAssignToDeleted() {
        loadMessageView("Caller", "testMessageRemoval");
        
        MessageOccurrenceSpecification sendEvent = (MessageOccurrenceSpecification) interaction.getFragments().get(5);
        
        CombinedFragment combinedFragment = (CombinedFragment) interaction.getFragments().get(9);
        InteractionOperand operand = combinedFragment.getOperands().get(0);
        AssignmentStatement assignment = (AssignmentStatement) operand.getFragments().get(0);
        Lifeline assignmentLifeline = assignment.getCovered().get(0);
        
        StructuralFeature structuralFeature = assignment.getAssignTo();
        assertThat(structuralFeature).as("AssignmentStatement needs an assignTo").isNotNull();
        
        controller.removeMessages(interaction, sendEvent.getMessage());
        
        // Check AssignmentStatement.
        assertThat(structuralFeature.eContainer()).isNull();
        assertThat(assignmentLifeline.getCoveredBy()).doesNotContain(assignment);
    }

}
