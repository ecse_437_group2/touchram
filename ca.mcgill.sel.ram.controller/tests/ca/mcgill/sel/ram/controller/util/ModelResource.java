package ca.mcgill.sel.ram.controller.util;

import org.eclipse.emf.ecore.EObject;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import ca.mcgill.sel.commons.emf.util.AdapterFactoryRegistry;
import ca.mcgill.sel.commons.emf.util.ResourceManager;
import ca.mcgill.sel.ram.RamPackage;
import ca.mcgill.sel.ram.provider.RamItemProviderAdapterFactory;
import ca.mcgill.sel.ram.util.RamResourceFactoryImpl;

/**
 * A base class for rules which loads and unloads model resources before and after tests.
 * 
 * Users need to provide the model filename and are informed using the call back to {@link #modelLoaded(EObject)}
 * once the model was successfully loaded.
 * 
 * In order to use a specific model for tests, the test needs to be annotated with the {@link Model} annotation.
 * Otherwise, the given default model is used.
 * 
 * @author mschoettle
 *
 */
public abstract class ModelResource implements TestRule {
    
    private EObject currentModel;
    private Model model;
    private String defaultModelFileName;
    
    /**
     * Creates a new instance with the given file name as the default model for all tests.
     * Initializes required resources.
     * 
     * @param defaultModelFileName the default file name of the model
     */
    public ModelResource(String defaultModelFileName) {
        this.defaultModelFileName = defaultModelFileName;
        
        initialize();
    }
    
    /**
     * Initializes a default resource manager and resource factories.
     */
    protected void initialize() {
        // Initialize ResourceManager.
        ResourceManager.initialize();
        // Initialize packages.
        RamPackage.eINSTANCE.eClass();
        
        // Register resource factories.
        ResourceManager.registerExtensionFactory("ram", new RamResourceFactoryImpl());

        // Initialize adapter factories.
        AdapterFactoryRegistry.INSTANCE.addAdapterFactory(RamItemProviderAdapterFactory.class);
    }
    
    @Override
    public Statement apply(final Statement base, final Description description) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                before(description);
                try {
                    base.evaluate();
                } finally {
                    after();
                }
            }
        };
    }
    
    /**
     * Notifies that the model has been loaded.
     * 
     * @param model the model object which has been loaded
     */
    public abstract void modelLoaded(EObject model);
    
    /**
     * Loads the model.
     * Either loads the default model file name, or a specific model, if the current test is providing one.
     * 
     * @param description A {@link Description} of the current test
     */
    private void before(Description description) {
        model = description.getAnnotation(Model.class);
        
        String modelFileName = getModelFileName();
        currentModel = ResourceManager.loadModel(modelFileName);
        
        if (currentModel != null) {
            modelLoaded(currentModel);
        }
    }
    
    /**
     * Returns the current model file name.
     * If the current executed test is annotated with {@link Model}, the provided file name is returned.
     * Otherwise, the default file name is returned.
     * 
     * @return the file name of the model
     */
    private String getModelFileName() { 
        if (model != null) {
            return model.fileName();
        }
        
        return defaultModelFileName;
    }
    
    /**
     * Unloads the currently loaded model.
     */
    private void after() {
        if (currentModel != null) {
            ResourceManager.unloadResource(currentModel.eResource());
            currentModel = null;
        }
    }

}
