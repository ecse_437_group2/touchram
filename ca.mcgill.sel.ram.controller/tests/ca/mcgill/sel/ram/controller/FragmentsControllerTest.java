package ca.mcgill.sel.ram.controller;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.junit.Rule;
import org.junit.Test;

import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.AssignmentStatement;
import ca.mcgill.sel.ram.Attribute;
import ca.mcgill.sel.ram.Classifier;
import ca.mcgill.sel.ram.CombinedFragment;
import ca.mcgill.sel.ram.FragmentContainer;
import ca.mcgill.sel.ram.Interaction;
import ca.mcgill.sel.ram.InteractionFragment;
import ca.mcgill.sel.ram.InteractionOperand;
import ca.mcgill.sel.ram.Lifeline;
import ca.mcgill.sel.ram.Message;
import ca.mcgill.sel.ram.MessageView;
import ca.mcgill.sel.ram.PrimitiveType;
import ca.mcgill.sel.ram.Reference;
import ca.mcgill.sel.ram.StructuralFeature;
import ca.mcgill.sel.ram.TemporaryProperty;
import ca.mcgill.sel.ram.controller.util.MessageViewTestUtil;
import ca.mcgill.sel.ram.controller.util.Model;
import ca.mcgill.sel.ram.controller.util.ModelResource;
import ca.mcgill.sel.ram.controller.util.TestUtil;
import ca.mcgill.sel.ram.impl.ContainerMapImpl;
import ca.mcgill.sel.ram.util.MessageViewUtil;

public class FragmentsControllerTest {

    @Rule
    public final ModelResource modelResource = new ModelResource("tests/models/MessageViews/Fragments.ram") {

        @Override
        public void modelLoaded(EObject model) {
            aspect = (Aspect) model;
        }

    };

    private static FragmentsController controller = ControllerFactory.INSTANCE.getFragmentsController();

    private Aspect aspect;
    private Classifier classifier;
    private MessageView messageView;
    private Interaction interaction;

    private void loadMessageView(String className, String operationName) {
        messageView = MessageViewTestUtil.loadMessageView(aspect, className, operationName);
        classifier = (Classifier) messageView.getSpecifies().eContainer();
        interaction = messageView.getSpecification();
    }

    /**
     * Test that assignment statement is properly deleted.
     */
    @Test
    public void testRemoveInteractionFragment_AssignmentStatementDeleted() {
        loadMessageView("OtherClass", "testAssignmentStatement");

        AssignmentStatement assignment = (AssignmentStatement) interaction.getFragments().get(1);
        Lifeline lifeline = assignment.getCovered().get(0);

        List<InteractionFragment> expectedFragments = new ArrayList<>(interaction.getFragments());
        expectedFragments.remove(assignment);

        controller.removeInteractionFragment(assignment);

        TestUtil.assertContainsOnlyElementsExactlyOf(interaction.getFragments(), expectedFragments);
        assertThat(assignment.eContainer()).isNull();
        assertThat(lifeline.getCoveredBy()).doesNotContain(assignment);
    }

    /**
     * Test that temporary property (assignTo) is not deleted when used by another AssignmentStatement.
     */
    @Test
    public void testRemoveInteractionFragment_AssignmentStatement_AssignToUsedByAssignmentNotDeleted() {
        loadMessageView("OtherClass", "testAssignmentStatement");

        AssignmentStatement assignment = (AssignmentStatement) interaction.getFragments().get(1);
        StructuralFeature previousAssignTo = assignment.getAssignTo();

        controller.removeInteractionFragment(assignment);

        assertThat(assignment.eContainer()).isNull();
        assertThat(previousAssignTo.eContainer()).isNotNull();
    }

    /**
     * Test that temporary property (assignTo) is not deleted when used by a message (assignTo).
     */
    @Test
    public void testRemoveInteractionFragment_AssignmentStatement_AssignToUsedByMessageNotDeleted() {
        loadMessageView("OtherClass", "testAssignmentStatement");

        AssignmentStatement assignment = (AssignmentStatement) interaction.getFragments().get(10);
        StructuralFeature previousAssignTo = assignment.getAssignTo();

        controller.removeInteractionFragment(assignment);

        assertThat(assignment.eContainer()).isNull();
        assertThat(previousAssignTo.eContainer()).isNotNull();
    }

    /**
     * Test that temporary property (assignTo) is deleted when not used by any other object.
     */
    @Test
    public void testRemoveInteractionFragment_AssignmentStatement_UnusedAssignToDeleted() {
        loadMessageView("OtherClass", "testAssignmentStatement");

        AssignmentStatement assignment = (AssignmentStatement) interaction.getFragments().get(11);
        StructuralFeature previousAssignTo = assignment.getAssignTo();

        controller.removeInteractionFragment(assignment);

        assertThat(assignment.eContainer()).isNull();
        assertThat(previousAssignTo.eContainer()).isNull();
    }

    /**
     * Test that structural feature (e.g., AssociationEnd) from structural view is not deleted.
     */
    @Test
    public void testRemoveInteractionFragment_AssignmentStatement_StructuralFeatureNotDeleted() {
        loadMessageView("OtherClass", "testAssignmentStatement");

        AssignmentStatement assignment = (AssignmentStatement) interaction.getFragments().get(12);
        StructuralFeature previousAssignTo = assignment.getAssignTo();

        controller.removeInteractionFragment(assignment);

        assertThat(assignment.eContainer()).isNull();
        assertThat(previousAssignTo.eContainer()).isNotNull();
    }

    /**
     * Test that AssignmentStatement and temporary property (assignTo) is deleted when located inside a
     * CombinedFragment.
     */
    @Test
    public void testRemoveInteractionFragment_AssignmentStatementInsideCombinedFragmentDeleted() {
        loadMessageView("OtherClass", "testAssignmentStatement");

        CombinedFragment fragment = (CombinedFragment) interaction.getFragments().get(13);
        AssignmentStatement assignment = (AssignmentStatement) fragment.getOperands().get(0).getFragments().get(0);
        StructuralFeature previousAssignTo = assignment.getAssignTo();

        Lifeline lifeline = assignment.getCovered().get(0);
        List<InteractionFragment> expectedCoveredBy = new ArrayList<>(lifeline.getCoveredBy());
        expectedCoveredBy.remove(fragment);
        expectedCoveredBy.remove(assignment);

        controller.removeInteractionFragment(fragment);

        assertThat(fragment.eContainer()).isNull();
        assertThat(assignment.eContainer().eContainer()).isNull();
        assertThat(previousAssignTo.eContainer()).isNull();
        assertThat(lifeline.getCoveredBy()).containsOnlyElementsOf(expectedCoveredBy);
    }

    /**
     * Test that when removing a CombinedFragment (containing another combined fragments) all its content are removed.
     */
    @Test
    public void testRemoveInteractionFragment_CombinedFragment_ContainedContentsRemoved() {
        loadMessageView("OtherClass", "testCombinedFragments");

        CombinedFragment combinedFragment = (CombinedFragment) interaction.getFragments().get(3);

        List<InteractionFragment> fragmentsToDelete = Collections.<InteractionFragment> singletonList(combinedFragment);
        List<InteractionFragment> expectedFragments = new ArrayList<>(interaction.getFragments());
        expectedFragments.removeAll(fragmentsToDelete);

        List<Message> expectedMessages = new ArrayList<>(interaction.getMessages());
        expectedMessages.removeAll(MessageViewTestUtil.getAffectedMessages(fragmentsToDelete));

        List<Lifeline> expectedLifelines = new ArrayList<>(interaction.getLifelines());
        expectedLifelines.remove(MessageViewTestUtil.getLifelineByName(interaction, "mySuperClass"));
        expectedLifelines.remove(MessageViewTestUtil.getLifelineByName(interaction, "myOtherClass1"));

        Lifeline sourceLifeline = MessageViewTestUtil.getLifelineByName(interaction, "target");
        Lifeline affectedLifeline = MessageViewTestUtil.getLifelineByName(interaction, "mySuperClass1");

        List<InteractionFragment> sourceCoveredBy = new ArrayList<>(sourceLifeline.getCoveredBy());
        sourceCoveredBy.removeAll(fragmentsToDelete);
        sourceCoveredBy.removeAll(combinedFragment.getOperands().get(0).getFragments());

        List<InteractionFragment> affectedCoveredBy = Collections.singletonList(interaction.getFragments().get(2));

        controller.removeInteractionFragment(combinedFragment);

        TestUtil.assertContainsOnlyElementsExactlyOf(interaction.getFragments(), expectedFragments);
        assertThat(combinedFragment.eContainer()).isNull();

        assertThat(sourceLifeline.getCoveredBy()).containsOnlyElementsOf(sourceCoveredBy);
        assertThat(affectedLifeline.getCoveredBy()).containsOnlyElementsOf(affectedCoveredBy);

        assertThat(interaction.getMessages()).containsOnlyElementsOf(expectedMessages);
        assertThat(interaction.getLifelines()).containsOnlyElementsOf(expectedLifelines);
        ContainerMapImpl layout = EMFModelUtil.getEntryFromMap(aspect.getLayout().getContainers(), messageView);
        assertThat(layout.getValue().keySet()).containsOnlyElementsOf(expectedLifelines);
    }

    /**
     * Tests temporary property removal with two assignment statements for the same property that will both be deleted.
     */
    @Test
    public void testRemoveInteractionFragment_CombinedFragment_TemporaryPropertyRemoved() {
        loadMessageView("OtherClass", "testAssignmentStatement");

        CombinedFragment combinedFragment = (CombinedFragment) interaction.getFragments().get(7);
        InteractionOperand operand = combinedFragment.getOperands().get(0);

        AssignmentStatement statement = (AssignmentStatement) operand.getFragments().get(0);
        StructuralFeature property = statement.getAssignTo();

        controller.removeInteractionFragment(combinedFragment);

        assertThat(property.eContainer()).isNull();
    }

    /**
     * Test that when removing a CombinedFragment (inside another combined fragments) all its content are removed.
     */
    @Test
    public void testRemoveInteractionFragment_NestedCombinedFragmentAndContentsRemoved() {
        loadMessageView("OtherClass", "testCombinedFragments");

        CombinedFragment combinedFragment = (CombinedFragment) interaction.getFragments().get(3);
        InteractionOperand operand = combinedFragment.getOperands().get(0);
        CombinedFragment nestedCombinedFragment = (CombinedFragment) operand.getFragments().get(2);

        Lifeline sourceLifeline = MessageViewTestUtil.getLifelineByName(interaction, "mySuperClass");
        Lifeline affectedLifeline = MessageViewTestUtil.getLifelineByName(interaction, "mySuperClass1");

        List<InteractionFragment> fragmentsToDelete =
                Collections.<InteractionFragment> singletonList(nestedCombinedFragment);
        List<InteractionFragment> expectedFragments = new ArrayList<>(operand.getFragments());
        expectedFragments.removeAll(fragmentsToDelete);

        List<Message> expectedMessages = new ArrayList<>(interaction.getMessages());
        expectedMessages.removeAll(MessageViewTestUtil.getAffectedMessages(fragmentsToDelete));

        List<Lifeline> expectedLifelines = new ArrayList<>(interaction.getLifelines());
        expectedLifelines.remove(MessageViewTestUtil.getLifelineByName(interaction, "myOtherClass1"));

        List<InteractionFragment> sourceCoveredBy = new ArrayList<>(sourceLifeline.getCoveredBy());
        sourceCoveredBy.remove(nestedCombinedFragment);
        sourceCoveredBy.removeAll(nestedCombinedFragment.getOperands().get(0).getFragments());
        sourceCoveredBy.removeAll(nestedCombinedFragment.getOperands().get(1).getFragments());

        List<Lifeline> expectedCovered = new ArrayList<>(combinedFragment.getCovered());
        expectedCovered.remove(affectedLifeline);
        expectedCovered.remove(MessageViewTestUtil.getLifelineByName(interaction, "myOtherClass1"));

        List<InteractionFragment> affectedCoveredBy = Collections.singletonList(interaction.getFragments().get(2));

        controller.removeInteractionFragment(nestedCombinedFragment);

        TestUtil.assertContainsOnlyElementsExactlyOf(operand.getFragments(), expectedFragments);
        assertThat(nestedCombinedFragment.eContainer()).isNull();

        assertThat(sourceLifeline.getCoveredBy()).containsOnlyElementsOf(sourceCoveredBy);
        assertThat(affectedLifeline.getCoveredBy()).containsOnlyElementsOf(affectedCoveredBy);
        assertThat(combinedFragment.getCovered()).containsOnlyElementsOf(expectedCovered);

        assertThat(interaction.getMessages()).containsOnlyElementsOf(expectedMessages);
        assertThat(interaction.getLifelines()).containsOnlyElementsOf(expectedLifelines);
        ContainerMapImpl layout = EMFModelUtil.getEntryFromMap(aspect.getLayout().getContainers(), messageView);
        assertThat(layout.getValue().keySet()).containsOnlyElementsOf(expectedLifelines);
    }

    /**
     * Test that when removing an operand (inside nested combined fragments) all its content are removed.
     * This includes a lifeline that is not used anywhere outside and should be removed.
     */
    @Test
    public void testRemoveInteractionOperand_NestedCombinedFragment_ContentsAndLifelineRemoved() {
        loadMessageView("OtherClass", "testCombinedFragments");

        CombinedFragment combinedFragment = (CombinedFragment) interaction.getFragments().get(3);
        InteractionOperand operand = combinedFragment.getOperands().get(0);
        CombinedFragment nestedCombinedFragment = (CombinedFragment) operand.getFragments().get(2);
        InteractionOperand expectedOperand = nestedCombinedFragment.getOperands().get(0);
        InteractionOperand nestedOperand = nestedCombinedFragment.getOperands().get(1);

        List<InteractionFragment> fragmentsToDelete = new ArrayList<>(nestedOperand.getFragments());

        List<Message> expectedMessages = new ArrayList<>(interaction.getMessages());
        expectedMessages.removeAll(MessageViewTestUtil.getAffectedMessages(fragmentsToDelete));

        List<Lifeline> expectedLifelines = new ArrayList<>(interaction.getLifelines());
        Lifeline lifelineToDelete = MessageViewTestUtil.getLifelineByName(interaction, "myOtherClass1");
        expectedLifelines.remove(lifelineToDelete);

        List<Lifeline> expectedCovered = new ArrayList<>(combinedFragment.getCovered());
        expectedCovered.remove(lifelineToDelete);

        List<Lifeline> expectedNestedCovered = new ArrayList<>(nestedCombinedFragment.getCovered());
        expectedNestedCovered.remove(lifelineToDelete);

        controller.removeInteractionOperand(nestedOperand);

        assertThat(nestedCombinedFragment.getOperands())
                .hasSize(1)
                .containsOnly(expectedOperand);
        assertThat(nestedOperand.eContainer()).isNull();

        assertThat(interaction.getMessages()).containsOnlyElementsOf(expectedMessages);
        assertThat(interaction.getLifelines()).containsOnlyElementsOf(expectedLifelines);
        ContainerMapImpl layout = EMFModelUtil.getEntryFromMap(aspect.getLayout().getContainers(), messageView);
        assertThat(layout.getValue().keySet()).containsOnlyElementsOf(expectedLifelines);

        assertThat(combinedFragment.getCovered()).containsOnlyElementsOf(expectedCovered);
        assertThat(nestedCombinedFragment.getCovered()).containsOnlyElementsOf(expectedNestedCovered);
    }

    /**
     * Test that when removing an operand (inside nested combined fragments) all its content are removed.
     * This excludes the lifeline that is used somewhere outside and should not be removed,
     * but needs to be removed from the combined fragment.
     */
    @Test
    public void testRemoveInteractionOperand_NestedCombinedFragment_ContentsAndLifelineRemovedFromCombinedFragment() {
        loadMessageView("OtherClass", "testCombinedFragments");

        CombinedFragment combinedFragment = (CombinedFragment) interaction.getFragments().get(3);
        InteractionOperand operand = combinedFragment.getOperands().get(0);
        CombinedFragment nestedCombinedFragment = (CombinedFragment) operand.getFragments().get(2);
        InteractionOperand nestedOperand = nestedCombinedFragment.getOperands().get(0);
        InteractionOperand expectedOperand = nestedCombinedFragment.getOperands().get(1);

        List<InteractionFragment> fragmentsToDelete = new ArrayList<>(nestedOperand.getFragments());

        List<Message> expectedMessages = new ArrayList<>(interaction.getMessages());
        expectedMessages.removeAll(MessageViewTestUtil.getAffectedMessages(fragmentsToDelete));

        List<Lifeline> expectedLifelines = new ArrayList<>(interaction.getLifelines());

        Lifeline affectedLifeline = MessageViewTestUtil.getLifelineByName(interaction, "mySuperClass1");
        List<Lifeline> expectedCovered = new ArrayList<>(combinedFragment.getCovered());
        expectedCovered.remove(affectedLifeline);

        List<Lifeline> expectedNestedCovered = new ArrayList<>(nestedCombinedFragment.getCovered());
        expectedNestedCovered.remove(affectedLifeline);

        List<InteractionFragment> expectedCoveredBy =
                Collections.<InteractionFragment> singletonList(interaction.getFragments().get(2));

        controller.removeInteractionOperand(nestedOperand);

        assertThat(nestedCombinedFragment.getOperands())
                .hasSize(1)
                .containsOnly(expectedOperand);
        assertThat(nestedOperand.eContainer()).isNull();

        assertThat(interaction.getMessages()).containsOnlyElementsOf(expectedMessages);
        assertThat(interaction.getLifelines()).containsOnlyElementsOf(expectedLifelines);
        ContainerMapImpl layout = EMFModelUtil.getEntryFromMap(aspect.getLayout().getContainers(), messageView);
        assertThat(layout.getValue().keySet()).containsOnlyElementsOf(expectedLifelines);

        assertThat(combinedFragment.getCovered()).containsOnlyElementsOf(expectedCovered);
        assertThat(nestedCombinedFragment.getCovered()).containsOnlyElementsOf(expectedNestedCovered);
        assertThat(affectedLifeline.getCoveredBy()).containsOnlyElementsOf(expectedCoveredBy);
    }

    /**
     * Test that AssignmentStatement and temporary property (assignTo) is deleted when located inside an
     * InteractionOperand.
     */
    @Test
    public void testRemoveInteractionOperand_AssignmentStatement_UnusedAssignToDeleted() {
        loadMessageView("OtherClass", "testAssignmentStatement");

        CombinedFragment fragment = (CombinedFragment) interaction.getFragments().get(13);
        InteractionOperand operand = fragment.getOperands().get(0);
        AssignmentStatement assignment = (AssignmentStatement) operand.getFragments().get(0);
        StructuralFeature previousAssignTo = assignment.getAssignTo();

        controller.removeInteractionOperand(operand);

        assertThat(operand.eContainer()).isNull();
        assertThat(assignment.getCovered()).isEmpty();
        assertThat(fragment.getOperands()).isEmpty();
        assertThat(previousAssignTo.eContainer()).isNull();
    }
    
    @Test 
    public void testRemoveInteractionOperand_EmptyOperand() {
        loadMessageView("OtherClass", "testCombinedFragments");

        CombinedFragment combinedFragment = (CombinedFragment) interaction.getFragments().get(4);
        InteractionOperand operand = combinedFragment.getOperands().get(1);
        
        List<InteractionOperand> operands = new ArrayList<>(combinedFragment.getOperands());
        operands.remove(operand);
        
        controller.removeInteractionOperand(operand);
        
        TestUtil.assertContainsOnlyElementsExactlyOf(combinedFragment.getOperands(), operands);
        assertThat(operand.eContainer()).isNull();
    }

    /**
     * Test proper creation of temporary property (Attribute) with no previous assign to.
     */
    @Test
    public void testCreateTemporaryAssignment_InitialCreation_AttributeCreated() {
        loadMessageView("OtherClass", "testAssignmentStatement");

        AssignmentStatement assignment = (AssignmentStatement) interaction.getFragments().get(14);
        Message initialMessage = MessageViewUtil.findInitialMessage(assignment);
        StructuralFeature previousAssignTo = assignment.getAssignTo();
        assertThat(previousAssignTo).isNull();

        List<TemporaryProperty> localProperties = new ArrayList<>(initialMessage.getLocalProperties());

        PrimitiveType type = (PrimitiveType) aspect.getStructuralView().getTypes().get(2);
        String name = "assignmentname";
        controller.createTemporaryAssignment(assignment, name, type);

        StructuralFeature newAssignTo = assignment.getAssignTo();

        assertThat(newAssignTo).isNotEqualTo(previousAssignTo);
        assertThat(newAssignTo).isInstanceOf(Attribute.class);

        localProperties.add((Attribute) newAssignTo);
        assertThat(initialMessage.getLocalProperties()).containsOnlyElementsOf(localProperties);

        assertThat(newAssignTo.getName()).isEqualTo(name);
        assertThat(newAssignTo.getType()).isEqualTo(type);
    }

    /**
     * Test proper creation of temporary property (Reference).
     */
    @Test
    public void testCreateAndAddTemporaryProperty_ReferenceCreated() {
        loadMessageView("OtherClass", "testAssignmentStatement");

        AssignmentStatement assignment = (AssignmentStatement) interaction.getFragments().get(14);
        Message initialMessage = MessageViewUtil.findInitialMessage(assignment);

        List<TemporaryProperty> localProperties = new ArrayList<>(initialMessage.getLocalProperties());

        Classifier type = MessageViewTestUtil.getClassifierByName(aspect, "OtherClass");
        String name = "assignmentname";
        controller.createAndAddTemporaryProperty(initialMessage, name, type);

        List<TemporaryProperty> newProperties = new ArrayList<>(initialMessage.getLocalProperties());
        newProperties.removeAll(localProperties);

        assertThat(newProperties).hasSize(1);
        assertThat(newProperties).hasOnlyElementsOfType(Reference.class);

        Reference newProperty = (Reference) newProperties.get(0);

        localProperties.add(newProperty);
        assertThat(initialMessage.getLocalProperties()).containsOnlyElementsOf(localProperties);

        assertThat(newProperty.getName()).isEqualTo(name);
        assertThat(newProperty.getType()).isEqualTo(type);
        assertThat(newProperty.isStatic()).isEqualTo(false);
        assertThat(newProperty.getLowerBound()).isEqualTo(1);
    }

    /**
     * Test that previous assignTo is replaced and not deleted (e.g., AssociationEnd).
     */
    @Test
    public void testCreateTemporaryAssignment_ExistingAssignToReplaced_StructuralFeatureNotDeleted() {
        loadMessageView("OtherClass", "testAssignmentStatement");

        AssignmentStatement assignment = (AssignmentStatement) interaction.getFragments().get(12);
        StructuralFeature previousAssignTo = assignment.getAssignTo();
        assertThat(previousAssignTo).isNotNull();

        Message initialMessage = MessageViewUtil.findInitialMessage(assignment);
        List<TemporaryProperty> localProperties = new ArrayList<>(initialMessage.getLocalProperties());

        PrimitiveType type = (PrimitiveType) aspect.getStructuralView().getTypes().get(2);
        String name = "assignmentname";
        controller.createTemporaryAssignment(assignment, name, type);

        StructuralFeature newAssignTo = assignment.getAssignTo();

        assertThat(previousAssignTo.eContainer()).isNotNull();
        assertThat(newAssignTo).isNotEqualTo(previousAssignTo);
        assertThat(newAssignTo).isInstanceOf(Attribute.class);

        localProperties.add((Attribute) newAssignTo);
        assertThat(initialMessage.getLocalProperties()).containsOnlyElementsOf(localProperties);
    }

    /**
     * Test that previous assignTo is replaced and deleted (temporary property).
     */
    @Test
    public void testCreateTemporaryAssignment_ExistingAssignToReplacedAndDeleted() {
        loadMessageView("OtherClass", "testAssignmentStatement");

        AssignmentStatement assignment = (AssignmentStatement) interaction.getFragments().get(11);
        TemporaryProperty previousAssignTo = (TemporaryProperty) assignment.getAssignTo();

        Message initialMessage = MessageViewUtil.findInitialMessage(assignment);
        List<TemporaryProperty> localProperties = new ArrayList<>(initialMessage.getLocalProperties());
        localProperties.remove(previousAssignTo);

        PrimitiveType type = (PrimitiveType) aspect.getStructuralView().getTypes().get(2);
        String name = "assignmentname";
        controller.createTemporaryAssignment(assignment, name, type);

        StructuralFeature newAssignTo = assignment.getAssignTo();

        assertThat(previousAssignTo.eContainer()).isNull();
        assertThat(newAssignTo).isNotEqualTo(previousAssignTo);

        localProperties.add((Attribute) newAssignTo);
        assertThat(initialMessage.getLocalProperties()).containsOnlyElementsOf(localProperties);
    }

    /**
     * Test that previous assignTo is replaced and previous (temporary) one not deleted because it is used by message.
     */
    @Test
    public void testCreateTemporaryAssignment_ExistingAssignToReplaced_UsedByMessageAndNotDeleted() {
        loadMessageView("OtherClass", "testAssignmentStatement");

        AssignmentStatement assignment = (AssignmentStatement) interaction.getFragments().get(10);
        StructuralFeature previousAssignTo = assignment.getAssignTo();

        Message initialMessage = MessageViewUtil.findInitialMessage(assignment);
        List<TemporaryProperty> localProperties = new ArrayList<>(initialMessage.getLocalProperties());

        PrimitiveType type = (PrimitiveType) aspect.getStructuralView().getTypes().get(2);
        String name = "assignmentname";
        controller.createTemporaryAssignment(assignment, name, type);

        StructuralFeature newAssignTo = assignment.getAssignTo();

        assertThat(previousAssignTo.eContainer()).isNotNull();
        assertThat(newAssignTo).isNotEqualTo(previousAssignTo);

        localProperties.add((Attribute) newAssignTo);
        assertThat(initialMessage.getLocalProperties()).containsOnlyElementsOf(localProperties);
    }

    /**
     * Test that previous assignTo is replaced and previous (temporary) one not deleted because it is by another
     * AssignmentStatement.
     */
    @Test
    public void testCreateTemporaryAssignment_ExistingAssignToReplaced_UsedByAssignmentAndNotDeleted() {
        loadMessageView("OtherClass", "testAssignmentStatement");

        AssignmentStatement assignment = (AssignmentStatement) interaction.getFragments().get(2);
        StructuralFeature previousAssignTo = assignment.getAssignTo();

        Message initialMessage = MessageViewUtil.findInitialMessage(assignment);
        List<TemporaryProperty> localProperties = new ArrayList<>(initialMessage.getLocalProperties());

        PrimitiveType type = (PrimitiveType) aspect.getStructuralView().getTypes().get(2);
        String name = "assignmentname";
        controller.createTemporaryAssignment(assignment, name, type);

        StructuralFeature newAssignTo = assignment.getAssignTo();

        assertThat(previousAssignTo.eContainer()).isNotNull();
        assertThat(newAssignTo).isNotEqualTo(previousAssignTo);

        localProperties.add((Attribute) newAssignTo);
        assertThat(initialMessage.getLocalProperties()).containsOnlyElementsOf(localProperties);
    }

    /**
     * Test that previous assignTo (temporary property) is not deleted when used by another assignment statement.
     */
    @Test
    public void testChangeAssignmentAssignTo_ExistingAssignToReplaced_UsedByAssignmentAndNotDeleted() {
        loadMessageView("OtherClass", "testAssignmentStatement");

        StructuralFeature feature = classifier.getAssociationEnds().get(0);
        AssignmentStatement assignment = (AssignmentStatement) interaction.getFragments().get(1);
        StructuralFeature previousAssignTo = assignment.getAssignTo();

        Message initialMessage = MessageViewUtil.findInitialMessage(assignment);
        List<TemporaryProperty> localProperties = new ArrayList<>(initialMessage.getLocalProperties());

        controller.changeAssignmentAssignTo(assignment, feature);

        assertThat(previousAssignTo.eContainer()).isNotNull();
        assertThat(assignment.getAssignTo()).isEqualTo(feature);
        assertThat(initialMessage.getLocalProperties()).containsOnlyElementsOf(localProperties);
    }

    /**
     * Test that previous assignTo (temporary property) is not deleted when used by a message.
     */
    @Test
    public void testChangeAssignmentAssignTo_ExistingAssignToReplaced_UsedByMessageAndNotDeleted() {
        loadMessageView("OtherClass", "testAssignmentStatement");

        StructuralFeature feature = classifier.getAssociationEnds().get(0);
        AssignmentStatement assignment = (AssignmentStatement) interaction.getFragments().get(10);
        StructuralFeature previousAssignTo = assignment.getAssignTo();

        Message initialMessage = MessageViewUtil.findInitialMessage(assignment);
        List<TemporaryProperty> localProperties = new ArrayList<>(initialMessage.getLocalProperties());

        controller.changeAssignmentAssignTo(assignment, feature);

        assertThat(previousAssignTo.eContainer()).isNotNull();
        assertThat(assignment.getAssignTo()).isEqualTo(feature);
        assertThat(initialMessage.getLocalProperties()).containsOnlyElementsOf(localProperties);
    }

    /**
     * Test that previous assignTo (temporary property) is deleted when not used by another object.
     */
    @Test
    public void testChangeAssignmentAssignTo_ExistingAssignToReplaced_UnUsedAndDeleted() {
        loadMessageView("OtherClass", "testAssignmentStatement");

        StructuralFeature feature = classifier.getAssociationEnds().get(0);
        AssignmentStatement assignment = (AssignmentStatement) interaction.getFragments().get(11);
        TemporaryProperty previousAssignTo = (TemporaryProperty) assignment.getAssignTo();

        Message initialMessage = MessageViewUtil.findInitialMessage(assignment);
        List<TemporaryProperty> localProperties = new ArrayList<>(initialMessage.getLocalProperties());
        localProperties.remove(previousAssignTo);

        controller.changeAssignmentAssignTo(assignment, feature);

        assertThat(previousAssignTo.eContainer()).isNull();
        assertThat(assignment.getAssignTo()).isEqualTo(feature);
        assertThat(initialMessage.getLocalProperties()).containsOnlyElementsOf(localProperties);
    }

    /**
     * Test that non-temporary previous assignTo is not deleted.
     */
    @Test
    public void testChangeAssignmentAssignTo_ExistingAssignToReplaced_StructuralFeatureNotDeleted() {
        loadMessageView("OtherClass", "testAssignmentStatement");

        StructuralFeature feature = classifier.getAssociationEnds().get(1);
        AssignmentStatement assignment = (AssignmentStatement) interaction.getFragments().get(12);
        StructuralFeature previousAssignTo = assignment.getAssignTo();

        Message initialMessage = MessageViewUtil.findInitialMessage(assignment);
        List<TemporaryProperty> localProperties = new ArrayList<>(initialMessage.getLocalProperties());

        controller.changeAssignmentAssignTo(assignment, feature);

        assertThat(previousAssignTo.eContainer()).isNotNull();
        assertThat(assignment.getAssignTo()).isEqualTo(feature);
        assertThat(initialMessage.getLocalProperties()).containsOnlyElementsOf(localProperties);
    }

    @Test
    @Model(fileName = "tests/models/MessageViews/Messages.ram")
    public void testMoveFragment_FragmentBefore_MovedUp() {
        loadMessageView("Caller", "testMessageMoving");
        int index = 5;

        InteractionFragment fragment = interaction.getFragments().get(index);

        List<InteractionFragment> interactionFragments = new ArrayList<>(interaction.getFragments());
        move(interactionFragments, interactionFragments.subList(index, index + 1), index - 1);

        controller.moveFragment(fragment, true);

        TestUtil.assertContainsOnlyElementsExactlyOf(interaction.getFragments(), interactionFragments);
    }

    @Test
    @Model(fileName = "tests/models/MessageViews/Messages.ram")
    public void testMoveFragment_MessageAfter_MovedDown() {
        loadMessageView("Caller", "testMessageMoving");
        int index = 1;

        InteractionFragment fragment = interaction.getFragments().get(index);

        List<InteractionFragment> interactionFragments = new ArrayList<>(interaction.getFragments());
        move(interactionFragments, interactionFragments.subList(index, index + 1), index + 2);

        controller.moveFragment(fragment, false);

        TestUtil.assertContainsOnlyElementsExactlyOf(interaction.getFragments(), interactionFragments);
    }

    @Test
    @Model(fileName = "tests/models/MessageViews/Messages.ram")
    public void testMoveFragment_MessageBefore_CombinedFragmentMovedUp() {
        loadMessageView("Caller", "testMessageMoving");
        int index = 4;

        InteractionFragment fragment = interaction.getFragments().get(index);

        List<InteractionFragment> interactionFragments = new ArrayList<>(interaction.getFragments());
        move(interactionFragments, interactionFragments.subList(index, index + 1), index - 2);

        controller.moveFragment(fragment, true);

        TestUtil.assertContainsOnlyElementsExactlyOf(interaction.getFragments(), interactionFragments);
    }

    @Test
    @Model(fileName = "tests/models/MessageViews/Messages.ram")
    public void testMoveFragment_NestedBehaviourAfter_FragmentMovedDown() {
        loadMessageView("Caller", "testMessageMoving");
        int index = 5;

        InteractionFragment fragment = interaction.getFragments().get(index);

        List<InteractionFragment> interactionFragments = new ArrayList<>(interaction.getFragments());
        move(interactionFragments, interactionFragments.subList(index, index + 1), index + 7);

        controller.moveFragment(fragment, false);

        TestUtil.assertContainsOnlyElementsExactlyOf(interaction.getFragments(), interactionFragments);
    }

    @Test
    @Model(fileName = "tests/models/MessageViews/Messages.ram")
    public void testMoveFragment_CombinedFragment_Moved() {
        loadMessageView("Caller", "testMessageMoving");
        int index = 4;

        InteractionFragment fragment = interaction.getFragments().get(index);

        List<Lifeline> covered = new ArrayList<>(fragment.getCovered());
        List<InteractionFragment> interactionFragments = new ArrayList<>(interaction.getFragments());
        move(interactionFragments, interactionFragments.subList(index, index + 1), index + 1);

        controller.moveFragment(fragment, false);

        TestUtil.assertContainsOnlyElementsExactlyOf(interaction.getFragments(), interactionFragments);
        assertThat(fragment.getCovered()).isEqualTo(covered);
    }

    private void move(List<InteractionFragment> fragments, List<InteractionFragment> subList, int toIndex) {
        List<InteractionFragment> fragmentsToMove = new ArrayList<>(subList);
        fragments.removeAll(subList);
        fragments.addAll(toIndex, fragmentsToMove);
    }

    @Test
    public void testRemoveCombinedFragmentAndKeepContents_OneOperandWithFragments() {
        loadMessageView("OtherClass", "testAssignmentStatement");

        int index = 7;
        FragmentContainer container = interaction;
        CombinedFragment combinedFragment = (CombinedFragment) container.getFragments().get(index);

        List<Lifeline> coveredLifelines = new ArrayList<>(combinedFragment.getCovered());

        List<InteractionFragment> expectedFragments = new ArrayList<>(container.getFragments());
        expectedFragments.remove(combinedFragment);
        expectedFragments.addAll(index, combinedFragment.getOperands().get(0).getFragments());

        controller.removeCombinedFragmentAndKeepContents(combinedFragment);

        TestUtil.assertContainsOnlyElementsExactlyOf(container.getFragments(), expectedFragments);
        assertThat(coveredLifelines).extracting("coveredBy").doesNotContain(combinedFragment);
    }

    @Test
    public void testRemoveCombinedFragmentAndKeepContents_OneOperandWithFragments_OuterCombinedFragmentDeleted() {
        loadMessageView("OtherClass", "testCombinedFragments");

        int index = 3;
        FragmentContainer container = interaction;
        CombinedFragment combinedFragment = (CombinedFragment) container.getFragments().get(index);

        List<Lifeline> coveredLifelines = new ArrayList<>(combinedFragment.getCovered());

        List<InteractionFragment> expectedFragments = new ArrayList<>(container.getFragments());
        expectedFragments.remove(combinedFragment);
        expectedFragments.addAll(index, combinedFragment.getOperands().get(0).getFragments());

        controller.removeCombinedFragmentAndKeepContents(combinedFragment);

        TestUtil.assertContainsOnlyElementsExactlyOf(container.getFragments(), expectedFragments);
        assertThat(coveredLifelines).extracting("coveredBy").doesNotContain(combinedFragment);
    }

    @Test
    public void testRemoveCombinedFragmentAndKeepContents_MultipleOperandsWithFragments_NestedCombinedFragmentDeleted() {
        loadMessageView("OtherClass", "testCombinedFragments");

        int index = 2;
        CombinedFragment outerCombinedFragment = (CombinedFragment) interaction.getFragments().get(3);
        FragmentContainer container = outerCombinedFragment.getOperands().get(0);
        CombinedFragment combinedFragment = (CombinedFragment) container.getFragments().get(index);

        List<Lifeline> coveredLifelines = new ArrayList<>(combinedFragment.getCovered());

        List<InteractionFragment> expectedFragments = new ArrayList<>(container.getFragments());
        expectedFragments.remove(combinedFragment);
        expectedFragments.addAll(index, combinedFragment.getOperands().get(1).getFragments());
        expectedFragments.addAll(index, combinedFragment.getOperands().get(0).getFragments());

        controller.removeCombinedFragmentAndKeepContents(combinedFragment);

        TestUtil.assertContainsOnlyElementsExactlyOf(container.getFragments(), expectedFragments);
        assertThat(coveredLifelines).extracting("coveredBy").doesNotContain(combinedFragment);
    }

    /**
     * @issue #444
     */
    @Test
    public void testRemoveInteractionFragment_StaticReferenceLifelineWithSeveralCalls_LifelineAndReferenceNotRemoved() {
        loadMessageView("OtherClass", "testRemoveStaticReferences");
        
        InteractionFragment sendEvent = interaction.getFragments().get(1);
        InteractionFragment receiveEvent = interaction.getFragments().get(2);
        
        Lifeline lifeline = receiveEvent.getCovered().get(0);
        Reference represents = (Reference) lifeline.getRepresents();
        
        assertThat(represents.eContainer()).isEqualTo(interaction);
        
        controller.removeInteractionFragment(sendEvent);
        
        assertThat(interaction.getProperties()).contains(represents);
        assertThat(interaction.getLifelines()).contains(lifeline);
        
        sendEvent = interaction.getFragments().get(1);
        
        controller.removeInteractionFragment(sendEvent);
        
        assertThat(interaction.getProperties()).doesNotContain(represents);
        assertThat(interaction.getLifelines()).doesNotContain(lifeline);
    }
    
    /**
     * @issue #444
     */
    @Test
    public void testRemoveInteractionFragment_StaticReferenceLifeline_LifelineAndReferenceRemoved() {
        loadMessageView("OtherClass", "testRemoveStaticReferences");
        
        InteractionFragment sendEvent = interaction.getFragments().get(9);
        InteractionFragment receiveEvent = interaction.getFragments().get(10);
        
        Lifeline lifeline = receiveEvent.getCovered().get(0);
        Reference represents = (Reference) lifeline.getRepresents();
        
        assertThat(represents.eContainer()).isEqualTo(interaction);
        
        controller.removeInteractionFragment(sendEvent);
        
        assertThat(interaction.getProperties()).doesNotContain(represents);
        assertThat(interaction.getLifelines()).doesNotContain(lifeline);
    }

}
