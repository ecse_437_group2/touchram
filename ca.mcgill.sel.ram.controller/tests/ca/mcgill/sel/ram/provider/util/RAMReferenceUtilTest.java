package ca.mcgill.sel.ram.provider.util;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Collection;

import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.junit.Rule;
import org.junit.Test;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.COREModelExtension;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.AssociationEnd;
import ca.mcgill.sel.ram.Attribute;
import ca.mcgill.sel.ram.AttributeMapping;
import ca.mcgill.sel.ram.Class;
import ca.mcgill.sel.ram.Classifier;
import ca.mcgill.sel.ram.ClassifierMapping;
import ca.mcgill.sel.ram.ImplementationClass;
import ca.mcgill.sel.ram.LayoutElement;
import ca.mcgill.sel.ram.ObjectType;
import ca.mcgill.sel.ram.Operation;
import ca.mcgill.sel.ram.OperationMapping;
import ca.mcgill.sel.ram.Parameter;
import ca.mcgill.sel.ram.ParameterMapping;
import ca.mcgill.sel.ram.REnum;
import ca.mcgill.sel.ram.RamFactory;
import ca.mcgill.sel.ram.RamPackage;
import ca.mcgill.sel.ram.StructuralView;
import ca.mcgill.sel.ram.Type;
import ca.mcgill.sel.ram.TypeParameter;
import ca.mcgill.sel.ram.controller.util.MessageViewTestUtil;
import ca.mcgill.sel.ram.controller.util.ModelResource;

// CHECKSTYLE:OFF Temporary location in controller project (therefore this package is not added to the exclusions).
public class RAMReferenceUtilTest {
    
    @Rule
    public final ModelResource modelResource = new ModelResource("tests/models/ExternalReferences/Extender.ram") {
        
        @Override
        public void modelLoaded(EObject model) {
            aspect = (Aspect) model;
            editingDomain = EMFEditUtil.getEditingDomain(aspect);
            command = new CompoundCommand();
            
            modelExtension = aspect.getModelExtensions().get(0);
            externalAspect = (Aspect) modelExtension.getSource();
        }
        
    };
    
    private Aspect aspect;
    private COREModelExtension modelExtension;
    private Aspect externalAspect;
    private EditingDomain editingDomain;
    private CompoundCommand command;
    
    @Test
    public void testSetLocalElementPropertyValue_ExternalClass() {
        Class externalClassifier = (Class) MessageViewTestUtil.getClassifierByName(externalAspect, "Account");
        Class owner = (Class) MessageViewTestUtil.getClassifierByName(aspect, "LocalClass");
        Operation localOperation = MessageViewTestUtil.getOperationByName(owner, "localOperation");
        
        RAMReferenceUtil.setLocalizedPropertyValue(editingDomain, localOperation, RamPackage.Literals.OPERATION__RETURN_TYPE, externalClassifier);
        
        StructuralView structuralView = aspect.getStructuralView();
        
        Class localClassifier = (Class) MessageViewTestUtil.getClassifierByName(aspect, "Account");
        
        assertThat(localOperation.getReturnType()).isEqualTo(localClassifier);
        
        assertClassifierWasCloned(structuralView, externalClassifier, localClassifier);
        assertThat(localClassifier.getOperations()).isEmpty();
        assertThat(localClassifier.getAttributes()).isEmpty();
        
        assertLayoutElementExists(structuralView, localClassifier);
        assertClassMappingReferenceExists(externalClassifier, localClassifier);
    }
    
    @Test
    public void testGetOrCreateLocalElement_LocalElements() {
        Classifier localClassifier = MessageViewTestUtil.getClassifierByName(aspect, "LocalClass");
        
        EObject result = RAMReferenceUtil.localizeElement(editingDomain, command, aspect, localClassifier);
        
        assertThat(result).isEqualTo(localClassifier);
        assertThat(command.isEmpty()).isTrue();
        
        Operation localOperation = MessageViewTestUtil.getOperationByName(localClassifier, "localOperation");
        
        result = RAMReferenceUtil.localizeElement(editingDomain, command, aspect, localOperation);
        
        assertThat(result).isEqualTo(localOperation);
        assertThat(command.isEmpty()).isTrue();
    }
    
    @Test
    public void testGetOrCreateLocalElement_ExternalClass() {
        Class externalClassifier = (Class) MessageViewTestUtil.getClassifierByName(externalAspect, "Account");
        
        Class localClassifier = RAMReferenceUtil.localizeElement(editingDomain, command, aspect, externalClassifier);
        editingDomain.getCommandStack().execute(command);
        
        StructuralView structuralView = aspect.getStructuralView();
        
        assertClassifierWasCloned(structuralView, externalClassifier, localClassifier);
        assertThat(localClassifier.getOperations()).isEmpty();
        assertThat(localClassifier.getAttributes()).isEmpty();
        
        assertLayoutElementExists(structuralView, localClassifier);
        assertClassMappingReferenceExists(externalClassifier, localClassifier);
    }
    
    @Test
    public void testGetOrCreateLocalElement_ExternalOperation_NoLocalClass() {
        Class externalClassifier = (Class) MessageViewTestUtil.getClassifierByName(externalAspect, "Account");
        Operation externalOperation = MessageViewTestUtil.getOperationByName(externalClassifier, "withdraw");
        
        Operation localOperation = RAMReferenceUtil.localizeElement(editingDomain, command, aspect, externalOperation);
        editingDomain.getCommandStack().execute(command);
        
        StructuralView structuralView = aspect.getStructuralView();
        
        Class localClassifier = (Class) MessageViewTestUtil.getClassifierByName(aspect, "Account");
        assertClassifierWasCloned(structuralView, externalClassifier, localClassifier);
        assertThat(localClassifier.getAttributes()).isEmpty();
        assertThat(localClassifier.getOperations()).containsOnly(localOperation);
        
        assertLayoutElementExists(structuralView, localClassifier);
        assertClassMappingReferenceExists(externalClassifier, localClassifier);
        
        assertThat(localOperation.eContainer()).isEqualTo(localClassifier);
        assertThat(localOperation).isNotEqualTo(externalOperation);
        assertThat(localOperation).isEqualToComparingOnlyGivenFields(externalOperation, "name", "visibility", "partiality", "extendedVisibility", "operationType");
                
        assertOperationMappingReferenceExists(externalOperation, localOperation);
    }
    
    @Test 
    public void testLocalizeElement_ExternalOperation_ParameterMappingsCreated() {
        Class externalClassifier = (Class) MessageViewTestUtil.getClassifierByName(externalAspect, "Account");
        Operation externalOperation = MessageViewTestUtil.getOperationByName(externalClassifier, "withdraw");
        
        Operation localOperation = RAMReferenceUtil.localizeElement(editingDomain, command, aspect, externalOperation);
        editingDomain.getCommandStack().execute(command);
        
        Class localClassifier = (Class) MessageViewTestUtil.getClassifierByName(aspect, "Account");
        
        assertThat(localOperation.eContainer()).isEqualTo(localClassifier);
        assertThat(localOperation).isNotEqualTo(externalOperation);
        assertThat(localOperation).isEqualToComparingOnlyGivenFields(externalOperation, "name", "visibility", "partiality", "extendedVisibility", "operationType");

        assertOperationMappingReferenceExists(externalOperation, localOperation);
        assertParameterMappingReferenceExists(externalOperation, localOperation);
    }
    
    @Test
    public void testGetOrCreateLocalElement_ExternalOperation_PrimitiveTypesUpdated() {
        Class externalClass = (Class) MessageViewTestUtil.getClassifierByName(externalAspect, "Account");
        Operation external = MessageViewTestUtil.getOperationByName(externalClass, "withdraw");
        
        Operation local = RAMReferenceUtil.localizeElement(editingDomain, command, aspect, external);
        editingDomain.getCommandStack().execute(command);
        
        assertLocalType(local.getReturnType(), external.getReturnType());
        
        assertThat(local.getParameters()).hasSameSizeAs(external.getParameters());
        
        for (int i = 0; i < local.getParameters().size(); i++) {
            Parameter localParameter = local.getParameters().get(i);
            Parameter externalParameter = external.getParameters().get(i);
            
            assertThat(localParameter).isEqualToComparingOnlyGivenFields(externalParameter, "name", "visibility", "partiality");
            assertLocalType(localParameter.getType(), externalParameter.getType());
        }
    }

    @Test
    public void testGetOrCreateLocalElement_ExternalAttribute_LocalClassExists() {
        Class externalClassifier = (Class) MessageViewTestUtil.getClassifierByName(externalAspect, "User");
        Attribute externalAttribute = externalClassifier.getAttributes().get(0);
        
        Class localClassifier = (Class) MessageViewTestUtil.getClassifierByName(aspect, "User");

        Attribute localAttribute = RAMReferenceUtil.localizeElement(editingDomain, command, aspect, externalAttribute);
        editingDomain.getCommandStack().execute(command);
        
        assertThat(localClassifier.getAttributes()).containsOnly(localAttribute);
        
        assertThat(localAttribute.eContainer()).isEqualTo(localClassifier);
        assertThat(localAttribute).isNotEqualTo(externalAttribute);
        assertThat(localAttribute).isEqualToComparingOnlyGivenFields(externalAttribute, "name", "visibility", "partiality");
        
        assertAttributeMappingReferenceExists(externalAttribute, localAttribute);
    }
    
    @Test
    public void testGetOrCreateLocalElement_ExternalAttribute_TypeUpdated() {
        Class externalClassifier = (Class) MessageViewTestUtil.getClassifierByName(externalAspect, "User");
        Attribute externalAttribute = externalClassifier.getAttributes().get(0);

        Attribute localAttribute = RAMReferenceUtil.localizeElement(editingDomain, command, aspect, externalAttribute);
        editingDomain.getCommandStack().execute(command);
        
        assertLocalType(localAttribute.getType(), externalAttribute.getType());
    }
    
    @Test
    public void testGetOrCreateLocalElement_ExternalOperation_TypeCloned() {
        Class externalClass = (Class) MessageViewTestUtil.getClassifierByName(externalAspect, "Account");
        Operation external = MessageViewTestUtil.getOperationByName(externalClass, "getBank");
        
        Operation local = RAMReferenceUtil.localizeElement(editingDomain, command, aspect, external);
        editingDomain.getCommandStack().execute(command);
        
        Class externalType = (Class) external.getReturnType();
        Class localType = (Class) local.getReturnType();
        
        assertLocalType(localType, externalType);
        
        assertClassifierWasCloned(aspect.getStructuralView(), externalType, localType);
        assertLayoutElementExists(aspect.getStructuralView(), localType);
        assertClassMappingReferenceExists(externalType, localType);
    }
    
    @Test
    public void testGetOrCreateLocalElement_ExternalOperation_TypeSameAsContainerCloned() {
        Class externalClass = (Class) MessageViewTestUtil.getClassifierByName(externalAspect, "Account");
        Operation external = MessageViewTestUtil.getOperationByName(externalClass, "getMe");
        
        Operation local = RAMReferenceUtil.localizeElement(editingDomain, command, aspect, external);
        editingDomain.getCommandStack().execute(command);
        
        Class externalType = (Class) external.getReturnType();
        Class localType = (Class) local.getReturnType();
        
        assertLocalType(localType, externalType);
        assertThat(local.eContainer()).isEqualTo(localType);
        
        Parameter parameter = local.getParameters().get(0);
        
        Class localParameterType = (Class) parameter.getType();
        Class externalParameterType = (Class) external.getParameters().get(0).getType();
        assertLocalType(localParameterType, externalParameterType);
        assertThat(local.eContainer()).isEqualTo(localParameterType);
    }
    
    @Test
    public void testGetOrCreateLocalElement_UnsupportedElement() {
        Class externalClass = (Class) MessageViewTestUtil.getClassifierByName(externalAspect, "Account");
        AssociationEnd end = RamFactory.eINSTANCE.createAssociationEnd();
        externalClass.getAssociationEnds().add(end);
        
        AssociationEnd local = RAMReferenceUtil.localizeElement(editingDomain, command, aspect, end);
        
        assertThat(local).isEqualTo(end);
        assertThat(command.canExecute()).isFalse();
    }
    
    @Test
    public void testGetOrCreateLocalElement_ExternalEnum() {
        REnum typeEnum = null;
        
        for (Type type : externalAspect.getStructuralView().getTypes()) {
            if (type.eClass() == RamPackage.Literals.RENUM) {
                if ("AccountType".equals(type.getName())) {
                    typeEnum = (REnum) type;
                }
            }
        }
        
        assertThat(typeEnum).isNotNull();
        
        REnum localEnum = RAMReferenceUtil.localizeElement(editingDomain, command, aspect, typeEnum);
        
        assertThat(localEnum).isNotEqualTo(typeEnum);
        assertThat(command.canExecute()).isTrue();
    }
    
    @Test
    public void testGetOrCreateLocalElement_ExternalImplementationClass() {
        ImplementationClass externalClassifier = (ImplementationClass) MessageViewTestUtil.getClassifierByName(externalAspect, "ArrayList");
        TypeParameter typeParameter = externalClassifier.getTypeParameters().get(0);
        ObjectType genericType = typeParameter.getGenericType();
        
        ImplementationClass localClassifier = RAMReferenceUtil.localizeElement(editingDomain, command, aspect, externalClassifier);
        editingDomain.getCommandStack().execute(command);
        
        StructuralView structuralView = aspect.getStructuralView();
        
        assertClassifierWasCloned(structuralView, externalClassifier, localClassifier);
        assertThat(localClassifier.getOperations()).isEmpty();
        TypeParameter localTypeParameter = localClassifier.getTypeParameters().get(0);
        assertClassifierWasCloned(structuralView, (Classifier) genericType, (Classifier) localTypeParameter.getGenericType()); 
        
        assertLayoutElementExists(structuralView, localClassifier);
        assertClassMappingReferenceExists(externalClassifier, localClassifier);
    }

    private void assertClassifierWasCloned(StructuralView structuralView, Classifier externalClassifier, Classifier localClassifier) {
        assertThat(localClassifier).isNotEqualTo(externalClassifier);
        
        assertThat(localClassifier).isEqualToComparingOnlyGivenFields(externalClassifier, "name", "partiality", "visibility");
        assertThat(localClassifier.getSuperTypes()).isEmpty();
        assertThat(localClassifier.getAssociationEnds()).isEmpty();
        
        assertThat(structuralView.getClasses()).contains(localClassifier);
    }

    private void assertLayoutElementExists(StructuralView structuralView, Classifier localClassifier) {
        EMap<EObject, LayoutElement> eMap = aspect.getLayout().getContainers().get(structuralView);
        
        assertThat(eMap.containsKey(localClassifier)).isTrue();
        assertThat(eMap.get(localClassifier)).isNotNull();
        assertThat(eMap.get(localClassifier)).isInstanceOf(LayoutElement.class);
    }

    private void assertClassMappingReferenceExists(Classifier externalClassifier, Classifier localClassifier) {
        ClassifierMapping mapping = getClassifierMappingFor(externalClassifier);
        assertThat(mapping).hasFieldOrPropertyWithValue("from", externalClassifier);
        assertThat(mapping).hasFieldOrPropertyWithValue("to", localClassifier);
    }
    
    private void assertOperationMappingReferenceExists(Operation externalOperation, Operation localOperation) {
        ClassifierMapping mapping = getClassifierMappingFor(externalOperation.eContainer());
        OperationMapping operationMapping = mapping.getOperationMappings().get(0);
        
        assertThat(operationMapping).hasFieldOrPropertyWithValue("from", externalOperation);
        assertThat(operationMapping).hasFieldOrPropertyWithValue("to", localOperation);
    }
    
    private void assertParameterMappingReferenceExists(Operation externalOperation, Operation localOperation) {
        ClassifierMapping mapping = getClassifierMappingFor(externalOperation.eContainer());
        OperationMapping operationMapping = mapping.getOperationMappings().get(0);
        
        for (int i = 0; i < externalOperation.getParameters().size(); i++) {
            ParameterMapping parameterMapping = operationMapping.getParameterMappings().get(i);
            
            assertThat(parameterMapping.getFrom()).isEqualTo(externalOperation.getParameters().get(i));
            assertThat(parameterMapping.getTo()).isEqualTo(localOperation.getParameters().get(i));
        }
    }
    
    private void assertAttributeMappingReferenceExists(Attribute externalAttribute, Attribute localAttribute) {
        ClassifierMapping mapping = getClassifierMappingFor(externalAttribute.eContainer());
        AttributeMapping attributeMapping = mapping.getAttributeMappings().get(0);
        
        assertThat(attributeMapping).hasFieldOrPropertyWithValue("from", externalAttribute);
        assertThat(attributeMapping).hasFieldOrPropertyWithValue("to", localAttribute);
    }
    
    private ClassifierMapping getClassifierMappingFor(EObject source) {
        Collection<ClassifierMapping> mappings =
                EMFModelUtil.findCrossReferencesOfType(aspect, source,
                        CorePackage.Literals.CORE_LINK__FROM,
                        RamPackage.Literals.CLASSIFIER_MAPPING);
        
        assertThat(mappings).hasSize(1);
        
        return mappings.iterator().next();
    }

    private void assertLocalType(Type localType, Type externalType) {
        EObject typeRoot = EcoreUtil.getRootContainer(localType);
        assertThat(typeRoot).isEqualTo(aspect);
        assertThat(localType).isEqualToComparingOnlyGivenFields(externalType, "name", "eProperties.eClass");
    }

}
