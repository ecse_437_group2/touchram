/**
 */
package ca.mcgill.sel.ucm;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Component Ref</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link ca.mcgill.sel.ucm.ComponentRef#getCompDef <em>Comp Def</em>}</li>
 *   <li>{@link ca.mcgill.sel.ucm.ComponentRef#getChildren <em>Children</em>}</li>
 *   <li>{@link ca.mcgill.sel.ucm.ComponentRef#getParent <em>Parent</em>}</li>
 *   <li>{@link ca.mcgill.sel.ucm.ComponentRef#getNodes <em>Nodes</em>}</li>
 * </ul>
 * </p>
 *
 * @see ca.mcgill.sel.ucm.UCMPackage#getComponentRef()
 * @model
 * @generated
 */
public interface ComponentRef extends UCMModelElement {
    /**
     * Returns the value of the '<em><b>Comp Def</b></em>' reference.
     * It is bidirectional and its opposite is '{@link ca.mcgill.sel.ucm.Component#getCompRefs <em>Comp Refs</em>}'.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Comp Def</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Comp Def</em>' reference.
     * @see #setCompDef(Component)
     * @see ca.mcgill.sel.ucm.UCMPackage#getComponentRef_CompDef()
     * @see ca.mcgill.sel.ucm.Component#getCompRefs
     * @model opposite="compRefs" required="true"
     * @generated
     */
    Component getCompDef();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.ucm.ComponentRef#getCompDef <em>Comp Def</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Comp Def</em>' reference.
     * @see #getCompDef()
     * @generated
     */
    void setCompDef(Component value);

    /**
     * Returns the value of the '<em><b>Children</b></em>' reference list.
     * The list contents are of type {@link ca.mcgill.sel.ucm.ComponentRef}.
     * It is bidirectional and its opposite is '{@link ca.mcgill.sel.ucm.ComponentRef#getParent <em>Parent</em>}'.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Children</em>' reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Children</em>' reference list.
     * @see ca.mcgill.sel.ucm.UCMPackage#getComponentRef_Children()
     * @see ca.mcgill.sel.ucm.ComponentRef#getParent
     * @model opposite="parent"
     * @generated
     */
    EList<ComponentRef> getChildren();

    /**
     * Returns the value of the '<em><b>Parent</b></em>' reference.
     * It is bidirectional and its opposite is '{@link ca.mcgill.sel.ucm.ComponentRef#getChildren <em>Children</em>}'.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Parent</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Parent</em>' reference.
     * @see #setParent(ComponentRef)
     * @see ca.mcgill.sel.ucm.UCMPackage#getComponentRef_Parent()
     * @see ca.mcgill.sel.ucm.ComponentRef#getChildren
     * @model opposite="children"
     * @generated
     */
    ComponentRef getParent();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.ucm.ComponentRef#getParent <em>Parent</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Parent</em>' reference.
     * @see #getParent()
     * @generated
     */
    void setParent(ComponentRef value);

    /**
     * Returns the value of the '<em><b>Nodes</b></em>' reference list.
     * The list contents are of type {@link ca.mcgill.sel.ucm.PathNode}.
     * It is bidirectional and its opposite is '{@link ca.mcgill.sel.ucm.PathNode#getCompRef <em>Comp Ref</em>}'.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Nodes</em>' reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Nodes</em>' reference list.
     * @see ca.mcgill.sel.ucm.UCMPackage#getComponentRef_Nodes()
     * @see ca.mcgill.sel.ucm.PathNode#getCompRef
     * @model opposite="compRef"
     * @generated
     */
    EList<PathNode> getNodes();

} // ComponentRef
