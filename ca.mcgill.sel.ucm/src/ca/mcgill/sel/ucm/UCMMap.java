/**
 */
package ca.mcgill.sel.ucm;

import ca.mcgill.sel.core.COREModel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Map</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link ca.mcgill.sel.ucm.UCMMap#getConnections <em>Connections</em>}</li>
 *   <li>{@link ca.mcgill.sel.ucm.UCMMap#getNodes <em>Nodes</em>}</li>
 *   <li>{@link ca.mcgill.sel.ucm.UCMMap#getResps <em>Resps</em>}</li>
 *   <li>{@link ca.mcgill.sel.ucm.UCMMap#getComps <em>Comps</em>}</li>
 *   <li>{@link ca.mcgill.sel.ucm.UCMMap#getCompRefs <em>Comp Refs</em>}</li>
 * </ul>
 * </p>
 *
 * @see ca.mcgill.sel.ucm.UCMPackage#getUCMMap()
 * @model
 * @generated
 */
public interface UCMMap extends COREModel {
    /**
     * Returns the value of the '<em><b>Connections</b></em>' containment reference list.
     * The list contents are of type {@link ca.mcgill.sel.ucm.NodeConnection}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Connections</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Connections</em>' containment reference list.
     * @see ca.mcgill.sel.ucm.UCMPackage#getUCMMap_Connections()
     * @model containment="true"
     * @generated
     */
    EList<NodeConnection> getConnections();

    /**
     * Returns the value of the '<em><b>Nodes</b></em>' containment reference list.
     * The list contents are of type {@link ca.mcgill.sel.ucm.PathNode}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Nodes</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Nodes</em>' containment reference list.
     * @see ca.mcgill.sel.ucm.UCMPackage#getUCMMap_Nodes()
     * @model containment="true"
     * @generated
     */
    EList<PathNode> getNodes();

    /**
     * Returns the value of the '<em><b>Resps</b></em>' containment reference list.
     * The list contents are of type {@link ca.mcgill.sel.ucm.Responsibility}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Resps</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Resps</em>' containment reference list.
     * @see ca.mcgill.sel.ucm.UCMPackage#getUCMMap_Resps()
     * @model containment="true"
     * @generated
     */
    EList<Responsibility> getResps();

    /**
     * Returns the value of the '<em><b>Comps</b></em>' containment reference list.
     * The list contents are of type {@link ca.mcgill.sel.ucm.Component}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Comps</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Comps</em>' containment reference list.
     * @see ca.mcgill.sel.ucm.UCMPackage#getUCMMap_Comps()
     * @model containment="true"
     * @generated
     */
    EList<Component> getComps();

    /**
     * Returns the value of the '<em><b>Comp Refs</b></em>' containment reference list.
     * The list contents are of type {@link ca.mcgill.sel.ucm.ComponentRef}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Comp Refs</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Comp Refs</em>' containment reference list.
     * @see ca.mcgill.sel.ucm.UCMPackage#getUCMMap_CompRefs()
     * @model containment="true"
     * @generated
     */
    EList<ComponentRef> getCompRefs();

} // UCMMap
