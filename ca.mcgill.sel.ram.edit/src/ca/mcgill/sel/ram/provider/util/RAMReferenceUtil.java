package ca.mcgill.sel.ram.provider.util;

import java.util.Collection;
import java.util.Set;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;

import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.COREMapping;
import ca.mcgill.sel.core.COREModel;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.COREModelElement;
import ca.mcgill.sel.core.COREModelExtension;
import ca.mcgill.sel.core.COREModelReuse;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.util.COREModelUtil;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.Attribute;
import ca.mcgill.sel.ram.Class;
import ca.mcgill.sel.ram.Classifier;
import ca.mcgill.sel.ram.ClassifierMapping;
import ca.mcgill.sel.ram.LayoutElement;
import ca.mcgill.sel.ram.Operation;
import ca.mcgill.sel.ram.Parameter;
import ca.mcgill.sel.ram.PrimitiveType;
import ca.mcgill.sel.ram.REnum;
import ca.mcgill.sel.ram.RamFactory;
import ca.mcgill.sel.ram.RamPackage;
import ca.mcgill.sel.ram.util.RAMModelUtil;

/**
 * Helper class for dealing with references across models for RAM.
 * Provides convenient methods that can be delegated to in order to ensure clones are created and mappings established. 
 * 
 * @author mschoettle
 */
public final class RAMReferenceUtil {
    
    /**
     * Creates a new instance.
     */
    private RAMReferenceUtil() {
        
    }
    
    /**
     * Sets the given value as the concrete value of the feature.
     * The value is first localized (see {@link #localizeElement(EditingDomain, CompoundCommand, EObject, EObject)}
     * to ensure that only a local element is referenced.
     * The changes are executed using commands on the command stack of the editing domain.
     * 
     * @param domain the {@link EditingDomain}
     * @param owner the object which owns the property
     * @param feature the {@link EStructuralFeature} for which a value is to be set
     * @param value the value to set
     * @see #localizeElement(EditingDomain, CompoundCommand, EObject, EObject)
     */
    public static void setLocalizedPropertyValue(EditingDomain domain, Object owner, 
                        EStructuralFeature feature, Object value) {
        EObject eObject = (EObject) owner;
        COREModelElement eValue = (COREModelElement) value;
        
        CompoundCommand command = new CompoundCommand();
        COREModelElement localValue = localizeElement(domain, command, eObject, eValue);
        
        command.append(SetCommand.create(domain, owner, feature, localValue));
        
        domain.getCommandStack().execute(command);
    }
    
    /**
     * Ensures that the given element is localized. I.e., if it is not already contained in the same model as the given
     * local model element, it is cloned and added to the model. In addition, a mapping reference is added to connect
     * the external and the local element. All changes are added as commands to the given command.
     * Supports {@link Class}, {@link Operation} and {@link Attribute}. 
     * This means that if an element is given which is contained in a class, the class is localized as well.
     * Furthermore, all references of the element are localized as well.
     * 
     * @param domain the {@link EditingDomain}
     * @param command the {@link CompoundCommand} to add commands to
     * @param localModelElement an element of the local model
     * @param element the element to localize
     * @param <T> the type of the element to be localized
     * @return the given element, if it is already local, 
     *          or a copy of the element, which will be added to the local model
     */
    public static <T extends EObject> T localizeElement(EditingDomain domain, CompoundCommand command, 
                                            EObject localModelElement, T element) {
        Aspect currentAspect = (Aspect) EcoreUtil.getRootContainer(localModelElement);
        EObject localElement = element;
        
        if (element != null && isReferenceRequired(currentAspect, element)) {
            if (element.eContainer() instanceof COREModelElement) {
                COREModelElement externalContainer = (COREModelElement) element.eContainer();
                
                COREModelElement localContainer = 
                        getOrCreateLocalContainer(domain, command, currentAspect, externalContainer);
                
                COREModelElement externalElement = (COREModelElement) element;
                localElement = createLocalElement(domain, command, currentAspect, localContainer, externalElement);
            } else {
                localElement = getOrCreateLocalContainer(domain, command, currentAspect, (COREModelElement) element);
            }
            
            // Update properties with external references of local element to local ones.
            updateLocalElementProperties(domain, command, currentAspect, localElement);
        }
        
        @SuppressWarnings("unchecked")
        T result = (T) localElement;
        
        return result;
    }

    /**
     * Returns whether a reference is required.
     * A reference is required if the value is not within the containment hierarchy of the given owner.
     * Therefore, the owner needs to be an element of a local model.
     * The value could be from the local or any other model. 
     * 
     * @param owner the object of the local model
     * @param value the value to check
     * @return true, if value is contained in the same model as owner, false otherwise
     */
    private static boolean isReferenceRequired(EObject owner, EObject value) {
        EObject ownerRoot = EcoreUtil.getRootContainer(owner);
        EObject valueRoot = EcoreUtil.getRootContainer(value);
        
        return ownerRoot != null && valueRoot != null && ownerRoot != valueRoot;
    }

    /**
     * Returns the local container either by retrieving the existing one or creating a copy of the given element.
     * 
     * @param domain the {@link EditingDomain}
     * @param command the {@link CompoundCommand} to append commands to
     * @param currentAspect the local aspect
     * @param element the container element
     * @return the local copy of the container element
     */
    private static COREModelElement getOrCreateLocalContainer(EditingDomain domain, CompoundCommand command,
            Aspect currentAspect, COREModelElement element) {
        COREModelElement localElement;
        COREMapping<COREModelElement> containerMapping = getParentMappingFor(command, currentAspect, element);
        
        if (containerMapping == null) {
            localElement = createLocalContainer(domain, command, currentAspect.getStructuralView(), element);
        } else {
            localElement = containerMapping.getTo();
        }
        
        return localElement;
    }

    /**
     * Creates a copy of the class and adds it to the aspect.
     * Adds commands to the given command to add the class, its layout and a mapping reference to the given aspect.
     * 
     * @param domain the {@link EditingDomain}
     * @param command the {@link CompoundCommand} to append commands to
     * @param localElementContainer the container the localized element should be added to
     * @param element the element which is contained in another model
     * @return the element copy, which will be added to the local model
     */
    private static COREModelElement createLocalContainer(EditingDomain domain, CompoundCommand command,
            EObject localElementContainer, COREModelElement element) {
        COREModelElement localElement = cloneElement(element);
        
        command.append(AddCommand.create(domain, localElementContainer, element.eContainingFeature(), localElement));
        
        // Create mapping and add it to the correct container.
        Aspect aspect = (Aspect) EcoreUtil.getRootContainer(localElementContainer);
        Aspect externalAspect = (Aspect) EcoreUtil.getRootContainer(element);
        
        COREModelComposition modelComposition = getModelCompositionFor(aspect, externalAspect);
        EStructuralFeature mappingContainingFeature = CorePackage.Literals.CORE_MODEL_COMPOSITION__COMPOSITIONS;
        EClass mappingClass = null;
        
        if (RamPackage.Literals.RENUM.isInstance(element)) {
            mappingClass = RamPackage.Literals.ENUM_MAPPING;
        } else if (RamPackage.Literals.CLASSIFIER.isInstance(element)) {
            mappingClass = RamPackage.Literals.CLASSIFIER_MAPPING;
        }
        
        createAndAddMappingReference(domain, command, modelComposition, mappingContainingFeature,
                mappingClass, element, localElement);
        
        LayoutElement layoutElement = RamFactory.eINSTANCE.createLayoutElement();
        command.append(RAMEditUtil.createAddLayoutElementCommand(domain, localElementContainer, 
                localElement, layoutElement));
        
        return localElement;
    }

    /**
     * Updates properties of the local element (the copy).
     * All references to other objects of the local element are checked and "localized".
     * I.e., if a reference points to another model, it is changed to the corresponding local element.
     * 
     * @param domain the {@link EditingDomain}
     * @param command the {@link CompoundCommand} where commands are appended to
     * @param aspect the {@link Aspect} the local element belongs to
     * @param localElement the local element which is a clone of an external element
     */
    private static void updateLocalElementProperties(EditingDomain domain, CompoundCommand command,
            Aspect aspect, EObject localElement) {
        for (EReference reference : localElement.eClass().getEAllReferences()) {
            if (reference.isMany()) {
                @SuppressWarnings("unchecked")
                EList<EObject> values = (EList<EObject>) localElement.eGet(reference);
                
                for (EObject value : values) {
                    updateLocalElementProperties(domain, command, aspect, value);
                }
            } else if (!reference.isContainer()) {
                // Ensure that a containing reference is never updated as it should not be changed.
                EObject referenceValue = (EObject) localElement.eGet(reference);
                
                if (referenceValue != null) {
                    // TODO: Add support for RArray and REnum.
                    if (RamPackage.Literals.PRIMITIVE_TYPE.isInstance(referenceValue)) {
                        PrimitiveType externalType = (PrimitiveType) referenceValue;
                        referenceValue = RAMModelUtil.getPrimitiveTypeByName(aspect.getStructuralView(),
                                externalType.getName());
                    } else if (RamPackage.Literals.RVOID.isInstance(referenceValue)) {
                        referenceValue = RAMModelUtil.getVoidType(aspect.getStructuralView());
                    } else if (CorePackage.Literals.CORE_MODEL_ELEMENT.isInstance(referenceValue)) {
                        COREModelElement referencedElement = (COREModelElement) referenceValue;
                        referenceValue = localizeElement(domain, command, aspect, referencedElement);
                    }
                    
                    localElement.eSet(reference, referenceValue);
                }                    
            }
        }
    }
    
    /**
     * Creates a copy of the element and adds it to the local container.
     * Adds commands to the given command to add the element to the container, 
     * and a mapping reference to the parent mapping.
     * 
     * @param domain the {@link EditingDomain}
     * @param command the {@link CompoundCommand} to append commands to
     * @param model the local model
     * @param localContainer the local container to add the element copy to
     * @param element the element which is contained in another model
     * @return the element copy, which will be added to the container
     */
    private static COREModelElement createLocalElement(EditingDomain domain, CompoundCommand command, COREModel model,
            EObject localContainer, COREModelElement element) {
        COREModelElement localElement = cloneElement(element);
        
        command.append(AddCommand.create(domain, localContainer, element.eContainingFeature(), localElement));
                
        // Create mapping and add it to the correct container.
        EStructuralFeature mappingContainingFeature = null;
        EClass mappingClass = null;
        EObject container = null;
        
        if (RamPackage.Literals.OPERATION.isInstance(element)) {
            mappingContainingFeature = RamPackage.Literals.CLASSIFIER_MAPPING__OPERATION_MAPPINGS;
            mappingClass = RamPackage.Literals.OPERATION_MAPPING;
            container = getParentMappingFor(command, model, (COREModelElement) element.eContainer());
        } else if (RamPackage.Literals.ATTRIBUTE.isInstance(element)) {
            mappingContainingFeature = RamPackage.Literals.CLASSIFIER_MAPPING__ATTRIBUTE_MAPPINGS;
            mappingClass = RamPackage.Literals.ATTRIBUTE_MAPPING;
            container = getParentMappingFor(command, model, (COREModelElement) element.eContainer());
        } else if (RamPackage.Literals.RENUM_LITERAL.isInstance(element)) {
            mappingContainingFeature = RamPackage.Literals.ENUM_MAPPING__ENUM_LITERAL_MAPPINGS;
            mappingClass = RamPackage.Literals.ENUM_LITERAL_MAPPING;
            container = getParentMappingFor(command, model, (COREModelElement) element.eContainer());
        } else if (RamPackage.Literals.CLASSIFIER.isInstance(element)) {
            mappingClass = RamPackage.Literals.CLASSIFIER_MAPPING;
            container = COREModelUtil.getCOREModelComposition(model, (COREModel) EcoreUtil.getRootContainer(element));
            mappingContainingFeature = CorePackage.Literals.CORE_MODEL_COMPOSITION__COMPOSITIONS;
        } else if (RamPackage.Literals.RENUM.isInstance(element)) {
            mappingClass = RamPackage.Literals.ENUM_MAPPING;
            container = COREModelUtil.getCOREModelComposition(model, (COREModel) EcoreUtil.getRootContainer(element));
            mappingContainingFeature = CorePackage.Literals.CORE_MODEL_COMPOSITION__COMPOSITIONS;
        }
        
        createAndAddMappingReference(domain, command, container, mappingContainingFeature,
                mappingClass, element, localElement);
        
        if (RamPackage.Literals.OPERATION.isInstance(element)) {
            Operation externalOperation = (Operation) element;
            Operation localOperation = (Operation) localElement;
            
            EObject parentMapping = getParentMappingFor(command, model, externalOperation);
            
            for (int i = 0; i < externalOperation.getParameters().size(); i++) {
                Parameter externalParameter = externalOperation.getParameters().get(i);
                Parameter localParameter = localOperation.getParameters().get(i);
                
                createAndAddMappingReference(domain, command, parentMapping,
                        RamPackage.Literals.OPERATION_MAPPING__PARAMETER_MAPPINGS,
                        RamPackage.Literals.PARAMETER_MAPPING, externalParameter, localParameter);
            }
        }
        
        return localElement;
    }
    
    /**
     * Creates a mapping of the given class for from the from to the to element and adds it to the given owner.
     * The mapping will be a reference, which will be executed as a separate command 
     * to allow interested clients to receive the appropriate notification.
     * 
     * @param domain the {@link EditingDomain}
     * @param command the {@link CompoundCommand} to append commands to
     * @param owner the {@link EObject} the mapping should be added to
     * @param feature the {@link EStructuralFeature} of the owner for the mapping
     * @param mappingClass the {@link EClass} of the mapping class
     * @param from the element to map from, i.e., the external element
     * @param to the element to map to, i.e., the local element
     */
    private static void createAndAddMappingReference(EditingDomain domain, CompoundCommand command, EObject owner,
            EStructuralFeature feature, EClass mappingClass, COREModelElement from, COREModelElement to) {
        
        // todo: This should probably go into a controller, no?
        @SuppressWarnings("unchecked")
        COREMapping<COREModelElement> mapping =
                (COREMapping<COREModelElement>) RamFactory.eINSTANCE.create(mappingClass);
        
        // Set to property using command to force notification of derived property (reference) in model element.
        mapping.setFrom(from);
        // Also directly set it in order to be able to use it in getOrCreateLocalContainer.
        mapping.setTo(to);
        
        command.append(AddCommand.create(domain, owner, feature, mapping));
        command.append(SetCommand.create(domain, mapping, CorePackage.Literals.CORE_LINK__TO, to));
    }
    
    /**
     * Clones the given element.
     * Creates a deep clone except for {@link Class}, where operations, attributes and association ends are not copied.
     * 
     * @param original the element to be cloned
     * @param <T> the type of the element being cloned
     * @return a clone of the element
     */
    private static <T extends EObject> T cloneElement(T original) {
        T clone = EcoreUtil.copy(original);
        
        // First test for enums, since enums are also classifiers
        if (RamPackage.Literals.RENUM.isInstance(original)) {
            // Remove enum literals
            REnum rEnum = (REnum) clone;
            
            rEnum.getLiterals().clear();
        } else if (RamPackage.Literals.CLASSIFIER.isInstance(original)) {
            // Remove everything from classes except type parameters.
            Classifier classifier = (Classifier) clone;
            
            classifier.getOperations().clear();
            classifier.getAssociationEnds().clear();
            classifier.getSuperTypes().clear();
            
            if (RamPackage.Literals.CLASS.isInstance(classifier)) {
                Class clazz = (Class) classifier;
                
                clazz.getAttributes().clear();
            }
        }
        
        return clone;
    }
    
    /**
     * Returns the classifier mapping for the given classifier, where the classifier is the from element.
     * Besides looking within the current model, also checks the result of the current command, in order to ensure
     * that a mapping that will be added is already considered.
     * 
     * @param command the current command being populated
     * @param owner the {@link Aspect} containing model compositions with mappings
     * @param from the {@link Classifier} the classifier mapping is searched for where it is the from element
     * @return the {@link ClassifierMapping} for the given classifier, null if none found
     */
    private static COREMapping<COREModelElement> getParentMappingFor(Command command, COREModel owner,
                    COREModelElement from) {
        Collection<COREMapping<COREModelElement>> crossReferences = EMFModelUtil.findCrossReferencesOfType(owner, from, 
                CorePackage.Literals.CORE_LINK__FROM, CorePackage.Literals.CORE_MAPPING);
        
        COREMapping<COREModelElement> mapping = null;
        
        if (crossReferences.size() == 1) {
            mapping = crossReferences.iterator().next();
        } else if (crossReferences.isEmpty()) {
            Collection<COREMapping<COREModelElement>> mappings = EcoreUtil.getObjectsByType(command.getResult(), 
                    CorePackage.Literals.CORE_MAPPING);
            
            for (COREMapping<COREModelElement> currentMapping : mappings) {
                if (currentMapping.getFrom() == from) {
                    mapping = currentMapping;
                }
            }
        }
        
        return mapping;
    }
    
    /**
     * Returns the model composition within the given model with the given source set.
     * 
     * @param model the model containing model compositions
     * @param source the source for which the model composition is sought
     * @return the model composition for the given source
     */
    private static COREModelComposition getModelCompositionFor(COREModel model, COREModel source) {
        for (COREModelReuse modelReuse : model.getModelReuses()) {
            if (modelReuse.getSource() == source) {
                return modelReuse;
            }
        }
        
        for (COREModelExtension modelExtension : model.getModelExtensions()) {
            if (modelExtension.getSource() == source) {
                return modelExtension;
            } else {
                /**
                 * Check whether the model is within the extension hierarchy.
                 */
                Set<COREModel> extendedModels = COREModelUtil.collectExtendedModels(modelExtension.getSource());
                
                if (extendedModels.contains(source)) {
                    return modelExtension;
                }
            }
        }
        
        return null;
    }
    
}
