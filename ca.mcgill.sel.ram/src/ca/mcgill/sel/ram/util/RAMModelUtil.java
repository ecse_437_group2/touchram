package ca.mcgill.sel.ram.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.common.util.BasicEMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EStructuralFeature.Setting;
import org.eclipse.emf.ecore.util.EcoreUtil;

import ca.mcgill.sel.commons.StringUtil;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.COREMapping;
import ca.mcgill.sel.core.COREModel;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.COREModelElement;
import ca.mcgill.sel.core.COREModelElementComposition;
import ca.mcgill.sel.core.CORENamedElement;
import ca.mcgill.sel.core.COREPartialityType;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.util.COREModelUtil;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.AssociationEnd;
import ca.mcgill.sel.ram.Class;
import ca.mcgill.sel.ram.Classifier;
import ca.mcgill.sel.ram.ClassifierMapping;
import ca.mcgill.sel.ram.ImplementationClass;
import ca.mcgill.sel.ram.Layout;
import ca.mcgill.sel.ram.LayoutElement;
import ca.mcgill.sel.ram.ObjectType;
import ca.mcgill.sel.ram.Operation;
import ca.mcgill.sel.ram.OperationMapping;
import ca.mcgill.sel.ram.OperationType;
import ca.mcgill.sel.ram.Parameter;
import ca.mcgill.sel.ram.PrimitiveType;
import ca.mcgill.sel.ram.RAny;
import ca.mcgill.sel.ram.RBoolean;
import ca.mcgill.sel.ram.RSequence;
import ca.mcgill.sel.ram.RVoid;
import ca.mcgill.sel.ram.RamFactory;
import ca.mcgill.sel.ram.RamPackage;
import ca.mcgill.sel.ram.StructuralFeature;
import ca.mcgill.sel.ram.StructuralView;
import ca.mcgill.sel.ram.Traceable;
import ca.mcgill.sel.ram.Type;
import ca.mcgill.sel.ram.WovenAspect;

/**
 * Helper class with convenient static methods for working with RAM EMF model objects.
 *
 * @author mschoettle
 */
public final class RAMModelUtil {

    /**
     * Default name for constructor operation.
     */
    public static final String CONSTRUCTOR_NAME = "create";

    /**
     * Default name for destructor operation.
     */
    public static final String DESTRUCTOR_NAME = "destroy";

    /**
     * Creates a new instance of {@link RAMModelUtil}.
     */
    private RAMModelUtil() {
        // suppress default constructor
    }

    /**
     * Look for the closest Aspect realizing a parent feature.
     * Only return non conflict resolution aspects.
     *
     * @param feature - The feature we want the realizing aspect for.
     * @return the closest aspect, or null if none exists or they all are conflict resolution aspects.
     */
    public static Aspect getParentAspect(COREFeature feature) {
        if (feature == null || feature.getParent() == null) {
            return null;
        }
        COREFeature parent = feature.getParent();
        // Collect all realizing aspects

        Collection<Aspect> aspects = EMFModelUtil.collectElementsOfType(parent,
                CorePackage.Literals.CORE_FEATURE__REALIZED_BY, RamPackage.eINSTANCE.getAspect());

        // Look for the first aspect realizing only the parent feature
        for (Aspect aspect : aspects) {
            if (aspect.getRealizes().size() == 1) {
                return aspect;
            }
        }
        // No realizing aspect was found, search in parent
        return getParentAspect(parent);
    }

    /**
     * Creates a new operation with the given properties if it not already exists. Otherwise it returns null.
     *
     * @param owner - owner of the operation (used to check if operation is unique)
     * @param name the name of the operation
     * @param returnType the return type of the operation
     * @param parameters a list of parameters for the operation
     * @return a new {@link Operation} with the given properties set or null if the operation already exists
     */
    public static Operation createOperation(EObject owner, String name,
            Type returnType, List<Parameter> parameters) {
        return createOperation(owner, name, returnType, parameters, true);
    }

    /**
     * Creates a new operation with the given properties. It can check if operation already exists and returns null in
     * this case
     *
     * @param owner - owner of the operation (used to check if operation is unique)
     * @param name the name of the operation
     * @param returnType the return type of the operation
     * @param parameters a list of parameters for the operation
     * @param checkUnicity - true if you want to check if operation already exists
     * @return a new {@link Operation} with the given properties set or null if the operation already exists
     */
    public static Operation createOperation(EObject owner, String name,
            Type returnType, List<Parameter> parameters, boolean checkUnicity) {

        if (checkUnicity && !RAMModelUtil.isUniqueOperation(owner, name, returnType, parameters)) {
            return null;
        }

        Operation operation = RamFactory.eINSTANCE.createOperation();
        operation.setName(name);
        operation.setReturnType(returnType);

        if (parameters != null) {
            operation.getParameters().addAll(parameters);
        }

        return operation;
    }

    /**
     * Returns whether the given sub-class has the given super class as a super type.
     * I.e., the sub-class inherits from the super class.
     *
     * @param subType the class that is considered a sub-class
     * @param superType the classifier that is considered a super-type
     * @return true, if the sub-type class has the super-type classifier as a super type, false otherwise
     */
    public static boolean hasSuperClass(Class subType, Classifier superType) {
        for (Classifier classifier : subType.getSuperTypes()) {
            if (classifier == superType) {
                return true;
            }
        }

        return false;
    }

    /**
     * Checks whether the given name is unique inside the owners feature.
     *
     * @param owner the owner containing {@link CORENamedElement}s
     * @param feature the feature of the owner containing {@link CORENamedElement}s
     * @param name the name to be checked for uniqueness
     * @return true, if the given name is unique among the {@link CORENamedElement}s of the owners feature,
     *         false otherwise
     */
    public static boolean isUniqueName(EObject owner, EStructuralFeature feature, String name) {
        Collection<CORENamedElement> children =
                EMFModelUtil.collectElementsOfType(owner, feature, CorePackage.eINSTANCE.getCORENamedElement());

        for (CORENamedElement namedElement : children) {
            if (namedElement.getName() != null && namedElement.getName().equalsIgnoreCase(name)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Checks whether the given object is unique inside the owners feature.
     *
     * @param owner the owner containing {@link CoreNamedElement}s
     * @param feature the feature of the owner containing {@link CoreNamedElement}s
     * @param name the name to be checked for uniqueness
     * @param object the object to be checked for uniqueness (used for signature operation checking)
     * @return true, if the given name is unique among the {@link CoreNamedElement}s of the owners feature,
     *         false otherwise
     */
    public static boolean isUnique(EObject owner, EStructuralFeature feature, String name, EObject object) {
        if (feature == RamPackage.Literals.CLASSIFIER__OPERATIONS) {
            Operation operation = (Operation) object;
            return isUniqueOperation(owner, name, operation.getReturnType(), operation.getParameters());
        } else if (object instanceof COREModel) {
            COREModel model = (COREModel) object;
            return isUniqueAspect(model, name);
        } else {
            return isUniqueName(owner, feature, name);
        }
    }

    /**
     * Checks whether the given model is unique among the same model type.
     * 
     * @param model the model to be checked for uniqueness
     * @param name the name to be checked for uniqueness
     * @return true, if the name is unique, false otherwise
     */
    private static boolean isUniqueAspect(COREModel model, String name) {
        Collection<CORENamedElement> children = EMFModelUtil.collectElementsOfType(model.getCoreConcern(),
                CorePackage.Literals.CORE_CONCERN__MODELS, model.eClass());

        for (CORENamedElement namedElement : children) {
            if (namedElement.getName() != null && namedElement.getName().equalsIgnoreCase(name)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Checks whether the given operation is unique inside the owners operations.
     * (Check name and the list of parameters)
     *
     * @param owner the owner containing {@link CORENamedElement}s
     * @param name the name to be checked for uniqueness
     * @param returnType the return type of the operation
     * @param parameters the list of parameters to be checked for uniqueness
     * @return true, if the given name (and this list of parameters) is unique
     *         among the {@link Operation}s of the owners feature, false otherwise
     */
    public static boolean isUniqueOperation(EObject owner, String name, Type returnType, List<Parameter> parameters) {
        @SuppressWarnings("unchecked")
        List<Operation> children = (List<Operation>) owner.eGet(RamPackage.Literals.CLASSIFIER__OPERATIONS);

        for (Operation operationElement : children) {
            if (operationElement.getName() != null && operationElement.getName().equalsIgnoreCase(name)) {

                // If the one of two operations have null parameters and the other 0 parameters
                // Or if both operations have null parameters
                if ((parameters == null && operationElement.getParameters() == null)
                        || (parameters == null && operationElement.getParameters().size() == 0)
                        || (operationElement.getParameters() == null && parameters.size() == 0)) {
                    return false;
                }

                boolean equal = false;
                // If operations have parameters
                if (parameters != null && operationElement.getParameters() != null
                        && parameters.size() == operationElement.getParameters().size()) {
                    equal = true;
                    for (int i = 0; i < parameters.size() && equal; i++) {
                        if (parameters.get(i).getType() != operationElement.getParameters().get(i).getType()) {
                            equal = false;
                        }
                    }
                }
                
                // Check for equal return type names.
                if (returnType != null && operationElement.getReturnType() != null
                        && !returnType.getName().equals(operationElement.getReturnType().getName())) {
                    equal = false;
                }
                
                if (equal) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Constructs and returns related getter of the given attribute. If the operation already has a getter, it will
     * return null.
     *
     *
     * @param attribute - the attribute to extract its getter
     * @return the getter of the attribute
     */
    public static Operation createGetter(StructuralFeature attribute) {
        Class owner = (Class) attribute.eContainer();

        if (owner == null) {
            return null;
        }

        StructuralView structuralView = (StructuralView) owner.eContainer();
        String attributeName = StringUtil.toUpperCaseFirst(attribute.getName());
        Type attributeType = attribute.getType();

        // If the attribute parameter is an AssociationEnd object,
        // we have to check the multiplicity
        if (attribute instanceof AssociationEnd) {
            attributeType =
                    RAMModelUtil.getTypeDependingMultiplicityOfAssociationEnd((AssociationEnd) attribute,
                            structuralView);
        }

        String prefix = (attribute.getType() instanceof RBoolean) ? "is" : "get";

        Operation newGetter = RAMModelUtil.createOperation(owner, prefix + attributeName, attributeType, null);
        if (newGetter != null && attribute.isStatic()) {
            newGetter.setStatic(true);
        }

        return newGetter;
    }

    /**
     * Constructs and returns related setter of the given attribute. If the operation already has a setter, it will
     * return null.
     *
     * @param attribute - the attribute to extract its setter
     * @param parameterName The name of the parameter
     * @return the setter of the attribute
     */
    public static Operation createSetter(StructuralFeature attribute, String parameterName) {
        Class owner = (Class) attribute.eContainer();

        if (owner == null) {
            return null;
        }

        // get the void type from the structural view
        StructuralView structuralView = (StructuralView) owner.eContainer();
        RVoid voidType = RAMModelUtil.getVoidType(structuralView);
        String attributeName = StringUtil.toUpperCaseFirst(attribute.getName());

        Parameter parameter = RamFactory.eINSTANCE.createParameter();
        Type attributeType = attribute.getType();

        // If the attribute parameter is an AssociationEnd object,
        // we have to check the multiplicity
        if (attribute instanceof AssociationEnd) {
            attributeType =
                    RAMModelUtil.getTypeDependingMultiplicityOfAssociationEnd((AssociationEnd) attribute,
                            structuralView);
        }

        parameter.setType(attributeType);
        parameter.setName(parameterName);
        List<Parameter> parameters = new ArrayList<Parameter>();
        parameters.add(parameter);

        Operation newSetter = RAMModelUtil.createOperation(owner, "set" + attributeName, voidType, parameters);
        if (newSetter != null && attribute.isStatic()) {
            newSetter.setStatic(true);
        }

        return newSetter;
    }

    /**
     * Check if the given attribute has already its getter into its owner.
     *
     * @param attribute - the attribute to check
     * @return true if the attribute has its getter into its owner
     */
    public static boolean isGetterUnique(StructuralFeature attribute) {
        Operation getter = createGetter(attribute);
        return getter == null;
    }

    /**
     * Check if the given attribute has already its setter into its owner.
     *
     * @param attribute - the attribute to check
     * @return true if the attribute has its setter into its owner
     */
    public static boolean isSetterUnique(StructuralFeature attribute) {
        Operation setter = createSetter(attribute, attribute.getName());
        return setter == null;
    }

    /**
     * Checks the multiplicity of the {@link AssociationEnd} in order to create or not an array type rather than a
     * simple type. Checks the upperBound : if it is upper than 1, it is an array of the {@link AssociationEnd} type.
     *
     * @param attribute {@link AssociationEnd} which have to be checked for multiplicity.
     * @param structuralView The corresponding {@link StructuralView}.
     * @return The new type of the attribute (array of the {@link AssociationEnd} type or just the
     *         {@link AssociationEnd} type.
     */
    public static Type getTypeDependingMultiplicityOfAssociationEnd(AssociationEnd attribute,
            StructuralView structuralView) {
        Type attributeType = attribute.getType();
        AssociationEnd asso = attribute;

        // If the upperBound is not 1, we need an array
        if (asso.getUpperBound() != 1) {
            RSequence rSeq = null;
            for (Type t : structuralView.getTypes()) {
                if (t instanceof RSequence && ((RSequence) t).getType() == attributeType) {
                    rSeq = (RSequence) t;
                }
            }
            if (rSeq == null) {
                rSeq = RamFactory.eINSTANCE.createRSequence();
                rSeq.setType((ObjectType) attributeType);
                structuralView.getTypes().add(rSeq);
            }
            attributeType = rSeq;
        }

        return attributeType;
    }

    /**
     * Gets the void type from the structural view.
     *
     * @param structuralView the structuralView
     * @return the void type, null if not found
     */
    public static RVoid getVoidType(StructuralView structuralView) {
        for (Type type : structuralView.getTypes()) {
            if (RVoid.class.isInstance(type)) {
                return (RVoid) type;
            }
        }

        return null;
    }

    /**
     * Returns a list of all primitive types contained in the given structural view.
     *
     * @param structuralView the {@link StructuralView}
     * @return a list of all primitive types of the given structural view
     */
    public static List<PrimitiveType> getPrimitiveTypes(StructuralView structuralView) {
        List<PrimitiveType> primitiveTypes = new ArrayList<PrimitiveType>();

        for (Type type : structuralView.getTypes()) {
            if (type instanceof PrimitiveType) {
                primitiveTypes.add((PrimitiveType) type);
            }
        }

        return primitiveTypes;
    }
    
    /**
     * Returns the primitive type contained in the given structural view corresponding to the name.
     * 
     * @param structuralView the {@link StructuralView} containing the types
     * @param name the name of the primitive type
     * @return the {@link PrimitiveType} with the given name, null if none found
     */
    public static PrimitiveType getPrimitiveTypeByName(StructuralView structuralView, String name) {
        PrimitiveType type = null;
        
        for (PrimitiveType primitiveType : getPrimitiveTypes(structuralView)) {
            if (name.equals(primitiveType.getName())) {
                type = primitiveType;
                break;
            }
        }
        
        return type;
    }

    /**
     * Creates a new {@link Aspect} with the given name, and an empty {@link StructuralView} containing default types.
     *
     * @param name - The name to give to the aspect
     * @return the newly created {@link Aspect}
     */
    public static Aspect createAspect(String name) {
        return createAspect(name, null);
    }

    /**
     * Creates a new {@link Aspect} with an empty {@link StructuralView} containing default types.
     *
     * @param baseName - The baseName to give to the aspect.
     * @param concern - The concern containing the aspect. Only used to create a unique name.
     * @return the newly created {@link Aspect}
     */
    public static Aspect createAspect(String baseName, COREConcern concern) {
        Aspect aspect = RamFactory.eINSTANCE.createAspect();
        Collection<Aspect> aspects =
                EMFModelUtil.collectElementsOfType(concern, CorePackage.Literals.CORE_CONCERN__MODELS,
                        RamPackage.eINSTANCE.getAspect());
        aspect.setName(COREModelUtil.createUniqueNameFromElements(baseName, aspects));

        // create structural view as it is needed
        StructuralView structuralView = RamFactory.eINSTANCE.createStructuralView();
        aspect.setStructuralView(structuralView);

        // create default types
        createDefaultTypes(structuralView);

        // create empty layout
        createLayout(aspect, structuralView);

        return aspect;
    }

    /**
     * Creates all the primitive types that do not currently exist in the structural view (and sets them...).
     *
     * @param structuralView the structural view of interest
     */
    public static void createDefaultTypes(StructuralView structuralView) {
        if (getTypeInstance(structuralView, RVoid.class) == null) {
            structuralView.getTypes().add(RamFactory.eINSTANCE.createRVoid());
        }

        if (getTypeInstance(structuralView, RAny.class) == null) {
            structuralView.getTypes().add(RamFactory.eINSTANCE.createRAny());
        }

        // add all primitive types
        EClass primitiveTypeClass = RamPackage.eINSTANCE.getPrimitiveType();

        for (EClassifier classifier : RamPackage.eINSTANCE.getEClassifiers()) {
            if (classifier instanceof EClass) {
                EClass clazz = (EClass) classifier;

                // is it a PrimitiveType but not an Enum or Array
                if (!clazz.isAbstract() && primitiveTypeClass.isSuperTypeOf(clazz)
                        && !RamPackage.eINSTANCE.getREnum().equals(clazz)
                        && !RamPackage.eINSTANCE.getRArray().equals(clazz)) {

                    boolean alreadyExists = false;

                    // if the type already exists we don't want to add it another time
                    for (Type type : structuralView.getTypes()) {

                        if (type.eClass().equals(clazz)) {
                            alreadyExists = true;
                            break;
                        }

                    }

                    if (!alreadyExists) {
                        Type newObject = (Type) RamFactory.eINSTANCE.create(clazz);
                        structuralView.getTypes().add(newObject);
                    }
                }
            }
        }
    }

    /**
     * Returns the type instance of the given class that is a type of the given structural view.
     * If no such type is found, null is returned.
     *
     * @param structuralView the {@link StructuralView} containing the types
     * @param typeClass the {@link java.lang.Class} of which an instance is requested
     * @param <T> the type of the requested type
     * @return the type instance of the given class, null if none is found
     */
    private static <T extends Type> T getTypeInstance(StructuralView structuralView, java.lang.Class<T> typeClass) {
        for (final Type type : structuralView.getTypes()) {
            if (typeClass.isInstance(type)) {
                @SuppressWarnings("unchecked")
                T typed = (T) type;
                return typed;
            }
        }
        return null;
    }

    /**
     * Creates a new layout for a given {@link StructuralView}.
     * The layout is the {@link ca.mcgill.sel.ram.impl.ContainerMapImpl} specifically
     * that holds all {@link LayoutElement} for children of the given {@link StructuralView}.
     *
     * @param aspect the aspect
     * @param structuralView the {@link StructuralView} holding the {@link LayoutElement} for its children
     */
    public static void createLayout(Aspect aspect, StructuralView structuralView) {
        Layout layout = RamFactory.eINSTANCE.createLayout();

        // workaround used here since creating the map, adding the values and then putting it doesn't work
        // EMF somehow does some magic with the passed map instance
        layout.getContainers().put(structuralView, new BasicEMap<EObject, LayoutElement>());

        aspect.setLayout(layout);
    }

    /**
     * Returns the operation that the given operation was mapped from.
     * Returns <code>null</code> if the operation wasn't mapped.
     * If the given operation was mapped several times, only the first found operation is returned.
     * 
     * @param operation the operation
     * @return the operation the given operation was mapped from, null if it wasn't mapped
     */
    public static Operation getMappedFrom(Operation operation) {
        Collection<OperationMapping> mappings = EMFModelUtil.findCrossReferencesOfType(operation, 
                    CorePackage.Literals.CORE_LINK__TO, RamPackage.Literals.OPERATION_MAPPING);   
        
        for (OperationMapping mapping : mappings) {
            Operation fromOperation = mapping.getFrom();

            return fromOperation;
        }

        return null;
    }

    /**
     * Collects a set of classifiers that contains the classifier, its super types and all other classifiers
     * that this classifiers is mapped to. Performs this "bottom-up", i.e., starting at the current aspect up the
     * extended aspects hierarchy.
     * If the given classifier is not from the given aspect, the classifier is resolved
     * to the one from the given aspect so that the "bottom-up" approach can be used.
     *
     * @param aspect the aspect containing the classifier
     * @param classifier the classifier all classifiers to retrieve for
     * @return a set of classifiers that this classifier is mapped to, including the classifier itself
     */
    public static Set<Classifier> collectClassifiersFor(Aspect aspect, Classifier classifier) {
        return collectClassifiersFor(aspect, classifier, true);
    }

    /**
     * Collects a set of classifiers that contains the classifier, its super types and all other classifiers
     * that this classifiers is mapped to. Performs this "bottom-up", i.e., starting at the current aspect up the
     * extended aspects hierarchy.
     * If the given classifier is not from the given aspect, the classifier is resolved
     * to the one from the given aspect so that the "bottom-up" approach can be used.
     *
     * @param aspect the aspect containing the classifier
     * @param classifier the classifier all classifiers to retrieve for
     * @param includeSuperTypes indicates if super types must be included in the final result
     * @return a set of classifiers that this classifier is mapped to, including the classifier itself
     */
    public static Set<Classifier> collectClassifiersFor(Aspect aspect, Classifier classifier,
            boolean includeSuperTypes) {
        Set<Classifier> classifiers = new HashSet<Classifier>();

        if (!classifier.eIsProxy()) {
            Classifier actualClassifier = resolveClassifier(aspect, classifier);

            collectClassifiersRecursive(classifiers, aspect, actualClassifier, includeSuperTypes);
            // Finally, when extending another aspect and a classifier of the other aspect is called,
            // but a classifier in the current aspect with the same name exists, it has to be considered as well.
            for (Classifier ownClassifier : aspect.getStructuralView().getClasses()) {
                for (Classifier extendedClassifier : new HashSet<Classifier>(classifiers)) {
                    if (isNameEqual(ownClassifier, extendedClassifier)) {
                        classifiers.add(ownClassifier);
                    }
                }
            }
        }

        return classifiers;
    }

    /**
     * Checks whether the name of two given classifiers are the same.
     * In case of implementation classes with generics, their type parameters have to match as well.
     *
     * @param classifier1 the first classifier
     * @param classifier2 the second classifier
     * @return true, if the name matches, false otherwise
     */
    public static boolean isNameEqual(Classifier classifier1, Classifier classifier2) {
        if (classifier2.getName().equals(classifier1.getName())) {
            if (classifier2 instanceof ImplementationClass
                    && classifier1 instanceof ImplementationClass) {
                ImplementationClass c1 = (ImplementationClass) classifier2;
                ImplementationClass c2 = (ImplementationClass) classifier1;
                if (isNameEqual(c1, c2)) {
                    return true;
                }
            } else if (classifier1 instanceof Class
                    && classifier2 instanceof Class) {
                return true;
            }
        }

        return false;
    }

    /**
     * Checks whether the two given implementation classes are the same.
     * Two implementation classes are the same if besides their name,
     * also the type parameters match in type and name.
     *
     * @param class1 the first {@link ImplementationClass}
     * @param class2 the second {@link ImplementationClass}
     * @return true, if they are the same, false otherwise
     */
    private static boolean isNameEqual(ImplementationClass class1, ImplementationClass class2) {
        if (class1.getInstanceClassName().equals(class2.getInstanceClassName())) {
            return typeParametersMatch(class1, class2);
        }

        return false;
    }

    /**
     * Returns whether the type parameters of two given implementation classes are the same.
     *
     * @param class1 the first {@link ImplementationClass}
     * @param class2 the second {@link ImplementationClass}
     * @return true, if they are the same, false otherwise
     */
    public static boolean typeParametersMatch(ImplementationClass class1, ImplementationClass class2) {
        if (class1.getTypeParameters().size() == class2.getTypeParameters().size()) {
            for (int index = 0; index < class1.getTypeParameters().size(); index++) {
                Type type = class1.getTypeParameters().get(index).getGenericType();
                Type type2 = class2.getTypeParameters().get(index).getGenericType();
                if ((type != null ^ type2 != null)
                        || (type != null && type2 != null && !type.getName().equals(type2.getName()))) {
                    return false;
                }
            }

            return true;
        }

        return false;
    }

    /**
     * Recursively collects a set of classifiers that contains the classifier, classifiers
     * that this classifiers is mapped to, and optionally its super types.
     * Checks the directly extended aspects and the model reuses of the given aspect.
     * Performs this recursively for every classifier found, in order to retrieve the complete set of classifiers.
     *
     * @param classifiers the current set of found classifiers, which classifiers are added to
     * @param aspect the aspect containing the classifier
     * @param classifier the classifier all classifiers to retrieve for
     * @param includeSuperTypes indicates if super types must be included or not
     */
    private static void collectClassifiersRecursive(Set<Classifier> classifiers, Aspect aspect, Classifier classifier,
            boolean includeSuperTypes) {
        classifiers.add(classifier);

        Set<COREModelComposition> modelCompositions = new HashSet<COREModelComposition>();
        modelCompositions.addAll(aspect.getModelExtensions());
        modelCompositions.addAll(aspect.getModelReuses());

        for (COREModelComposition modelComposition : modelCompositions) {
            for (COREModelElementComposition<?> composition : modelComposition.getCompositions()) {
                COREMapping<?>  mapping = (COREMapping<?>) composition;
                if (mapping.getTo() == classifier) {
                    collectClassifiersRecursive(classifiers, (Aspect) modelComposition.getSource(),
                            (Classifier) mapping.getFrom(), includeSuperTypes);
                }
            }
        }

        // Classifiers from extended aspects with the same name as the called classifier
        // need to be considered, because they are implicitly mapped.
        // At the same time, if the extendedClassifier is
        // an Implementation Class, it needs to be considered as well.
        modelCompositions.addAll(COREModelUtil.collectModelExtensions(aspect));
        
        for (COREModelComposition modelComposition : modelCompositions) {
            Aspect source = (Aspect) modelComposition.getSource();
            
            StructuralView structuralView = source.getStructuralView();

            for (Classifier extendedClassifier : structuralView.getClasses()) {
                if (modelComposition.eClass() == CorePackage.Literals.CORE_MODEL_EXTENSION
                        && !(extendedClassifier instanceof ImplementationClass)) {
                    if (classifier.getName().equals(extendedClassifier.getName())) {
                        collectClassifiersRecursive(classifiers, source, extendedClassifier,
                                includeSuperTypes);
                    }
                } else if (extendedClassifier instanceof ImplementationClass
                        && classifier instanceof ImplementationClass) {
                    ImplementationClass c1 = (ImplementationClass) extendedClassifier;
                    ImplementationClass c2 = (ImplementationClass) classifier;
                    if (isNameEqual(c1, c2)) {
                        collectClassifiersRecursive(classifiers, source, extendedClassifier,
                                includeSuperTypes);
                    }
                }
            }
        }

        // Add the super types of each possible classifier.
        if (includeSuperTypes) {
            for (Classifier superType : classifier.getSuperTypes()) {
                collectClassifiersRecursive(classifiers, aspect, superType, includeSuperTypes);
            }
        }
    }

    /**
     * Resolves the given classifier to the corresponding classifier in the given aspect.
     * Returns the given classifier if it is already contained in the aspect.
     * For instance, mapping the classifier to another classifier B in the given aspect
     * would result in B being returned.
     *
     * @param aspect the {@link Aspect}
     * @param classifier the {@link Classifier} to find the corresponding one in the aspect for
     * @return the given classifier, if it is already contained in the aspect,
     *         otherwise the located classifier in the aspect corresponding to the given classifier
     */
    public static Classifier resolveClassifier(Aspect aspect, Classifier classifier) {
        Classifier clazz = classifier;

        if (EcoreUtil.getRootContainer(classifier) != aspect && !classifier.eIsProxy()) {
            Set<COREModelComposition> modelCompositions = new HashSet<>();
            modelCompositions.addAll(COREModelUtil.collectModelExtensions(aspect));
            modelCompositions.addAll(COREModelUtil.collectAllModelReuses(aspect));

            Collection<Setting> crossReferences =
                    EcoreUtil.UsageCrossReferencer.find(clazz, clazz.eResource().getResourceSet());

            for (Setting crossReference : crossReferences) {
                if (crossReference.getEStructuralFeature() == CorePackage.Literals.CORE_LINK__FROM) {
                    // the following cast is always safe, because we know it is a classifier
                    ClassifierMapping classifierMapping = (ClassifierMapping) crossReference.getEObject();
                    
                    COREModelComposition modelComposition = (COREModelComposition) classifierMapping.eContainer();
                    if (modelCompositions.contains(modelComposition) && classifierMapping.getTo() != null) {
                        return resolveClassifier(aspect, classifierMapping.getTo());
                    }
                }
            }

            for (Classifier currentClassifier : aspect.getStructuralView().getClasses()) {
                if (isNameEqual(clazz, currentClassifier)) {
                    return currentClassifier;
                }
            }
        }

        return clazz;
    }

    /**
     * Finds the index of where to add a new operation to the class.
     * Constructors are added at the beginning, but after existing constructors.
     * Destructors are added after the constructors, but also after existing destructors.
     *
     * @param owner the class to add an operation to
     * @param type the type of the operation
     * @return the index of where to add a new operation for the given type
     */
    public static int findConstructorIndexFor(Classifier owner, OperationType type) {
        int index = 0;

        for (Operation operation : owner.getOperations()) {
            switch (operation.getOperationType()) {
                case CONSTRUCTOR:
                    index++;
                    break;
                case DESTRUCTOR:
                    if (type == OperationType.DESTRUCTOR) {
                        index++;
                    }
                    break;
                case NORMAL:
                    return index;
            }
        }

        return index;
    }

    /**
     * Returns whether the given class is empty.
     * A class is empty if it has no (or only partial) operations and attributes and no outgoing associations.
     * It is okay if there are incoming associations.
     *
     * @param clazz the {@link Class} to check
     * @return true, if the class is empty, false otherwise
     */
    public static boolean isClassEmpty(Class clazz) {
        for (AssociationEnd end : clazz.getAssociationEnds()) {
            if (end.isNavigable()) {
                return false;
            }
        }

        for (Operation operation : clazz.getOperations()) {
            if (operation.getPartiality() != COREPartialityType.CONCERN) {
                return false;
            }
        }

        return clazz.getAttributes().size() == 0;
    }

    /**
     * Returns true if the given class can be not abstract. The class can't be not abstract if it contains at least one
     * abstract operation.
     *
     * @param clazz - the class asked
     * @return true if the class can be not abstract, false otherwise.
     */
    public static boolean classCanBeNotAbstract(Class clazz) {
        for (Operation op : clazz.getOperations()) {
            if (op.isAbstract()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Returns the implementation class contained in the structural view that has the given instance class name.
     * Returns null if it doesn't exist.
     *
     * @param structuralView the {@link StructuralView} that contains the classifiers
     * @param instanceClassName the fully qualified name of the implementation class
     * @return the {@link ImplementationClass}, null if none found
     */
    public static ImplementationClass getImplementationClassByName(StructuralView structuralView,
            String instanceClassName) {
        Collection<ImplementationClass> implClasses =
                EcoreUtil.getObjectsByType(structuralView.getClasses(), RamPackage.Literals.IMPLEMENTATION_CLASS);
        for (ImplementationClass implClass : implClasses) {
            if (instanceClassName.equals(implClass.getInstanceClassName())) {
                return implClass;
            }
        }

        return null;
    }

    /**
     * Returns true if the given operation is a constructor or a destructor.
     *
     * @param operation - the operation to test.
     * @return true if the given operation is a constructor or a destructor.
     */
    public static boolean isConstructorOrDestructor(Operation operation) {
        OperationType op = operation.getOperationType();
        return op.equals(OperationType.CONSTRUCTOR) || op.equals(OperationType.DESTRUCTOR);
    }

    /**
     * Returns true if both operations have the same signature.
     *
     * @param operation1 The first {@link Operation}
     * @param operation2 The second {@link Operation}
     * @return whether both operations have the same signature
     */
    public static boolean hasSameSignature(Operation operation1, Operation operation2) {
        if (!operation1.getName().equals(operation2.getName())
                || operation1.getParameters().size() != operation2.getParameters().size()
                || !operation1.getReturnType().getName().equals(operation2.getReturnType().getName())) {
            return false;
        }
        
        for (int i = 0; i < operation1.getParameters().size(); i++) {
            if (!operation1.getParameters().get(i).getType().equals(operation2.getParameters().get(i).getType())) {
                return false;
            }
        }
        
        return true;
    }

    /**
     * Returns the multiplicity representation of the association end.
     *
     * @param associationEnd The end of interest.
     * @param isKeyIndexed Whether the feature selection is of type KeyIndexed
     * @return The multiplicity string for the association end.
     */
    public static String getMultiplicity(AssociationEnd associationEnd, boolean isKeyIndexed) {
        String upperBound;

        if (isKeyIndexed) {
            return "0..1";
        } else if (associationEnd.getUpperBound() == -1) {
            upperBound = "*";
        } else {
            upperBound = String.valueOf(associationEnd.getUpperBound());
        }

        if (associationEnd.getLowerBound() == associationEnd.getUpperBound()) {
            return upperBound;
        } else {
            StringBuilder builder = new StringBuilder();

            builder.append(associationEnd.getLowerBound());
            builder.append("..");
            builder.append(upperBound);

            return builder.toString();
        }
    }

    /**
     * For a given Traceable 'origin' element, get the corresponding element from the woven aspect.
     * In woven aspects, the trace contains information that links elements of the aspect (Woven_Association for
     * example) to the origin elements from the aspects that were woven to obtain this aspect (Association and ArrayList
     * for example)
     * This method takes an 'origin' element (such as Association.AssociationCollection), and returns the associated
     * element in the woven aspect (in this case, it could be Woven_Association.ArrayList<Data>)
     *
     * @param aspect - The woven aspect containing the element.
     * @param originElement - The element we want to resolve.
     * @param <T> - The type of elements stored in the given mappings
     * @return The highest level 'from' element, or null if none is found.
     */
    public static <T extends COREModelElement> T getWovenElementFromOrigin(Aspect aspect, T originElement) {
        for (WovenAspect wa : aspect.getWovenAspects()) {
            T wovenElement = getWovenElementFromTrace(wa, originElement);
            if (wovenElement != null && wovenElement != originElement) {
                return wovenElement;
            }
        }
        return null;
    }

    /**
     * For a given Traceable 'origin' element, get the corresponding element from the woven aspect.
     * In woven aspects, the trace contains information that links elements of the aspect (Woven_Association for
     * example) to the origin elements from the aspects that were woven to obtain this aspect (Association and ArrayList
     * for example)
     * This method takes an 'origin' element (such as Association.AssociationCollection), and returns the associated
     * element in the woven aspect (in this case, it could be Woven_Association.ArrayList<Data>)
     *
     * @param wa - The woven aspect containing the element.
     * @param originElement - The element we want to resolve.
     * @param <T> - The type of elements stored in the given mappings
     * @return The highest level 'from' element, or the given origin element if none is found.
     */
    private static <T extends COREModelElement> T getWovenElementFromTrace(WovenAspect wa, T originElement) {
        if (originElement != null) {
            for (Traceable from : wa.getWovenElements().keySet()) {
                Traceable to = wa.getWovenElements().get(from);
                if (to == originElement) {
                    // No choice but to do this, because it can happen that ImplemenationClasses are mapped to Classes,
                    // thus fromElement.eClass().isInstance(from) would return false even if we only want to cast to
                    // Classifier.
                    try {
                        @SuppressWarnings("unchecked")
                        T result = (T) from;
                        return result;
                    } catch (ClassCastException cce) {
                    }
                }
            }
            for (WovenAspect child : wa.getChildren()) {
                T wovenElement = getWovenElementFromTrace(child, originElement);
                if (wovenElement != null) {
                    return wovenElement;
                }
            }
        }
        return originElement;
    }

    /**
     * For a given Traceable 'to' element, get the first mapping saved in the hierarchy of woven aspect.
     * This works only if the wovenAspects are ordered from highest level to lowest level elements.
     * Will look in the given WovenAspect and its hierarchy before moving to next WovenAspect.
     *
     * @param element - The traceable for which we are looking for the origin element.
     * @param <T> - Type of the element we're looking for
     * @return The highest level 'origin' element, or null if none is found.
     */
    public static <T extends Traceable> T getOriginElement(T element) {
        Aspect aspect = EMFModelUtil.getRootContainerOfType(element, RamPackage.Literals.ASPECT);
        if (aspect != null) {
            for (WovenAspect wa : aspect.getWovenAspects()) {
                T from = getFirstMapping(wa, element);
                if (from != null) {
                    return from;
                }
            }
        }
        return null;
    }

    /**
     * For a given Traceable element, look for the first mapping saved in woven aspect.
     * Will look in a the wovenAspect and then its hierarchy of descendants.
     *
     * @param wa - The wovenAspect containing the tracing information to search.
     * @param element - The traceable for which we are looking for the origin element.
     * @param <T> - Type of the element we're looking for
     * @return The highest level 'from' element, or null if none is found.
     */
    private static <T extends Traceable> T getFirstMapping(WovenAspect wa, T element) {
        if (wa.getWovenElements().containsKey(element)) {
            @SuppressWarnings("unchecked")
            T fromElement = (T) wa.getWovenElements().get(element);
            return fromElement;
        }
        for (WovenAspect child : wa.getChildren()) {
            T from = getFirstMapping(child, element);
            if (from != null) {
                return from;
            }
        }
        return null;
    }

    /**
     * Checks if the feature selection of the given association end is of type key indexed.
     *
     * @param associationEnd The end to check the selection for
     * @return true if the association end has a key indexed feature selection
     */
    public static boolean hasKeyIndexedSelection(AssociationEnd associationEnd) {
        if (associationEnd.getFeatureSelection() != null
                && associationEnd.getFeatureSelection().getReuse() != null
                && associationEnd.getFeatureSelection().getReuse().getSelectedConfiguration() != null) {
            HashSet<COREFeature> selectedFeatures = new HashSet<COREFeature>(associationEnd.getFeatureSelection()
                    .getReuse().getSelectedConfiguration().getSelected());
            return hasKeyIndexedSelection(selectedFeatures);
        }
        return false;
    }

    /**
     * Checks if the feature selections is of type key indexed.
     *
     * @param selectedFeatures The set of selected features
     * @return true if the feature selection has a key indexed feature selection
     */
    public static boolean hasKeyIndexedSelection(Set<COREFeature> selectedFeatures) {
        for (COREFeature feature : selectedFeatures) {
            if (feature.getName().matches("(KeyIndexed|HashMap|TreeMap)")) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns whether an association can be created between the two given classifiers.
     * 
     * @param from the {@link Classifier} from
     * @param to the {@link Classifier} to
     * @return true, if an association can be created, false otherwise
     */
    public static boolean canCreateAssociation(Classifier from, Classifier to) {
        return !from.isDataType() && !to.isDataType() && !(from instanceof ImplementationClass);
    }

    /**
     * Returns whether the from class can inherit from the to class.
     * 
     * @param from the {@link Classifier} from
     * @param to the {@link Classifier} to
     * @return true, if an inheritance can be created between the two, false otherwise
     */
    public static boolean canCreateInheritance(Classifier from, Classifier to) {
        return !from.isDataType() && !to.isDataType() && !(from instanceof ImplementationClass)
                    && from instanceof Class && !(to instanceof PrimitiveType);
    }
}
