/**
 */
package ca.mcgill.sel.ram;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Literal Float</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.ram.LiteralFloat#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.ram.RamPackage#getLiteralFloat()
 * @model
 * @generated
 */
public interface LiteralFloat extends LiteralSpecification {
    /**
     * Returns the value of the '<em><b>Value</b></em>' attribute.
     * The default value is <code>"0.0f"</code>.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Value</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Value</em>' attribute.
     * @see #setValue(float)
     * @see ca.mcgill.sel.ram.RamPackage#getLiteralFloat_Value()
     * @model default="0.0f" required="true"
     * @generated
     */
    float getValue();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.ram.LiteralFloat#getValue <em>Value</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Value</em>' attribute.
     * @see #getValue()
     * @generated
     */
    void setValue(float value);

} // LiteralFloat
