/**
 */
package ca.mcgill.sel.ram;

import ca.mcgill.sel.core.COREMapping;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Enum Literal Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.mcgill.sel.ram.RamPackage#getEnumLiteralMapping()
 * @model
 * @generated
 */
public interface EnumLiteralMapping extends COREMapping<REnumLiteral> {
} // EnumLiteralMapping
