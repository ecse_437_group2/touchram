package ca.mcgill.sel.ram.ui.components.navigationbar;

import org.mt4j.AbstractMTApplication;
import org.mt4j.components.visibleComponents.shapes.MTRectangle;
import org.mt4j.sceneManagement.Iscene;
import org.mt4j.sceneManagement.transition.AbstractTransition;
import org.mt4j.util.MTColor;
import org.mt4j.util.animation.AnimationEvent;
import org.mt4j.util.animation.IAnimation;
import org.mt4j.util.animation.IAnimationListener;
import org.mt4j.util.animation.ani.AniAnimation;
import org.mt4j.util.math.Vector3D;

/**
 * The Class SlideTransition.
 * 
 * @author Christopher Ruff
 */
public class SlideUpDownTransition extends AbstractTransition {
    
    /** The type of slide. */
    private boolean slideUpDown;
    
    /** The app. */
    private AbstractMTApplication app;
    
    /** The finished. */
    private boolean finished;
    
    /** The last scene. */
    private Iscene lastScene;
    
    /** The next scene. */
    private Iscene nextScene;
    
    /** The anim. */
    private IAnimation anim;
    
    /** The duration. */
    private int duration; 
    
    /** The last scene rectangle. */
    private MTRectangle lastSceneRectangle;
    
    /** The next scene rectangle. */
    private MTRectangle nextSceneRectangle;
    
    /**
     * Instantiates a new slide transition.
     * 
     * @param mtApplication the mt application
     */
    public SlideUpDownTransition(AbstractMTApplication mtApplication) {
        this(mtApplication, 2000);
    }
    
    /**
     * Instanciates a new slide transition up or down.
     * @param mtApplication application
     * @param duration duration
     */
    public SlideUpDownTransition(AbstractMTApplication mtApplication, long duration) {
        this(mtApplication, duration, true);
    }
    
    /**
     * Instantiates a new slide transition.
     * 
     * @param mtApplication the mt application
     * @param duration the duration
     * @param slideUp toggle a slide up or down
     */
    public SlideUpDownTransition(final AbstractMTApplication mtApplication, long duration, boolean slideUp) {
        super(mtApplication, "Slide Transition");
        this.app = mtApplication;
        this.duration = (int) duration;
        this.finished = true;
        this.slideUpDown = slideUp;
        anim = new AniAnimation(app.height, 0, this.duration, AniAnimation.SINE_IN, this);
        if (!slideUpDown) {
            ((AniAnimation) anim).reverse();
        }
        anim.addAnimationListener(new IAnimationListener() {
            @Override
            public void processAnimationEvent(AnimationEvent ae) {
                switch (ae.getId()) {
                    case AnimationEvent.ANIMATION_STARTED:
                    case AnimationEvent.ANIMATION_UPDATED:
                        nextSceneRectangle.translateGlobal(new Vector3D(0, ae.getDelta(), 0));
                        lastSceneRectangle.translateGlobal(new Vector3D(0, ae.getDelta(), 0));
                        break;
                    case AnimationEvent.ANIMATION_ENDED:
                        nextSceneRectangle.translateGlobal(new Vector3D(0, ae.getDelta(), 0));
                        lastSceneRectangle.translateGlobal(new Vector3D(0, ae.getDelta(), 0));
                        finished = true;
                        break;
                    default:
                        break;
                }
            } });
//      ((Animation)anim).setResetOnFinish(true);
    }

    /**Constructor handling the slide up transition.
     * 
     * @param mtApplication application context
     * @param duration time for the transition to execute
     * @param slideUp triggers up whether down 
     * @param c boolean
     */
    public SlideUpDownTransition(final AbstractMTApplication mtApplication, long duration, boolean slideUp, boolean c) {
        super(mtApplication, "Slide Transition");
        this.app = mtApplication;
        this.duration = (int) duration;
        this.finished = true;
        this.slideUpDown = slideUp;
        anim = new AniAnimation(app.height, 0, this.duration, AniAnimation.SINE_IN, this);
        if (!slideUpDown) {
            ((AniAnimation) anim).reverse();
        }
        anim.addAnimationListener(new IAnimationListener() {
            @Override
            public void processAnimationEvent(AnimationEvent ae) {
                switch (ae.getId()) {
                    case AnimationEvent.ANIMATION_STARTED:
                    case AnimationEvent.ANIMATION_UPDATED:
                        nextSceneRectangle.translateGlobal(new Vector3D(ae.getDelta(), 0, 0));
                        lastSceneRectangle.translateGlobal(new Vector3D(ae.getDelta(), 0, 0));
                        break;
                    case AnimationEvent.ANIMATION_ENDED:
                        nextSceneRectangle.translateGlobal(new Vector3D(ae.getDelta(), 0, 0));
                        lastSceneRectangle.translateGlobal(new Vector3D(ae.getDelta(), 0, 0));
                        finished = true;
                        break;
                    default:
                        break;
                }
            } });
    }

    /* (non-Javadoc)
     * @see org.mt4j.sceneManagement.transition.ITransition#isFinished()
     */
    @Override
    public boolean isFinished() {
        return finished;
    }

    /* (non-Javadoc)
     * @see org.mt4j.sceneManagement.transition.ITransition#setup(
     * org.mt4j.sceneManagement.Iscene, org.mt4j.sceneManagement.Iscene).
     */
    @Override
    public void setup(Iscene lastScenee, Iscene nextScenee) {
        this.lastScene = lastScenee;
        this.nextScene = nextScenee;
        finished = false;
        
        //Disable the scene's global input processors. We will be redirecting the input
        //from the current scene to the window scene
        app.getInputManager().disableGlobalInputProcessors(lastScene);
        app.getInputManager().disableGlobalInputProcessors(nextScene);
        
        app.invokeLater(new Runnable() {
            @Override
            public void run() {
                lastSceneRectangle = new MTRectangle(app, 0, 0, app.width, app.height);
                lastSceneRectangle.setTexture(app.g.get());
                lastSceneRectangle.setStrokeColor(MTColor.BLACK);

                nextSceneRectangle = new MTRectangle(app, 0, 0, app.width, app.height);
                nextScene.drawAndUpdate(app.g, 0);
                nextSceneRectangle.setTexture(app.g.get());
                nextSceneRectangle.setStrokeColor(MTColor.BLACK);

                getCanvas().addChild(lastSceneRectangle);
                getCanvas().addChild(nextSceneRectangle);
                
                if (slideUpDown) {
                    nextSceneRectangle.translateGlobal(new Vector3D(0, app.height, 0));
                } else {
                    nextSceneRectangle.translateGlobal(new Vector3D(0, -app.height, 0));
                }
                
                nextSceneRectangle.setVisible(true);
                
                anim.start();
            }
        });
    }
    
    
    @Override
    public void onLeave() {
        finished = true;
        this.lastScene = null;
        this.nextScene = null;
        
        lastSceneRectangle.destroy();
        nextSceneRectangle.destroy();
    }
    

}
