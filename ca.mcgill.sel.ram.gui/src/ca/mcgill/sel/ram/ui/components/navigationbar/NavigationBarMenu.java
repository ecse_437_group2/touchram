package ca.mcgill.sel.ram.ui.components.navigationbar;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import ca.mcgill.sel.ram.ui.components.RamListComponent.Filter;
import ca.mcgill.sel.ram.ui.components.RamRoundedRectangleComponent;
import ca.mcgill.sel.ram.ui.components.listeners.RamListListener;
import ca.mcgill.sel.ram.ui.layouts.VerticalLayout;
import ca.mcgill.sel.ram.ui.utils.Colors;

/**
 * A menu containing a list of {@link NavigationBarMenuElement} and showing elements which can be tapped. It represents
 * a menu linked to a {@link NavigationBarSection} which allows to navigate to next sections not shown.
 * 
 * @author g.Nicolas
 *
 */
public class NavigationBarMenu extends RamRoundedRectangleComponent {

    private List<NavigationBarMenuElement<?>> elements;

    /**
     * Namer which allows to initialize the {@link NavigationBarMenu} with {@link NavigationBarMenuElement}.
     * 
     * @author g.Nicolas
     *
     * @param <T> - type of element related to the {@link NavigationBarSection} related to the menu.
     */
    public interface NavigatonBarNamer<T> {

        /**
         * Initialize the {@link NavigationBarMenu} with {@link NavigationBarMenuElement}.
         * 
         * @param menu - the menu to initialize related to {@link NavigationBarSection}
         * @param element - element related to {@link NavigationBarSection}
         */
        void initMenu(NavigationBarMenu menu, T element);

    }
    
    /**
     * Namer allowing to initialize two different menus from a {@link NavigationBarMenuElement}.
     * 
     * @author andrea
     *
     * @param <T> - type of element the section will treat.
     */
    public interface NavigatonBarNamerRM<T> {
        
        /**
         * Initialize normal menu of a Section.
         * 
         * @param menu menus to be initialized.
         * @param element element related to specific section to whom the menus will be attached.
         */
        void initMenu(NavigationBarMenu menu, T element);
        
        /**
         * Initialize fan Out menu of a section.
         * 
         * @param menu to be initialized.
         * @param element related to specific fan out section search.
         */
        void initFanOutMenu(NavigationBarMenu menu, T element);
        
    }

    /**
     * Constructs a menu and created an empty {@link NavigationBarMenuElement} list.
     */
    NavigationBarMenu() {
        super(10);
        elements = new LinkedList<NavigationBarMenuElement<?>>();

        setLayout(new VerticalLayout());
        setNoStroke(false);
        setNoFill(false);
        setFillColor(Colors.NAVIGATION_BAR_MENU_COLOR);
    }

    /**
     * Adds a {@link NavigationBarMenuElement} in the menu. It's a general method to centralize the addMenuElement
     * behavior.
     * 
     * @param label - label of the {@link NavigationBarMenuElement}
     * @param data - {@link EObject} to construct a {@link ca.mcgill.sel.ram.ui.components.RamEMFListComponent} list.
     *            See
     *            {@link NavigationBarMenuElement}.
     * @param feature - {@link EStructuralFeature} to construct a
     *            {@link ca.mcgill.sel.ram.ui.components.RamEMFListComponent} list. See
     *            {@link NavigationBarMenuElement}.
     * @param type - {@link EClassifier} to construct a {@link ca.mcgill.sel.ram.ui.components.RamEMFListComponent}
     *            list. See
     *            {@link NavigationBarMenuElement}.
     * @param list - list for a {@link NavigationBarMenuElement}
     * @param namer - namer to initialize a sub-menu when clicking an element of the {@link NavigationBarMenuElement}
     *            list
     * @param listener - listener when clicking an element of the {@link NavigationBarMenuElement} list
     * @param filter - filter to select elements of the {@link NavigationBarMenuElement} list
     * @param <T> - type of elements in the {@link NavigationBarMenuElement} list.
     */
    // CHECKSTYLE:IGNORE ParameterNumberCheck: general method which centralize the addMenuElement behavior.
    private <T> void addMenuElement(String label, EObject data, EStructuralFeature feature, EClassifier type,
            List<T> list, NavigatonBarNamer<T> namer, RamListListener<T> listener, Filter<T> filter) {
        NavigationBarMenuElement<T> elem =
                new NavigationBarMenuElement<T>(this, label, data, feature, type, list, namer, listener, filter);
        elements.add(elem);
        addChild(elem);
    }

    /**
     * Adds a new {@link NavigationBarMenuElement} to the menu, with a label and an
     * {@link ca.mcgill.sel.ram.ui.components.RamEMFListComponent} which
     * contains sub-menus.
     * 
     * @param label - label heading the {@link NavigationBarMenuElement} list.
     * @param data - data for the {@link ca.mcgill.sel.ram.ui.components.RamEMFListComponent}
     * @param feature - feature for the {@link ca.mcgill.sel.ram.ui.components.RamEMFListComponent}
     * @param type - type for the {@link ca.mcgill.sel.ram.ui.components.RamEMFListComponent}
     * @param namer - {@link NavigatonBarNamer} to initialize sub-menu when taping on an element of the
     *            {@link ca.mcgill.sel.ram.ui.components.RamEMFListComponent} list.
     * @param filter - filter to select elements of the {@link NavigationBarMenuElement} list
     * @param <T> - type of elements in the {@link NavigationBarMenuElement} list.
     */
    public <T> void addMenuElement(String label, EObject data, EStructuralFeature feature, EClassifier type,
            NavigatonBarNamer<T> namer, Filter<T> filter) {
        this.addMenuElement(label, data, feature, type, null, namer, null, filter);
    }

    /**
     * Adds a new {@link NavigationBarMenuElement} to the menu, with a label and an
     * {@link ca.mcgill.sel.ram.ui.components.RamEMFListComponent}.
     * 
     * @param label - label heading the {@link NavigationBarMenuElement} list.
     * @param data - data for the {@link ca.mcgill.sel.ram.ui.components.RamEMFListComponent}
     * @param feature - feature for the {@link ca.mcgill.sel.ram.ui.components.RamEMFListComponent}
     * @param type - type for the {@link ca.mcgill.sel.ram.ui.components.RamEMFListComponent}
     * @param listener - listener called when clicking an element of the {@link NavigationBarMenuElement} list
     * @param filter - filter to select elements of the {@link NavigationBarMenuElement} list
     * @param <T> - type of elements in the {@link NavigationBarMenuElement} list.
     */
    public <T> void addMenuElement(String label, EObject data, EStructuralFeature feature, EClassifier type,
            RamListListener<T> listener, Filter<T> filter) {
        this.addMenuElement(label, data, feature, type, null, null, listener, filter);
    }

    /**
     * Adds a new {@link NavigationBarMenuElement} to the menu, without label and an
     * {@link ca.mcgill.sel.ram.ui.components.RamEMFListComponent} which
     * contains sub-menus.
     * 
     * @param data - data for the {@link ca.mcgill.sel.ram.ui.components.RamEMFListComponent}
     * @param feature - feature for the {@link ca.mcgill.sel.ram.ui.components.RamEMFListComponent}
     * @param type - type for the {@link ca.mcgill.sel.ram.ui.components.RamEMFListComponent}
     * @param namer - {@link NavigatonBarNamer} to initialize sub-menu when taping on an element of the
     *            {@link ca.mcgill.sel.ram.ui.components.RamEMFListComponent} list.
     * @param filter - filter to select elements of the {@link NavigationBarMenuElement} list
     * @param <T> - type of elements in the {@link NavigationBarMenuElement} list.
     */
    public <T> void addMenuElement(EObject data, EStructuralFeature feature, EClassifier type,
            NavigatonBarNamer<T> namer, Filter<T> filter) {
        this.addMenuElement(null, data, feature, type, null, namer, null, filter);
    }

    /**
     * Adds a new {@link NavigationBarMenuElement} to the menu, without label and an
     * {@link ca.mcgill.sel.ram.ui.components.RamEMFListComponent}.
     * 
     * @param data - data for the {@link ca.mcgill.sel.ram.ui.components.RamEMFListComponent}
     * @param feature - feature for the {@link ca.mcgill.sel.ram.ui.components.RamEMFListComponent}
     * @param type - type for the {@link ca.mcgill.sel.ram.ui.components.RamEMFListComponent}
     * @param listener - listener called when clicking an element of the {@link NavigationBarMenuElement} list
     * @param filter - filter to select elements of the {@link NavigationBarMenuElement} list
     * @param <T> - type of elements in the {@link NavigationBarMenuElement} list.
     */
    public <T> void addMenuElement(EObject data, EStructuralFeature feature, EClassifier type,
            RamListListener<T> listener, Filter<T> filter) {
        this.addMenuElement(null, data, feature, type, null, null, listener, filter);
    }

    /**
     * Adds a new {@link NavigationBarMenuElement} to the menu, with a label and an
     * {@link ca.mcgill.sel.ram.ui.components.RamListComponent}.
     * 
     * @param label - label heading the {@link NavigationBarMenuElement} list.
     * @param list - list of <T> elements for the {@link NavigationBarMenuElement}
     * @param listener - listener called when clicking an element of the {@link NavigationBarMenuElement} list
     * @param filter - filter to select elements of the {@link NavigationBarMenuElement} list
     * @param <T> - type of elements in the {@link NavigationBarMenuElement} list.
     */
    public <T> void addMenuElement(String label, List<T> list, RamListListener<T> listener, Filter<T> filter) {
        this.addMenuElement(label, null, null, null, list, null, listener, filter);
    }

    /**
     * Adds a new {@link NavigationBarMenuElement} to the menu, with a label and an
     * {@link ca.mcgill.sel.ram.ui.components.RamListComponent} which
     * contains sub-menus.
     * 
     * @param label - label heading the {@link NavigationBarMenuElement} list.
     * @param list - list of <T> elements for the {@link NavigationBarMenuElement}
     * @param namer - {@link NavigatonBarNamer} to initialize sub-menu when taping on an element of the list.
     * @param filter - filter to select elements of the {@link NavigationBarMenuElement} list
     * @param <T> - type of elements in the {@link NavigationBarMenuElement} list.
     */
    public <T> void addMenuElement(String label, List<T> list, NavigatonBarNamer<T> namer, Filter<T> filter) {
        this.addMenuElement(label, null, null, null, list, namer, null, filter);
    }

    /**
     * Adds a new {@link NavigationBarMenuElement} to the menu, without label and an
     * {@link ca.mcgill.sel.ram.ui.components.RamListComponent}.
     * 
     * @param list - list of <T> elements for the {@link NavigationBarMenuElement}
     * @param listener - listener called when clicking an element of the {@link NavigationBarMenuElement} list
     * @param filter - filter to select elements of the {@link NavigationBarMenuElement} list
     * @param <T> - type of elements in the {@link NavigationBarMenuElement} list.
     */
    public <T> void addMenuElement(List<T> list, RamListListener<T> listener, Filter<T> filter) {
        this.addMenuElement(null, null, null, null, list, null, listener, filter);
    }

    /**
     * Adds a new {@link NavigationBarMenuElement} to the menu, without label and an
     * {@link ca.mcgill.sel.ram.ui.components.RamListComponent} which
     * contains sub-menus.
     * 
     * @param list - list of <T> elements for the {@link NavigationBarMenuElement}
     * @param namer - {@link NavigatonBarNamer} to initialize sub-menu when taping on an element of the list.
     * @param filter - filter to select elements of the {@link NavigationBarMenuElement} list
     * @param <T> - type of elements in the {@link NavigationBarMenuElement} list.
     */
    public <T> void addMenuElement(List<T> list, NavigatonBarNamer<T> namer, Filter<T> filter) {
        this.addMenuElement(null, null, null, null, list, namer, null, filter);
    }

    /**
     * Returns true if the menu contains at least one element.
     * 
     * @return true if the menu contains at least one element. False otherwise.
     */
    public boolean containsElements() {
       
        return this.elements.size() > 0;
    }

    /**
     * Adds a new deletable element to the list.
     * 
     * @param label - label heading the {@link NavigationBarMenuElement} list.
     * @param list - list of <T> elements for the {@link NavigationBarMenuElement}
     * @param filter - filter to select elements of the {@link NavigationBarMenuElement} list
     * @param <T> - type of elements in the {@link NavigationBarMenuElement} list.
     * @param listener - listener attached to the list
     * @param deletable - boolean used to implement a deletable button for each new element 
     */
    public <T> void addMenuElement(String label, LinkedList<T> list, RamListListener<T> listener,
            Filter<T> filter, boolean deletable) {
        NavigationBarMenuElement<T> elem =
                new NavigationBarMenuElement<T>(
                        this, label, list, null, listener, filter, deletable);
        if (elem.containsElements()) {
            elements.add(elem);
            addChild(elem);
        } else {
            elem.destroy();
        }        
    }

    /**Method to add elements to the peculiar history menu.
     * 
     * @param history triggers the history menu chronologically sorted instead of alphabetically
     * @param list list of elements
     * @param listener listener triggering the scene changes
     * @param filter filter to remove particular elements
     * @param <T> type
     */
    public <T> void addMenuElement(boolean history, LinkedList<T> list,
            RamListListener<T> listener, Filter<T> filter) {
        NavigationBarMenuElement<T> elem =
                new NavigationBarMenuElement<T>(
                        history, this, list, null, listener, filter);
        if (elem.containsElements()) {
            elements.add(elem);
            addChild(elem);
        } else {
            elem.destroy();
        }        
        
    }
    
}
