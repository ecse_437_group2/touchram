package ca.mcgill.sel.ram.ui.components.navigationbar;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.mt4j.components.StateChange;
import org.mt4j.components.StateChangeEvent;
import org.mt4j.components.StateChangeListener;
import org.mt4j.components.TransformSpace;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.CORENamedElement;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.AspectMessageView;
import ca.mcgill.sel.ram.MessageView;
import ca.mcgill.sel.ram.Operation;
import ca.mcgill.sel.ram.controller.ControllerFactory;
import ca.mcgill.sel.ram.provider.util.RAMEditUtil;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.ConfirmPopup;
import ca.mcgill.sel.ram.ui.components.RamButton;
import ca.mcgill.sel.ram.ui.components.RamEMFListComponent;
import ca.mcgill.sel.ram.ui.components.RamExpendableComponent;
import ca.mcgill.sel.ram.ui.components.RamImageComponent;
import ca.mcgill.sel.ram.ui.components.RamListComponent;
import ca.mcgill.sel.ram.ui.components.RamListComponent.Filter;
import ca.mcgill.sel.ram.ui.components.RamListComponent.Namer;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamSpacerComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent;
import ca.mcgill.sel.ram.ui.components.listeners.RamListListener;
import ca.mcgill.sel.ram.ui.components.navigationbar.NavigationBarMenu.NavigatonBarNamer;
import ca.mcgill.sel.ram.ui.events.listeners.ActionListener;
import ca.mcgill.sel.ram.ui.layouts.HorizontalLayout;
import ca.mcgill.sel.ram.ui.layouts.VerticalLayout;
import ca.mcgill.sel.ram.ui.scenes.DisplayAspectScene;
import ca.mcgill.sel.ram.ui.scenes.DisplayConcernEditScene;
import ca.mcgill.sel.ram.ui.scenes.DisplayImpactModelEditScene;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.structural.StructuralDiagramView;

/**
 * An element in a {@link NavigationBarMenu}. This component contains a list of <T> elements.
 * The list can be headed by a label. When it's the case, the list will be contained in an
 * {@link RamExpendableComponent} if the list contains more than one element. Otherwise, it will show it in line.
 * When an element of the list is tapped it can either shows a sub-menu if a {@link NavigatonBarNamer} is given,
 * otherwise it will do the actions of the given listener.
 *
 * @param <T> - type of elements in the list.
 * @author Andrea
 */
public class NavigationBarMenuElement<T> extends RamRectangleComponent implements RamListListener<T> {

    private RamListComponent<T> list;
    private NavigatonBarNamer<T> namer;
    private NavigationBarMenu menu;

    /**
     * Constructs an element in a {@link NavigationBarMenu} in function of the parameters given. It's a default
     * constructor. "menu" parameter must not be null. Then if you want a {@link RamEMFListComponent} you have to give
     * "data" and "feature", otherwise give "list" parameter. If you want a sub-menu you have to give "namer" otherwise
     * "listener" parameter. "label" and "type" and "filter" are optional.
     * 
     * @param menu - menu containing the element.
     * @param label - label heading the list (Optional)
     * @param data - {@link EObject} for {@link RamEMFListComponent}
     * @param feature - {@link EStructuralFeature} for {@link RamEMFListComponent}
     * @param type - {@link EClassifier} for {@link RamEMFListComponent} (Optional)
     * @param list - list of <T> elements
     * @param namer - {@link NavigatonBarNamer} to initialize sub-menu when item is tapped.
     * @param listener - listener called when item is tapped.
     * @param filter - filter to remove elements in the list (Optional)
     */
    // CHECKSTYLE:IGNORE ParameterNumberCheck: general constructor which is only called by the addMenuElement methods
    NavigationBarMenuElement(NavigationBarMenu menu, String label, EObject data, EStructuralFeature feature,
            EClassifier type, List<T> list, NavigatonBarNamer<T> namer, RamListListener<T> listener, Filter<T> filter) {
        super();
        this.menu = menu;
        
        if (list != null) {
            this.list = new RamListComponent<T>(list, false);
        } else {
            this.list = new RamEMFListComponent<T>(data, feature, false);
        }
        this.list.setFilter(filter);
        this.list.setNoFill(true);
        this.list.setNoStroke(true);
        this.list.setNamer(new Namer<T>() {
            @Override
            public RamRectangleComponent getDisplayComponent(T element) {
                return customizeNamer(element);
            }
           
            @Override
            public String getSortingName(T element) {
                return sortElementsForMenus(element);   
            }
            @Override
            public String getSearchingName(T element) {
                return null;
            }
        });
        this.namer = namer;

        if (listener != null) {
            this.list.registerListener(listener);
        } else {
            this.list.registerListener(this);
        }

        if (label != null) {
            RamTextComponent labelComponent = new RamTextComponent(label + ": ");
            
            if (this.list.getElementMap().size() > 1) {
                initElementExpendable(labelComponent);
            } else {
                initElementOneLine(labelComponent);
            }
        } else {
            initElementSimple();
        }
    }
    
    /**Namer used to represent a sub menu with deletable elements.
     * 
     * @param menu menu where to insert the elements
     * @param label label for submenu
     * @param list list of elements
     * @param namer namer for submenus
     * @param listener listener to handle taps on elements
     * @param filter filter to remove particular elements
     * @param deletable triggers the deletion button
     */
    NavigationBarMenuElement(NavigationBarMenu menu, String label, List<T> list, 
            NavigatonBarNamer<T> namer, RamListListener<T> listener, Filter<T> filter, 
            boolean deletable) {
        super();
        this.menu = menu;
        
        if (list != null) {
            this.list = new RamListComponent<T>(list, false);
        } 
        this.list.setFilter(filter);
        this.list.setNoFill(true);
        this.list.setNoStroke(true);
        this.list.setNamer(new Namer<T>() {
            @Override
            public RamRectangleComponent getDisplayComponent(final T element) {
                
                return customizeDeletableNamer(element);
                
            }
            
            @Override
            public String getSortingName(T element) {
                return sortElementsForMenus(element);

            }
            @Override
            public String getSearchingName(T element) {
                return null;
            }
        });
        this.namer = namer;
        if (listener != null) {
            this.list.registerListener(listener);
        } else {
            this.list.registerListener(this);
        }
        int nbElement = this.list.getElementMap().size();
        if (nbElement > 0) {
            if (label != null) {
                RamTextComponent labelComponent = new RamTextComponent(label + ": ");
                initElementExpendable(labelComponent);   
            } else {
                initElementSimple();
            }
        }
    }
    
    /**
     * Method used to populate the peculiar history menu, where the sorting order is NOT alphabetic but chronological.
     * @param history triggers the creation of the history menu
     * @param menu menu itself
     * @param list list of elements found 
     * @param namer namer for eventual submenus
     * @param listener listener triggering the scene change
     * @param filter filter to potentially remove elements
     */
    public NavigationBarMenuElement(boolean history, NavigationBarMenu menu, final LinkedList<T> list,
            NavigatonBarNamer<T> namer, RamListListener<T> listener, Filter<T> filter) {
        
        super();
        this.menu = menu;
        if (list != null) {
            this.list = new RamListComponent<T>(list, false);
        } 
        this.list.setFilter(filter);
        this.list.setNoFill(true);
        this.list.setNoStroke(true);
        this.list.setNamer(new Namer<T>() {
            @Override
            public RamRectangleComponent getDisplayComponent(final T element) {
                return customizeNamer(element);
            }
            
            @Override
            public String getSortingName(T element) {
                return Integer.toString(list.indexOf(element));
            }
            @Override
            public String getSearchingName(T element) {
                return null;
            }
        });
        this.namer = namer;
        if (listener != null) {
            this.list.registerListener(listener);
        } else {
            this.list.registerListener(this);
        }
        int nbElement = this.list.getElementMap().size();
        if (nbElement > 0) {
            initElementSimple();
        }

    }

    /**Method to handle a deletable element in a menu.
     * 
     * @param element element to examine
     * @return RamRectangleComponent with the customized name
     */
    private RamRectangleComponent customizeDeletableNamer(final T element) {
        
        RamRectangleComponent displayRectangle;
        RamTextComponent aspectLabel; 
        RamSpacerComponent beforeSpacing = new RamSpacerComponent(4.0f, 0.0f);
        displayRectangle = new RamRectangleComponent(new HorizontalLayout());
        if (element instanceof Aspect) {
            RamSpacerComponent spacing = new RamSpacerComponent(13.0f, 0.0f);
            aspectLabel = new RamTextComponent(((Aspect) element).getName());
            RamImageComponent closeImage = new RamImageComponent(Icons.ICON_CLOSE, Colors.ICON_CLOSE_COLOR);
            closeImage.setMinimumSize(20, 20);
            closeImage.setMaximumSize(20, 20);
            closeImage.setBufferSize(Cardinal.WEST, 10);
            closeImage.setBufferSize(Cardinal.EAST, 10);
            if (RamApp.getApplication().getCurrentScene() instanceof DisplayConcernEditScene) {
                RamButton delete = new RamButton(closeImage);
                delete.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent event) {
                        
                        RamApp.getApplication();
                        final Aspect aspect = (Aspect) element;
                        ConfirmPopup deleteConfirmPopup = new ConfirmPopup(
                                Strings.popupDeleteElement(aspect.getName()),
                                ConfirmPopup.OptionType.YES_NO_CANCEL);
                        final DisplayConcernEditScene currentScene = (DisplayConcernEditScene) 
                                RamApp.getActiveConcernEditScene();
                        currentScene.displayPopup(deleteConfirmPopup);
                        deleteConfirmPopup.setListener(new ConfirmPopup.SelectionListener() {
                            @Override
                            public void optionSelected(int selectedOption) {
                                if (selectedOption == ConfirmPopup.YES_OPTION) {
                                    currentScene.getHandler().deleteModel(currentScene, aspect);
                                    NavigationBar.getInstance().closeMenu();
                                    return;
                                }
                            }
                        });
                    }
                });
                displayRectangle.addChild(beforeSpacing);
                displayRectangle.addChild(delete);
            }
            displayRectangle.addChild(spacing);
            displayRectangle.addChild(aspectLabel);
            return displayRectangle;
            
        } else if (element instanceof AspectMessageView) {
            final AspectMessageView messageView = (AspectMessageView) element;
            RamSpacerComponent spacing = new RamSpacerComponent(13.0f, 0.0f);
            aspectLabel = new TextView(messageView, CorePackage.Literals.CORE_NAMED_ELEMENT__NAME);
            RamImageComponent closeImage = new RamImageComponent(Icons.ICON_CLOSE, Colors.ICON_CLOSE_COLOR);
            closeImage.setMinimumSize(20, 20);
            closeImage.setMaximumSize(20, 20);
            closeImage.setBufferSize(Cardinal.WEST, 10);
            closeImage.setBufferSize(Cardinal.EAST, 10);
            if (RamApp.getActiveAspectScene().getCurrentView() instanceof StructuralDiagramView) {
                RamButton delete = new RamButton(closeImage);
                delete.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent event) {
                        ControllerFactory.INSTANCE.getAspectController().removeAspectMessageView(messageView);
                        NavigationBar.getInstance().closeMenu();
                    }
                });
                
                displayRectangle.addChild(beforeSpacing);
                displayRectangle.addChild(delete);
            }
            displayRectangle.addChild(spacing);
            displayRectangle.addChild(aspectLabel);
            return displayRectangle;                    
        }
        
        return beforeSpacing;
    }
    
    /**Returns according to the type of element that has been treated the relative TextComponent for the menu element.
     * 
     * @param element element to examine
     * @return the RamTextComponent with the corresponding element name customized accordingly
     */
    private RamRectangleComponent customizeNamer(T element) {
        String label = sortElementsForMenus(element);
        
        if (element instanceof DisplayAspectScene) {
            label = String.format("Aspect: %s", label);
        } else if (element instanceof DisplayConcernEditScene) {
            label = String.format("Concern: %s", label);
        } else if (element instanceof DisplayImpactModelEditScene) {
            label = String.format("Goal: %s", label);
        }
        
        return new RamTextComponent(label);
    }

    /**
     * Sorts the elements appearing in every menu of a section in alphabetical order.
     * @param element to sort
     * @return String of the element name
     */
    protected String sortElementsForMenus(T element) {
        String result = null;
        
        if (element instanceof Operation) {
            Operation operation = (Operation) element;
            AdapterFactory adapterFactory = EMFEditUtil.getAdapterFactory(operation);
            
            result = EMFEditUtil.stripTypeName(operation, 
                    RAMEditUtil.getOperationSignature(adapterFactory, operation, false, true));
        } else if (element instanceof MessageView) {
            MessageView messageView = (MessageView) element;
            AdapterFactory adapterFactory = EMFEditUtil.getAdapterFactory(messageView);
            Operation operation = messageView.getSpecifies();
            
            result = EMFEditUtil.stripTypeName(operation, 
                    RAMEditUtil.getOperationSignature(adapterFactory, operation, true, false));
        } else if (element instanceof COREModelComposition) {
            result = ((COREModelComposition) element).getSource().getName();
        } else if (element instanceof DisplayAspectScene) {
            result = ((DisplayAspectScene) element).getAspect().getName();
        } else if (element instanceof DisplayConcernEditScene) {
            result = ((DisplayConcernEditScene) element).getConcern().getName();
        } else if (element instanceof DisplayImpactModelEditScene) {
            result = ((DisplayImpactModelEditScene) element).getGoal().getName();
        } else if (element instanceof CORENamedElement) {
            result = ((CORENamedElement) element).getName();
        } else {
            result = element.toString();
        }
        
        return result;
    }
    
    /**
     * Creates a new sub-menu related to the given element and call the sub-menu manager in {@link NavigationBar}.
     * 
     * @param element - the element related to the sub-menu.
     * @param elementView - view related to the given element.
     */
    public void createSubMenu(T element, RamRectangleComponent elementView) {
        NavigationBarMenu subMenu = new NavigationBarMenu();
        namer.initMenu(subMenu, element);
        
        if (subMenu.containsElements()) {
            Vector3D positionGlobal = elementView.getPosition(TransformSpace.GLOBAL);
            positionGlobal.y += elementView.getHeightXY(TransformSpace.GLOBAL) / 2;
            getParentOfType(NavigationBar.class).openSubMenu(menu, subMenu, positionGlobal);
        } else {
            subMenu.destroy();
        }
    }

    /**
     * Initialize the element to show in one line. Used when there is just one element in the list and a label.
     * 
     * @param label - label before the list
     */
    private void initElementOneLine(RamTextComponent label) {
        this.setLayout(new HorizontalLayout());
        this.addChild(label);
        this.addChild(this.list);
    }

    /**
     * Initialize the element to show just the list, whitout label.
     */
    private void initElementSimple() {
        this.setLayout(new VerticalLayout());
        this.addChild(this.list);
    }

    /**
     * Initialize the element to show an {@link RamExpendableComponent} which contains the list. So it must have a label
     * as header of the {@link RamExpendableComponent}.
     * 
     * @param label - header of the {@link RamExpendableComponent}
     */
    private void initElementExpendable(RamTextComponent label) {
        this.setLayout(new VerticalLayout());
        StateChangeListener listener = new InternalStateChangeListener();
        this.list.addStateChangeListener(StateChange.REMOVED_FROM_PARENT, listener);
        this.list.addStateChangeListener(StateChange.ADDED_TO_PARENT, listener);
        RamExpendableComponent expendable = new RamExpendableComponent(label, this.list);
        expendable.getFirstLine().setNoFill(true);
        expendable.getFirstLine().setNoStroke(true);
        expendable.setNoFill(true);
        expendable.setNoStroke(true);
        this.addChild(expendable);
    }

    @Override
    public void elementSelected(RamListComponent<T> listComponent, T element) {
        createSubMenu(element, listComponent.getDisplayComponent(element));
    }

    @Override
    public void elementDoubleClicked(RamListComponent<T> listComponent, T element) {
    }

    @Override
    public void elementHeld(RamListComponent<T> listComponent, T element) {
    }

    /**
     * Returns true if the list contains at least one element.
     * 
     * @return true if the list contains at least one element. False otherwise.
     */
    public boolean containsElements() {
        return this.list.size() > 0;
    }

    /**
     * Stage change listener which close the sub-menu linked to the element when the list is removed of its parent. Used
     * in the case of expandable components.
     * 
     * @author g.Nicolas
     *
     */
    private class InternalStateChangeListener implements StateChangeListener {
        @Override
        public void stateChanged(StateChangeEvent evt) {
            getParentOfType(NavigationBar.class).closeSubMenu(menu);
        }
    }

}
