package ca.mcgill.sel.ram.ui.scenes;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.mt4j.components.visibleComponents.shapes.MTRectangle;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.COREReuse;
import ca.mcgill.sel.core.util.COREModelUtil;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.scenes.handler.IRamAbstractSceneHandler;
import ca.mcgill.sel.ram.ui.views.feature.FeatureDiagramView;
import ca.mcgill.sel.ram.ui.views.feature.FeatureView;
import ca.mcgill.sel.ram.ui.views.feature.helpers.Constraint;
import ca.mcgill.sel.ram.ui.views.feature.helpers.Constraint.ConstraintType;
import ca.mcgill.sel.ram.ui.views.feature.helpers.SelectionFeature;

/**
 * ConcernAbstractScene is used as an abstract class for rest of the concern scenes to extend it from.
 *
 * @author Nishanth.
 * @param <T> the class for the feature diagram type
 * @param <E> generic type the handler
 */
public abstract class AbstractConcernScene<T extends FeatureDiagramView<?>, E extends IRamAbstractSceneHandler>
        extends RamAbstractScene<E> {

    /**
     * The root Feature of the Feature Model.
     */
    protected COREFeature root;

    /**
     * The concern representing the Feature Model.
     */
    protected COREConcern concern;

    /**
     * The file path of the concern.
     */
    protected File filePath;

    /**
     * View for the feature model.
     */
    protected T featureDiagramView;

    /**
     * View for the feature model.
     */
    protected MTRectangle concernRectangle;
    
    /**
     * Default Constructor which passes the RAMApplication and name of the scene.
     *
     * @param application - The RAM Application
     * @param name - The scene name passed by the user
     * @param defaultActions - Whether we want the default actions or not.
     */
    public AbstractConcernScene(RamApp application, String name, boolean defaultActions) {
        super(application, name, defaultActions);
    }

    /**
     * Method used to initialize and display the whole scene.
     */
    protected abstract void build();

    /**
     * Method used to redraw only the feature diagram after model changes for example.
     *
     * @param repopulate - Whether we have to update the tree because a new element has been added or just update
     *            the position of the features and/or their links.
     */
    public abstract void redrawFeatureDiagram(boolean repopulate);

    /**
     * Getter of the concern.
     *
     * @return concern
     */
    public COREConcern getConcern() {
        return concern;
    }
    
    /**
     * Method is used to get the corresponding Feature View of the CORE Feature.
     * 
     * @param coreFeature - The CORE Feature for which the FeatureView is to be fetched.
     * @return FeatureView
     */
    public FeatureView getFeatureView(COREFeature coreFeature) {
        return featureDiagramView.getFeatureView(coreFeature);
    }

    /**
     * Function called to unload all the resources. Resource Manager takes up resources which have to be unloaded after
     * use.
     */
    public void unLoadAllResources() {
        Set<Aspect> list = new HashSet<Aspect>();

        list = featureDiagramView.collectRealizedAspects();

        for (Aspect aspect : list) {
            if (aspect.eResource() != null) {
                COREModelUtil.unloadEObject(aspect);
            }
        }
        COREModelUtil.unloadEObject(concern);

    }

    /**
     * Get children of the given feature that are part of the given list.
     *
     * @param featurePassed - the feature to check
     * @param list - the list to check against
     * @return intersection between given list and children of passed feature
     */
    protected Set<COREFeature> getContainedChildren(COREFeature featurePassed, List<COREFeature> list) {

        Set<COREFeature> collectedChildren = new HashSet<COREFeature>();

        if (list.contains(featurePassed)) {
            collectedChildren.add(featurePassed);
        }

        for (COREFeature child : featurePassed.getChildren()) {
            if (list.contains(child)) {
                collectedChildren.add(child);
            }
            collectedChildren.addAll(getContainedChildren(child, list));
        }

        return collectedChildren;
    }

    /**
     * Get the FeatureView closest to the position in the diagram, if there is one.
     *
     * @param position - The position to check
     * @return the closest {@link FeatureView} or null if there is no close enough feature
     */
    public FeatureView liesAround(Vector3D position) {
        return featureDiagramView.liesAround(position);
    }

    /**
     * Get the FeatureView containing the given position in the diagram, if there is one.
     *
     * @param position - The position to check
     * @return the closest {@link FeatureView} or null if there is no feature at this position
     */
    public FeatureView liesInside(Vector3D position) {
        return featureDiagramView.liesInside(position);
    }

    /**
     * Get all the {@link FeatureView} of the diagram.
     *
     * @param reuses - Whether we want to get features from reuses as well or not
     * @return the set of {@link FeatureView}
     */
    public Set<FeatureView> collectFeatureViews(boolean reuses) {
        return featureDiagramView.collectFeatureViews(reuses);
    }

    /**
     * Get all constraints for the current container scene.
     *
     * @param reuses - Whether to display constraints coming from reuses.
     * @return - The list of constraint for the feature and its children.
     */
    protected List<Constraint> getConstraints(boolean reuses) {
        return getConstraints(featureDiagramView.getRootFeature().getSelectionFeature(), reuses);
    }

    /**
     * Add recursively the constraints for the given feature and its children to the list.
     * Only add constraints for features that are part of the given set of featureViews.
     * Does not add constraints between features if the constraint will are always satisfied.
     * 
     * @param selectionFeature - The feature to add constraints for.
     * @param reuses - Whether to display constraints coming from reuses.
     * @return - The list of constraint for the feature and its children.
     */
    private List<Constraint> getConstraints(SelectionFeature selectionFeature, boolean reuses) {
        COREFeature coreFeature = selectionFeature.getCoreFeature();
        COREReuse reuse = selectionFeature.getCoreReuse();

        Set<SelectionFeature> features = selectionFeature.getFeaturesSameReuse();

        List<Constraint> list = new ArrayList<Constraint>();
        // Check constraints on the set of features
        for (SelectionFeature feature : features) {
            // Don't check for constraints if same features or if they don't come from the same reuse
            if (selectionFeature.equals(feature) || reuse != feature.getCoreReuse()) {
                continue;
            }
            // Check requires constraint. No need to add requires constraints if features are always selected
            if (coreFeature.getRequires().contains(feature.getCoreFeature())
                    && (!selectionFeature.isSelected() || !feature.isSelected())) {
                Constraint constraint = new Constraint(selectionFeature, feature, ConstraintType.REQUIRES, reuse);
                list.add(constraint);
            } else if (coreFeature.getExcludes().contains(feature.getCoreFeature())) {
                Constraint constraint = new Constraint(selectionFeature, feature, ConstraintType.EXCLUDES, reuse);
                list.add(constraint);
            }
        }

        // Add constraints recursively for children
        for (SelectionFeature child : selectionFeature.getChildrenFeatures()) {
            if (child.getCoreReuse() == reuse || reuses) {
                list.addAll(getConstraints(child, reuses));
            }
        }

        return list;
    }

    /**
     * Check whether a feature is part of the collapsed features of the scene.
     *
     * @param feature - The {@link FeatureView} to check.
     * @return true if the feature is collapsed.
     */
    public boolean isFeatureCollapsed(FeatureView feature) {
        return featureDiagramView.getCollapsedFeatures().contains(feature);
    }


}
