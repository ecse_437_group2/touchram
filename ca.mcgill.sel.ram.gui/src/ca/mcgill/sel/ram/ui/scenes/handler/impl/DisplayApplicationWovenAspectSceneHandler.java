package ca.mcgill.sel.ram.ui.scenes.handler.impl;

import org.mt4j.sceneManagement.Iscene;

import ca.mcgill.sel.core.util.COREModelUtil;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.scenes.DisplayAspectScene;

/**
 * The handler for a {@link DisplayAspectScene} of the woven model of a built application.
 * Switch back to the first scene and not the previousScene.
 *
 * @author CCamillieri
 */
public class DisplayApplicationWovenAspectSceneHandler extends DisplayAspectSceneHandler {
    
    @Override
    public void switchToConcern(final DisplayAspectScene scene) {
        if (scene.getAspect().eResource() == null) {
            showCloseConfirmPopup(scene);
        } else {
            super.switchToConcern(scene);
        }
    }

    @Override
    protected void doSwitchToPreviousScene(DisplayAspectScene scene) {
        // Unload WovenAspect of this aspect when we leave
        COREModelUtil.unloadExternalResources(scene.getAspect());

        // destroy and remove the select scene
        Iscene previousScene = scene.getPreviousScene();
        previousScene.destroy();
        RamApp.getApplication().removeScene(previousScene);
        
        // Here we want to go back to the main TouchCORE menu
        RamApp.getApplication().closeAspectScene(scene);
    }
    
}
