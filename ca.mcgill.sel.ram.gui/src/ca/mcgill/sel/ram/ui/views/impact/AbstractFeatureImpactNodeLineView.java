package ca.mcgill.sel.ram.ui.views.impact;

import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.layouts.HorizontalLayout;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.HandlerFactory;
import ca.mcgill.sel.ram.ui.views.handler.ITextViewHandler;

/**
 * This view describes one of the differents lines of a FeatureImpactNodeView.
 *
 * @author Romain
 *
 */
public abstract class AbstractFeatureImpactNodeLineView extends RamRectangleComponent {

    /**
     * The TextView that represent the weight of this line.
     */
    protected TextView weight;

    /**
     * The handler for the weight TextView.
     */
    protected ITextViewHandler handler;

    /**
     * Create a new {@link AbstractFeatureImpactNodeLineView}. By default, his layout is a {@link HorizontalLayout} has
     * fill and stroke.
     */
    public AbstractFeatureImpactNodeLineView() {
        setNoFill(false);
        setFillColor(Colors.DEFAULT_ELEMENT_FILL_COLOR);
        setNoStroke(false);
        setStrokeColor(Colors.DEFAULT_ELEMENT_STROKE_COLOR);

        setLayout(new HorizontalLayout());
    }

    /**
     * Remove {@link TextView} for the name.
     */
    public abstract void removeNameTextView();

    /**
     * Remove {@link TextView} for the weight.
     */
    public void removeWeightTextView() {
        if (weight != null && this.containsChild(weight)) {
            this.removeChild(weight);
            weight = null;
        }
    }

    /**
     * Retrieve {@link TextView} for the name.
     *
     * @return the first {@link TextView}.
     */
    public abstract TextView getNameTextView();

    /**
     * Retrieve {@link TextView} for the weight.
     *
     * @return the last {@link TextView}.
     */
    public TextView getWeightTextView() {
        return this.weight;
    }

    /**
     * Return true if this line contains a TextView for the weight.
     *
     * @return true if this line contains a TextView for the weight.
     */
    public boolean hasWeightTextView() {
        return this.weight != null;
    }

    /**
     * Set the handler for the weight TextView.
     */
    public void setHandler() {
        this.handler = HandlerFactory.INSTANCE.getTextViewHandler();
        if (hasWeightTextView()) {
            this.getWeightTextView().unregisterAllInputProcessors();
            this.getWeightTextView().registerTapProcessor(this.handler);
        }
    }
}
