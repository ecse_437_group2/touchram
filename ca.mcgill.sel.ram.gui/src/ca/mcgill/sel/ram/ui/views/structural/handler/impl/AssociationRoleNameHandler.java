package ca.mcgill.sel.ram.ui.views.structural.handler.impl;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;

import ca.mcgill.sel.ram.AssociationEnd;
import ca.mcgill.sel.ram.controller.AssociationController;
import ca.mcgill.sel.ram.controller.ControllerFactory;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamSelectorComponent;
import ca.mcgill.sel.ram.ui.components.listeners.AbstractDefaultRamSelectorListener;
import ca.mcgill.sel.ram.ui.utils.MetamodelRegex;
import ca.mcgill.sel.ram.ui.views.OptionSelectorView;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.impl.ValidatingTextViewHandler;
import ca.mcgill.sel.ram.ui.views.structural.handler.IAssociationRoleNameHandler;

/**
 * The default handler for a {@link TextView} representing the role name of an {@link AssociationEnd}.
 * The name gets validated in order to only allow valid names.
 *
 * @author lmartellotto
 *
 */
public class AssociationRoleNameHandler extends ValidatingTextViewHandler implements IAssociationRoleNameHandler {

    /**
     * Creates a new {@link AssociationRoleNameHandler}.
     */
    public AssociationRoleNameHandler() {
        super(MetamodelRegex.REGEX_TYPE_NAME);
    }

    /**
     * Features that can be used on the role name of an {@link AssociationEnd}.
     */
    private enum AssociationRoleFeatures {
        STATIC;
    }

    @Override
    public boolean processTapAndHoldEvent(TapAndHoldEvent tapAndHoldEvent) {
        if (tapAndHoldEvent.isHoldComplete()) {
            TextView target = (TextView) tapAndHoldEvent.getTarget();

            final AssociationEnd attribute = (AssociationEnd) target.getData();

            OptionSelectorView<AssociationRoleFeatures> selector =
                    new OptionSelectorView<AssociationRoleNameHandler.AssociationRoleFeatures>(
                            AssociationRoleFeatures.values());
            RamApp.getActiveScene().addComponent(selector, tapAndHoldEvent.getLocationOnScreen());

            selector.registerListener(
                    new AbstractDefaultRamSelectorListener<AssociationRoleNameHandler.AssociationRoleFeatures>() {
                        @Override
                        public void elementSelected(RamSelectorComponent<AssociationRoleFeatures> selector,
                                AssociationRoleFeatures element) {
                            AssociationController associationController = ControllerFactory.INSTANCE
                                    .getAssociationController();

                            switch (element) {
                                case STATIC:
                                    associationController.switchStatic(attribute);
                                    break;
                            }
                        }

                    });
        }

        return true;
    }

    @Override
    protected void setValue(EObject data, EStructuralFeature feature, Object value) {
        final AssociationEnd associationEnd = (AssociationEnd) data;
        AssociationController associationController = ControllerFactory.INSTANCE.getAssociationController();
        associationController.setRoleName(RamApp.getActiveAspectScene().getAspect(), associationEnd, value.toString());
    }

}
