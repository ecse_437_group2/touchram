package ca.mcgill.sel.ram.ui.views.feature.helpers;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.COREFeatureRelationshipType;
import ca.mcgill.sel.core.COREReuse;

/**
 * This helper class is used to encapsulate data such surrounding a {@link COREFeature} such as:
 * - is it coming from a specific reuse
 * - is it the root feature of this reuse
 * - the status of the feature during selection (RE_EXPOSED, SELECTED, ...)
 * - parent and children features as displayed in the view (ie, if Feature A reuses a concern,
 * whose root feature is called B, B will have A as parent, and A will have B as child.)
 * This hierarchy allows to compute easily status of the features and configurations from the selection.
 * 
 * @author CCamillieri
 */
public class SelectionFeature {

    /**
     * Enumeration containing all possible status for features during selection.
     * 
     * @author CCamillieri
     */
    public enum FeatureSelectionStatus {
        /** The Feature has been re-exposed. */
        RE_EXPOSED,
        /** The Feature has been auto-selected. */
        AUTO_SELECTED,
        /** The Feature has been selected manually. */
        SELECTED,
        /** The Feature is not selected, nor re-exposed. */
        NOT_SELECTED,
        /** There is an issue with the Feature selection, the feature was auto-selected. */
        WARNING_AUTO_SELECTED,
        /** There is an issue with the Feature selection, the feature was manually-selected. */
        WARNING_SELECTED,
        /** There is an issue with the Feature selection because the feature was unselected. */
        WARNING_NOT_SELECTED
    }

    private COREFeature feature;
    private COREReuse reuse;
    private boolean isReuse;
    private FeatureSelectionStatus status;
    private SelectionFeature parent;
    private List<SelectionFeature> childrenFeatures;
    private COREFeatureRelationshipType parentRelationship = COREFeatureRelationshipType.NONE;
    private COREFeatureRelationshipType childrenRelationship = COREFeatureRelationshipType.NONE;

    /**
     * Constructor for SelectionFeature.
     * 
     * @param feature - The {@link COREFeature} represented by this object.
     * @param reuse - The reuse this features comes from, can be null.
     * @param parent - The parent {@link SelectionFeature}
     */
    public SelectionFeature(COREFeature feature, COREReuse reuse, SelectionFeature parent) {
        this.feature = feature;
        this.reuse = reuse;
        this.isReuse = reuse != null && feature.getParent() == null;
        this.status = FeatureSelectionStatus.NOT_SELECTED;
        this.childrenFeatures = new LinkedList<SelectionFeature>();
        // add to parent
        this.parent = parent;
        if (parent != null) {
            parent.addChildFeature(this);
        }
    }

    /**
     * Getter for {@link COREFeature}.
     * 
     * @return the {@link COREFeature}
     */
    public COREFeature getCoreFeature() {
        return feature;
    }

    /**
     * Getter for {@link COREReuse}.
     * 
     * @return the {@link COREReuse}
     */
    public COREReuse getCoreReuse() {
        return reuse;
    }

    /**
     * Getter for {@link FeatureSelectionStatus}.
     * 
     * @return the {@link FeatureSelectionStatus}
     */
    public FeatureSelectionStatus getSelectionStatus() {
        return status;
    }

    /**
     * Setter for {@link FeatureSelectionStatus}.
     * 
     * @param status - the {@link FeatureSelectionStatus} to set
     */
    public void setStatus(FeatureSelectionStatus status) {
        this.status = status;
    }

    /**
     * Add a {@link SelectionFeature} as child of this one.
     * 
     * @param child - the {@link SelectionFeature} to add as a child
     */
    public void addChildFeature(SelectionFeature child) {
        childrenFeatures.add(child);
    }

    /**
     * Getter for the parent {@link SelectionFeature}.
     * 
     * @return the parent {@link SelectionFeature}
     */
    public SelectionFeature getParentFeature() {
        return parent;
    }

    /**
     * Setter for the parent {@link SelectionFeature}.
     * 
     * @param newParent - the parent {@link SelectionFeature}
     */
    public void setParentFeature(SelectionFeature newParent) {
        this.parent = newParent;
    }

    /**
     * Getter for list of children {@link SelectionFeature}.
     * 
     * @return children {@link SelectionFeature}
     */
    public List<SelectionFeature> getChildrenFeatures() {
        return childrenFeatures;
    }

    /**
     * Whether the feature is root of its reuse.
     * 
     * @return true if feature is root of a reuse.
     */
    public boolean isReuse() {
        return isReuse;
    }

    /**
     * Getter for the relationship to children {@link SelectionFeature}.
     * Should only be OR or XOR.
     * 
     * @return relationship to children {@link SelectionFeature}
     */
    public COREFeatureRelationshipType getChildrenRelationship() {
        return childrenRelationship;
    }

    /**
     * Setter for the relationship to children {@link SelectionFeature}.
     * Should only be OR or XOR.
     * 
     * @param type - relationship to children {@link SelectionFeature}
     */
    public void setChildrenRelationShip(COREFeatureRelationshipType type) {
        childrenRelationship = type;
    }

    /**
     * Getter for the relationship to parent {@link SelectionFeature}.
     * 
     * @return relationship to parent {@link SelectionFeature}
     */
    public COREFeatureRelationshipType getParentRelationship() {
        return parentRelationship;
    }

    /**
     * Setter for the relationship to parent {@link SelectionFeature}.
     * 
     * @param type - relationship to parent {@link SelectionFeature}
     */
    public void setParentRelationShip(COREFeatureRelationshipType type) {
        parentRelationship = type;
    }

    /**
     * Check if the feature has been selected.
     * 
     * @return true if the feature is in status "selected, auto-selected,
     *         warning_selected or warning_autoselected", false otherwise
     */
    public boolean isSelected() {
        return status == FeatureSelectionStatus.SELECTED
                || status == FeatureSelectionStatus.AUTO_SELECTED
                || status == FeatureSelectionStatus.WARNING_SELECTED
                || status == FeatureSelectionStatus.WARNING_AUTO_SELECTED;
    }

    /**
     * Look for a selection feature representing the given {@link COREFeature}.
     *
     * @param feature - The {@link COREFeature} to look for.
     * @param current - The {@link SelectionFeature} we are currently at.
     * @return the selectionFeature associated to it.
     */
    public static SelectionFeature findSelectionFeature(COREFeature feature, SelectionFeature current) {
        if (current.getCoreReuse() == null) {
            if (current.getCoreFeature() == feature) {
                return current;
            }
            for (SelectionFeature child : current.getChildrenFeatures()) {
                SelectionFeature found = findSelectionFeature(feature, child);
                if (found != null) {
                    return found;
                }
            }
        }
        return null;
    }

    /**
     * Get the root feature from the same model and reuse as this one.
     * 
     * @return root {@link SelectionFeature} (can be this).
     */
    public SelectionFeature getRootParent() {
        SelectionFeature root = this;
        while (root.getParentFeature() != null && root.getParentFeature().getCoreReuse() == reuse) {
            root = root.getParentFeature();
        }
        return root;
    }

    /**
     * Get {@link SelectionFeature}s of the same reuse in the feature's hierarchy, including the feature.
     *
     * @return list of root {@link SelectionFeature}s of reuses.
     */
    public Set<SelectionFeature> getFeaturesSameReuse() {
        return getRootParent().getDescendantsSameReuse();
    }

    /**
     * Get {@link SelectionFeature}s of the same reuse in the feature's descendants, including the feature.
     *
     * @return list of descendant {@link SelectionFeature}s from the same reuse.
     */
    private Set<SelectionFeature> getDescendantsSameReuse() {
        Set<SelectionFeature> reuses = new HashSet<SelectionFeature>();
        reuses.add(this);
        for (SelectionFeature child : getChildrenFeatures()) {
            if (child.getCoreReuse() == this.getCoreReuse()) {
                reuses.addAll(child.getDescendantsSameReuse());
            }
        }
        return reuses;
    }

    /**
     * Get root {@link SelectionFeature}s of all reuses in the feature's hierarchy.
     *
     * @return list of root {@link SelectionFeature}s of reuses.
     */
    public Set<SelectionFeature> getDescendantsReuseRoot() {
        Set<SelectionFeature> reuses = new HashSet<SelectionFeature>();
        for (SelectionFeature child : getChildrenFeatures()) {
            if (child.isReuse()) {
                reuses.add(child);
            } else {
                reuses.addAll(child.getDescendantsReuseRoot());
            }
        }
        return reuses;
    }

}
