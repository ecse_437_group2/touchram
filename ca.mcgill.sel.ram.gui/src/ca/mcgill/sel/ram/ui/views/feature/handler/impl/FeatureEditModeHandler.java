package ca.mcgill.sel.ram.ui.views.feature.handler.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.mt4j.components.MTComponent;
import org.mt4j.components.TransformSpace;
import org.mt4j.input.inputProcessors.MTGestureEvent;
import org.mt4j.input.inputProcessors.componentProcessors.dragProcessor.DragEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;
import org.mt4j.sceneManagement.transition.SlideTransition;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.COREFeatureModel;
import ca.mcgill.sel.core.COREFeatureRelationshipType;
import ca.mcgill.sel.core.COREModel;
import ca.mcgill.sel.core.COREReuse;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.controller.COREControllerFactory;
import ca.mcgill.sel.core.controller.FeatureController;
import ca.mcgill.sel.core.util.COREModelUtil;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamListComponent.Namer;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamSelectorComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent;
import ca.mcgill.sel.ram.ui.components.browser.AspectFileBrowser;
import ca.mcgill.sel.ram.ui.components.browser.interfaces.AspectFileBrowserListener;
import ca.mcgill.sel.ram.ui.components.listeners.AbstractDefaultRamSelectorListener;
import ca.mcgill.sel.ram.ui.components.listeners.RamSelectorListener;
import ca.mcgill.sel.ram.ui.events.DelayedDrag;
import ca.mcgill.sel.ram.ui.scenes.AbstractConcernScene;
import ca.mcgill.sel.ram.ui.scenes.DisplayConcernEditScene;
import ca.mcgill.sel.ram.ui.scenes.RamAbstractScene;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.GUIConstants;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.OptionSelectorView;
import ca.mcgill.sel.ram.ui.views.feature.FeatureView;
import ca.mcgill.sel.ram.ui.views.feature.handler.IFeatureHandler;
import ca.mcgill.sel.ram.ui.views.handler.BaseHandler;
import ca.mcgill.sel.ram.util.RAMModelUtil;

/**
 * Feature Handler, used to handle events on the Feature.
 *
 * @author Nishanth
 */
public class FeatureEditModeHandler extends BaseHandler implements IFeatureHandler {

    private DelayedDrag dragAction = new DelayedDrag(GUIConstants.DELAYED_DRAG_MIN_DRAG_DISTANCE);

    /**
     * Enum containing all the options to be shown when tap and hold is complete.
     */
    private enum CreateFeature {
        OPEN_REALIZING_DESIGN_MODEL, NEW_REALIZING_DESIGN_MODEL, DELETE_FEATURE, ADD_OPTIONAL, ADD_MANDATORY, ADD_XOR,
        ADD_OR, ASSOCIATE_REALIZING_DESIGN_MODEL, COLLAPSE, EXPAND
    }

    /**
     * Namer for the selector of models to show.
     */
    private class ModelSelectorNamer implements Namer<COREModel> {
        @Override
        public RamRectangleComponent getDisplayComponent(COREModel element) {
            RamTextComponent textComponent = new RamTextComponent(element.getName());
            textComponent.setNoFill(false);
            textComponent.setFillColor(Colors.DEFAULT_ELEMENT_FILL_COLOR);
            textComponent.setAutoMaximizes(true);
            textComponent.setNoStroke(false);
            textComponent.setStrokeColor(Colors.DEFAULT_ELEMENT_STROKE_COLOR);
            return textComponent;
        }

        @Override
        public String getSortingName(COREModel element) {
            return element.getName();
        }

        @Override
        public String getSearchingName(COREModel element) {
            return element.getName();
        }
    }

    /**
     * Listener for the selector of aspects to show.
     */
    private class ModelSelectorListener implements RamSelectorListener<COREModel> {
        @Override
        public void elementSelected(RamSelectorComponent<COREModel> selector, COREModel element) {
            showModel(element);
            selector.destroy();
        }

        @Override
        public void elementHeld(RamSelectorComponent<COREModel> selector, COREModel element) {
        }

        @Override
        public void elementDoubleClicked(RamSelectorComponent<COREModel> selector, COREModel element) {
        }

        @Override
        public void closeSelector(RamSelectorComponent<COREModel> selector) {
            selector.destroy();
        }
    }

    @Override
    public boolean processTapAndHoldEvent(TapAndHoldEvent tapAndHoldEvent) {
        // Get the current working concern scene.
        DisplayConcernEditScene concernScene = RamApp.getActiveConcernEditScene();

        if (tapAndHoldEvent.isHoldComplete() && concernScene != null) {
            FeatureView featureIcon = (FeatureView) tapAndHoldEvent.getTarget();
            showFeatureOptions(concernScene, featureIcon, tapAndHoldEvent.getLocationOnScreen());
        }
        return true;
    }

    /**
     * Shows the options for the feature.
     *
     * @param scene the current scene
     * @param view the view of the selected feature
     * @param location the location on the screen where the options should be shown
     */
    private void showFeatureOptions(final DisplayConcernEditScene scene,
            final FeatureView view, final Vector3D location) {

        OptionSelectorView<CreateFeature> selector = new OptionSelectorView<CreateFeature>(getActions(scene, view));

        scene.addComponent(selector, location);

        // CHECKSTYLE:IGNORE AnonInnerLength: They can be more than 30 lines
        selector.registerListener(new AbstractDefaultRamSelectorListener<CreateFeature>() {
            @Override
            public void elementSelected(RamSelectorComponent<CreateFeature> selector, CreateFeature element) {
                switch (element) {
                    case OPEN_REALIZING_DESIGN_MODEL:
                        handleShowModel(view, location);
                        break;
                    case ADD_OPTIONAL:
                        addChild(scene, view.getFeature(), -1, COREFeatureRelationshipType.OPTIONAL);
                        break;
                    case ADD_MANDATORY:
                        addChild(scene, view.getFeature(), -1, COREFeatureRelationshipType.MANDATORY);
                        break;
                    case ADD_XOR:
                        addChild(scene, view.getFeature(), -1, COREFeatureRelationshipType.XOR);
                        break;
                    case ADD_OR:
                        addChild(scene, view.getFeature(), -1, COREFeatureRelationshipType.OR);
                        break;
                    case DELETE_FEATURE:
                        view.getHandler().deleteFeature(scene, view);
                        break;
                    case NEW_REALIZING_DESIGN_MODEL:
                        handleNewDesignModel(scene.getConcern(), view.getFeature());
                        break;
                    case ASSOCIATE_REALIZING_DESIGN_MODEL:
                        loadBrowserAndAssociate(view);
                        break;
                    case COLLAPSE:
                    case EXPAND:
                        view.getHandler().hideFeature(scene, view);
                        break;
                }
            }

            
        });
    }

    /**
     * Get the possible actions on the given feature.
     * 
     * @param scene - The current scene
     * @param feature - The given {@link FeatureView}
     * @return the set of available actions
     */
    private static CreateFeature[] getActions(DisplayConcernEditScene scene, FeatureView feature) {
        EnumSet<CreateFeature> options = EnumSet.allOf(CreateFeature.class);
        // Remove collapse and expand options depending of the state.
        if (feature.getIsRoot()) {
            options.remove(CreateFeature.DELETE_FEATURE);
            options.remove(CreateFeature.COLLAPSE);
            options.remove(CreateFeature.EXPAND);
        } else if (!feature.isReuse() && !feature.isParentReuse()) {
            options.remove(CreateFeature.COLLAPSE);
            options.remove(CreateFeature.EXPAND);
        } else if (scene.isShowReuses() && feature.getChildrenFeatureViews().isEmpty()) {
            options.remove(CreateFeature.COLLAPSE);
            options.remove(CreateFeature.EXPAND);
        } else if (!scene.isShowReuses() && feature.getFeature().getChildren().isEmpty()) {
            options.remove(CreateFeature.COLLAPSE);
            options.remove(CreateFeature.EXPAND);
        } else if (!scene.isFeatureCollapsed(feature)) {
            options.remove(CreateFeature.EXPAND);
        } else {
            options.remove(CreateFeature.COLLAPSE);
        }
        
        // Remove 'open realizing model' if there is none.
        if (feature.getFeature().getRealizedBy().isEmpty()) {
            options.remove(CreateFeature.OPEN_REALIZING_DESIGN_MODEL);
        }
        
        return options.toArray(new CreateFeature[0]);
    }

    /**
     * Function called, when an external aspect is to be loaded and associated with a feature.
     *
     * @param view - The {@link FeatureView} we want to associate with the new aspect
     */
    private static void loadBrowserAndAssociate(final FeatureView view) {
        AspectFileBrowser.loadAspect(new AspectFileBrowserListener() {

            @Override
            public void aspectLoaded(Aspect aspect) {
                COREFeatureModel fM = (COREFeatureModel) view.getFeature().eContainer();
                COREConcern concern = (COREConcern) fM.eContainer();
                COREControllerFactory.INSTANCE.getFeatureController()
                        .associateModel(concern, view.getFeature(), aspect);
            }

            @Override
            public void aspectSaved(File file) {
            }
        });
    }

    @Override
    public boolean processTapEvent(final TapEvent tapEvent) {
        // unused
        return false;
    }

    @Override
    public boolean processDragEvent(DragEvent dragEvent) {

        DisplayConcernEditScene scene = RamApp.getActiveConcernEditScene();
        if (scene == null) {
            return true;
        }

        FeatureView featureIcon = (FeatureView) dragEvent.getTarget();

        switch (dragEvent.getId()) {
            case MTGestureEvent.GESTURE_STARTED:
            case MTGestureEvent.GESTURE_UPDATED:
                dragAction.processGestureEvent(dragEvent);
                for (MTComponent comp : featureIcon.getAllDragContainers()) {
                    dragEvent.setTarget(comp);
                    dragAction.processGestureEvent(dragEvent);
                }
                updateFeatureColors(scene.collectFeatureViews(false), featureIcon);
                break;
            case MTGestureEvent.GESTURE_ENDED:
                if (dragAction.wasDragPerformed()) {
                    setPositionUpdate(featureIcon, scene);
                }
                // Reset highlight
                for (FeatureView fv : scene.collectFeatureViews(false)) {
                    fv.highlight(false);
                }
                break;
        }
        return true;
    }

    /**
     * Display realizing model(s) for the given feature.
     * If there is more than one, opens a selector with models.
     * 
     * @param featureIcon - The {@link FeatureView} to display model for.
     * @param selectorLocation - Location to display the selector on screen.
     */
    private void handleShowModel(FeatureView featureIcon, Vector3D selectorLocation) {
        RamAbstractScene<?> scene = RamApp.getActiveScene();
        if (scene instanceof DisplayConcernEditScene) {
            EList<COREModel> realizingModels = featureIcon.getFeature().getRealizedBy();
            if (realizingModels.size() == 1) {
                showModel(realizingModels.get(0));
            } else if (realizingModels.size() > 1) {
                RamSelectorComponent<COREModel> selector;
                selector = new RamSelectorComponent<COREModel>(new ModelSelectorNamer());
                selector.setElements(featureIcon.getFeature().getRealizedBy());
                selector.registerListener(new ModelSelectorListener());
                // Display selector
                scene.addComponent(selector, selectorLocation);
            }
        }
    }
    
    /**
     * Handles the request to create a new design model and associate it to a feature.
     * 
     * @param concern the concern to add it to
     * @param feature the feature being realized by the design model
     */
    private static void handleNewDesignModel(COREConcern concern, COREFeature feature) {
        Aspect aspect = RAMModelUtil.createAspect(feature.getName(), concern);
        Aspect parentAspect = RAMModelUtil.getParentAspect(feature);
        
        FeatureController controller = COREControllerFactory.INSTANCE.getFeatureController();
        controller.addModelAndAssociate(concern, feature, aspect, parentAspect);
        
        showModel(aspect);
    }

    /**
     * Highlight the underlying {@link FeatureView} when dragging a feature.
     * 
     * @param set - The set of features to examine
     * @param feature - The dragged feature
     */
    private static void updateFeatureColors(Set<FeatureView> set, FeatureView feature) {
        // reset all highlights
        for (FeatureView fv : set) {
            fv.highlight(false);
        }
        // highlight underlying feature, if any
        FeatureView featureView = getUnderlyingFeature(set, feature);
        if (featureView != null) {
            featureView.highlight(true);
            feature.highlight(true);
        } else {
            // Check if we are to be moved
            FeatureView parentV = feature.getParentFeatureView();
            int newPosition = getFeaturePosition(feature);
            
            if (newPosition == -1) {
                return;
            }
            
            newPosition += parentV.getFeature().getReuses().size();
            int offset = feature.getCurrentPosition() - newPosition;
            int prevPosition;
            int nextPosition;
            // To the left
            if (offset > 0) {
                prevPosition = newPosition - 1;
                nextPosition = newPosition;
            } else {
                prevPosition = newPosition;
                nextPosition = newPosition + 1;
            }
            if (prevPosition >= 0 && prevPosition < parentV.getChildrenFeatureViews().size()) {
                FeatureView prevChild = parentV.getChildrenFeatureViews().get(prevPosition);
                if (!prevChild.isReuse()) {
                    prevChild.highlight(true);
                    parentV.highlight(true);
                }
            }
            if (nextPosition >= 0 && nextPosition < parentV.getChildrenFeatureViews().size()) {
                FeatureView nextChild = parentV.getChildrenFeatureViews().get(nextPosition);
                nextChild.highlight(true);
                parentV.highlight(true);
            }
        }
    }

    /**
     * Function used to translate the Feature when dragged.
     * In addition to translating the Feature,
     * all its children and the line associated with its parent has to be translated.
     * 
     * @param position - The position after translation.
     * @param featureIcon - The Feature on which the drag gesture was called.
     */
    public void setNewTranslation(Vector3D position, FeatureView featureIcon) {
        // drag also the children
        for (MTComponent each : featureIcon.getAllDragContainers()) {
            each.translate(position);
        }
    }

    @Override
    public void setPositionUpdate(FeatureView featureIcon, AbstractConcernScene<?, ?> scene) {
        /*
         * First checks if a feature had to be added as a child of another feature
         * This is horizontal movement, where intersection adds it as a child.
         */
        FeatureView target = getUnderlyingFeature(scene.collectFeatureViews(false), featureIcon);
        if (target != null && target != featureIcon && !target.equals(featureIcon.getParentFeatureView())) {

            if (target.getChildrenRelationship() == COREFeatureRelationshipType.XOR
                    || target.getChildrenRelationship() == COREFeatureRelationshipType.OR) {
                COREControllerFactory.INSTANCE.getFeatureController().addExistingFeature(scene.getConcern(),
                        featureIcon.getFeature(), target.getFeature(), target.getChildrenRelationship());
            } else {
                COREFeatureRelationshipType rel = featureIcon.getFeature().getParentRelationship();
                if (rel == COREFeatureRelationshipType.MANDATORY || rel == COREFeatureRelationshipType.OPTIONAL) {
                    COREControllerFactory.INSTANCE.getFeatureController().addExistingFeature(scene.getConcern(),
                            featureIcon.getFeature(), target.getFeature(), rel);
                } else {
                    COREControllerFactory.INSTANCE.getFeatureController()
                            .addExistingFeature(scene.getConcern(),
                                    featureIcon.getFeature(), target.getFeature(),
                                    COREFeatureRelationshipType.OPTIONAL);
                }
            }
            return;
        }

        /* Check if we want to change the position of the feature */
        int newPosition = getFeaturePosition(featureIcon);
        if (newPosition != -1) {
            COREControllerFactory.INSTANCE.getFeatureController().changePositionOfFeature(scene.getConcern(),
                    featureIcon.getParentFeatureView().getFeature(), featureIcon.getFeature(), newPosition);
        } else {
            // Move the feature back to its original position
            resetFeatureLocation(featureIcon, scene);
        }
    }

    /**
     * Return the given {@link FeatureView} to its default location.
     * 
     * @param feature - The feature to move
     * @param scene - The scene
     */
    private void resetFeatureLocation(FeatureView feature, AbstractConcernScene<?, ?> scene) {
        Vector3D initPosition = new Vector3D(feature.getXposition(), feature.getYposition());
        Vector3D currentPosition = feature.getPosition(TransformSpace.RELATIVE_TO_PARENT);
        Vector3D translation = initPosition.subtractLocal(currentPosition);
        feature.translate(translation);
        setNewTranslation(translation, feature);
    }

    /**
     * Return the current position of a feature related to its sibling on the screen.
     * This is used when a feature is dragged to find out were it should be placed in the end.
     *
     * @param feature - The {@link FeatureView}
     * @return the new position, or -1 if the position hasn't changed
     */
    private static int getFeaturePosition(FeatureView feature) {
        int initialPosition = feature.getCurrentPosition();
        // Get the siblings
        List<FeatureView> siblings = new ArrayList<FeatureView>();
        for (FeatureView fv : feature.getParentFeatureView().getChildrenFeatureViews()) {
            if (!fv.equals(feature) && !fv.isReuse()) {
                siblings.add(fv);
            } else if (fv.isReuse() && fv.getCurrentPosition() < feature.getCurrentPosition()) {
                // if there is a reuse, index should not take it in account
                initialPosition--;
            }
        }
        int newPosition = initialPosition;
        for (FeatureView sib : siblings) {
            if (feature.getCurrentPosition() < sib.getCurrentPosition()) {
                // The feature is originally located left of its sibling
                if (feature.getCenterPointGlobal().getX() > sib.getX() + sib.getWidth()) {
                    // the feature as now moved farther to the right of the sibling
                    newPosition++;
                }
            } else {
                // The feature is originally located right of its sibling
                if (feature.getCenterPointGlobal().getX() < sib.getX()) {
                    // the feature as now moved farther to the left of the sibling
                    newPosition--;
                }
            }
        }
        if (newPosition != initialPosition) {
            return newPosition;
        }
        return -1;
    }

    /**
     * Get the {@link FeatureView} lying under the given one. Used during drag events to check where the gesture ended.
     * 
     * @param list - The list of {@link FeatureView}s to examine
     * @param feature - The dragged {@link FeatureView}
     * @return the feature the gesture ended on, or null if none
     */
    private static FeatureView getUnderlyingFeature(Set<FeatureView> list, FeatureView feature) {
        list.remove(feature);
        for (FeatureView fv : list) {
            if (fv.containsPointGlobal(feature.getCenterPointGlobal())) {
                return fv;
            }
        }
        return null;
    }

    /**
     * Function called to associate an aspect as RealizedBy for the Feature.
     *
     * @param concern - The concern containing the feature.
     * @param feature - The feature to be realized by the aspect
     * @param aspect - The {@link Aspect} to associate to the feature
     */
    public static void setAspect(COREConcern concern, COREFeature feature, Aspect aspect) {
        COREControllerFactory.INSTANCE.getFeatureController().associateModel(concern, feature, aspect);
    }

    /**
     * Add a child feature to the given feature.
     *
     * @param scene - the current scene
     * @param parentFeature - the feature to add a child to
     * @param position - the position to add the child at
     * @param relationshipType - relationship for the child
     */
    @Override
    public void addChild(DisplayConcernEditScene scene, COREFeature parentFeature, int position,
            COREFeatureRelationshipType relationshipType) {
        
        // parentFeature.eContainer() corresponds to the Feature Model
        parentFeature.eContainer().eAdapters().add(new EContentAdapter() {
            @Override
            public void notifyChanged(Notification notification) {
                if (notification.getFeature() == CorePackage.Literals.CORE_FEATURE_MODEL__FEATURES) {
                    if (notification.getEventType() == Notification.ADD) {
                        COREFeature coreFeature = (COREFeature) notification.getNewValue();
                        scene.getFeatureView(coreFeature).showKeyboard();
                        scene.getFeatureView(coreFeature).clearNameField();
                        parentFeature.eContainer().eAdapters().remove(this);
                    }
                }
            }
        });

        final String childName = COREModelUtil.createUniqueNameFromElements(Strings.DEFAULT_FEATURE_NAME,
                new HashSet<COREFeature>(scene.getConcern().getFeatureModel().getFeatures()));

        FeatureController controller = COREControllerFactory.INSTANCE.getFeatureController();

        controller.addFeature(scene.getConcern(), position, parentFeature, childName, relationshipType);

    }

    @Override
    public void renameFeature(FeatureView view) {
        if (view.getIsRoot()) {
            return;
        }
        view.showKeyboard();
    }

    @Override
    public void deleteFeature(DisplayConcernEditScene scene, FeatureView view) {
        if (!view.getIsRoot() && !view.isReuse()) {
            // Get the feature and its children
            List<COREFeature> featuresDeleted = view.collectFeatures();
    
            // Check if there are any model reuse left
            for (COREFeature feature : featuresDeleted) {
                for (COREReuse reuse : feature.getReuses()) {
                    if (!COREModelUtil.getModelReuses(scene.getConcern(), reuse).isEmpty()) {
                        scene.displayPopup(Strings.POPUP_CANT_DELETE_FEATURE);
                        return;
                    }
                }
            }
    
            FeatureController controller = COREControllerFactory.INSTANCE.getFeatureController();
            controller.deleteFeature(scene.getConcern(), featuresDeleted);
        }
    }

    @Override
    public void hideFeature(DisplayConcernEditScene scene, FeatureView view) {
        scene.switchCollapse(view);
    }

    /**
     * Display the scene for a model.
     *
     * @param model - The {@link COREModel} to display
     */
    public static void showModel(COREModel model) {
        if (model instanceof Aspect) {
            RamApp.getActiveScene().setTransition(new SlideTransition(RamApp.getApplication(), 700, true));
            RamApp.getApplication().loadAspect((Aspect) model);
        }
    }

}
