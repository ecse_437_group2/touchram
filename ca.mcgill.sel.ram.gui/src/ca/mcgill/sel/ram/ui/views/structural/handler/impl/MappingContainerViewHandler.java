package ca.mcgill.sel.ram.ui.views.structural.handler.impl;

import ca.mcgill.sel.ram.AttributeMapping;
import ca.mcgill.sel.ram.ClassifierMapping;
import ca.mcgill.sel.ram.EnumLiteralMapping;
import ca.mcgill.sel.ram.EnumMapping;
import ca.mcgill.sel.ram.OperationMapping;
import ca.mcgill.sel.ram.controller.ControllerFactory;
import ca.mcgill.sel.ram.controller.MappingsController;
import ca.mcgill.sel.ram.ui.views.structural.handler.IMappingContainerViewHandler;

/**
 * Default handler for {@link ca.mcgill.sel.ram.ui.views.structural.ClassifierMappingContainerView}.
 * 
 * @author eyildirim
 */
public class MappingContainerViewHandler implements IMappingContainerViewHandler {
    
    @Override
    public void addAttributeMapping(ClassifierMapping classifierMapping) {
        if (classifierMapping.getFrom() != null && classifierMapping.getTo() != null) {
            
            MappingsController mappingsController = ControllerFactory.INSTANCE.getMappingsController();
            mappingsController.createAttributeMapping(classifierMapping);
        }
        
    }
    
    @Override
    public void addEnumLiteralMapping(EnumMapping enumMapping) {
        if (enumMapping.getFrom() != null && enumMapping.getTo() != null) {
            
            MappingsController mappingsController = ControllerFactory.INSTANCE.getMappingsController();
            mappingsController.createEnumLiteralMapping(enumMapping);
        }
        
    }
    
    @Override
    public void addOperationMapping(ClassifierMapping classifierMapping) {
        if (classifierMapping.getFrom() != null && classifierMapping.getTo() != null) {
            MappingsController mappingsController = ControllerFactory.INSTANCE.getMappingsController();
            mappingsController.createOperationMapping(classifierMapping);
        }
    }
    
    @Override
    public void deleteAttributeMapping(AttributeMapping attributeMapping) {
        MappingsController mappingsController = ControllerFactory.INSTANCE.getMappingsController();
        mappingsController.deleteMapping(attributeMapping);
    }
    
    @Override
    public void deleteEnumLiteralMapping(EnumLiteralMapping enumLiteralMapping) {
        MappingsController mappingsController = ControllerFactory.INSTANCE.getMappingsController();
        mappingsController.deleteMapping(enumLiteralMapping);
    }
    
    @Override
    public void deleteClassifierMapping(ClassifierMapping classifierMapping) {
        MappingsController mappingsController = ControllerFactory.INSTANCE.getMappingsController();
        mappingsController.deleteMapping(classifierMapping);
    }
    
    @Override
    public void deleteEnumMapping(EnumMapping enumMapping) {
        MappingsController mappingsController = ControllerFactory.INSTANCE.getMappingsController();
        mappingsController.deleteMapping(enumMapping);
    }

    @Override
    public void deleteOperationMapping(OperationMapping operationMapping) {
        MappingsController mappingsController = ControllerFactory.INSTANCE.getMappingsController();
        mappingsController.deleteMapping(operationMapping);
    }

}
