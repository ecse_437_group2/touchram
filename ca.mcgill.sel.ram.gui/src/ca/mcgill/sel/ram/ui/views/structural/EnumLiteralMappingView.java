package ca.mcgill.sel.ram.ui.views.structural;

import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.ram.EnumLiteralMapping;
import ca.mcgill.sel.ram.ui.components.RamButton;
import ca.mcgill.sel.ram.ui.components.RamImageComponent;
import ca.mcgill.sel.ram.ui.components.RamRoundedRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent.Alignment;
import ca.mcgill.sel.ram.ui.events.listeners.ActionListener;
import ca.mcgill.sel.ram.ui.layouts.HorizontalLayoutVerticallyCentered;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.Fonts;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.HandlerFactory;

/**
 * This view shows the attribute mapping such as AttributeA-->AttributeB along with a delete button.
 * 
 * @author joerg
 */
public class EnumLiteralMappingView extends RamRoundedRectangleComponent implements ActionListener {

    private static final String ACTION_ENUM_LITERAL_MAPPING_DELETE = "view.enumLiteralMapping.delete";

    private static final float ICON_SIZE = Fonts.FONTSIZE_COMPOSITION + 2;

    private EnumLiteralMapping myEnumLiteralMapping;

    /**
     * Button to delete Enum Literal Mapping.
     */
    private RamButton buttonEnumLiteralMappingDelete;

    /**
     * EnumLiteralMapping from element.
     */
    private TextView textEnumLiteralMappingFromElement;

    /**
     * EnumLiteralMapping to element.
     */
    private TextView textEnumLiteralMappingToElement;

    /**
     * Image for an arrow between mapping elements.
     */
    private RamImageComponent arrow;

    /**
     * Creates a new view for the given attribute mapping.
     * 
     * @param containerView - the {@link EnumMappingContainerView} which holds this {@link EnumLiteralMappingView}
     * @param enumLiteralMapping - the {@link EnumLiteralMapping} to create a view for
     */
    public EnumLiteralMappingView(EnumMappingContainerView containerView, EnumLiteralMapping enumLiteralMapping) {
        super(4);
        setNoStroke(true);
        setNoFill(true);
        setBuffers(0);
        myEnumLiteralMapping = enumLiteralMapping;

        // Add a button for deleting the enum literal mapping

        RamImageComponent deleteEnumLiteralMappingImage =
                new RamImageComponent(Icons.ICON_DELETE, Colors.ICON_DELETE_COLOR, ICON_SIZE, ICON_SIZE);
        buttonEnumLiteralMappingDelete = new RamButton(deleteEnumLiteralMappingImage);
        buttonEnumLiteralMappingDelete.setActionCommand(ACTION_ENUM_LITERAL_MAPPING_DELETE);
        buttonEnumLiteralMappingDelete.addActionListener(this);
        buttonEnumLiteralMappingDelete.setBuffers(0);
        buttonEnumLiteralMappingDelete.setBufferSize(Cardinal.WEST, 5);
        addChild(buttonEnumLiteralMappingDelete);

        RamTextComponent enumText = new RamTextComponent(Strings.LABEL_ENUM_LITERAL);
        enumText.setFont(Fonts.FONT_COMPOSITION);
        enumText.setBufferSize(Cardinal.SOUTH, 0);
        enumText.setBufferSize(Cardinal.EAST, 0);
        enumText.setBufferSize(Cardinal.WEST, Fonts.FONTSIZE_COMPOSITION);
        this.addChild(enumText);

        textEnumLiteralMappingFromElement = new TextView(myEnumLiteralMapping, CorePackage.Literals.CORE_LINK__FROM);
        textEnumLiteralMappingFromElement.setHandler(HandlerFactory.INSTANCE.getTextViewHandler());
        textEnumLiteralMappingFromElement.setFont(Fonts.FONT_COMPOSITION);
        textEnumLiteralMappingFromElement.setBufferSize(Cardinal.SOUTH, 0);
        textEnumLiteralMappingFromElement.setBufferSize(Cardinal.EAST, 0);
        textEnumLiteralMappingFromElement.setBufferSize(Cardinal.WEST, 0);
        textEnumLiteralMappingFromElement.setAlignment(Alignment.CENTER_ALIGN);
        textEnumLiteralMappingFromElement.setPlaceholderText(Strings.PH_SELECT_ENUM);
        textEnumLiteralMappingFromElement.setAutoMinimizes(true);
        this.addChild(textEnumLiteralMappingFromElement);

        arrow = new RamImageComponent(Icons.ICON_ARROW_RIGHT, Colors.ICON_ARROW_COLOR, ICON_SIZE, ICON_SIZE);
        arrow.setBufferSize(Cardinal.EAST, 1.0f);
        arrow.setBufferSize(Cardinal.WEST, 1.0f);
        this.addChild(arrow);

        textEnumLiteralMappingToElement = new TextView(myEnumLiteralMapping,
                CorePackage.Literals.CORE_LINK__TO);
        textEnumLiteralMappingToElement.setHandler(HandlerFactory.INSTANCE.getTextViewHandler());
        textEnumLiteralMappingToElement.setFont(Fonts.FONT_COMPOSITION);
        textEnumLiteralMappingToElement.setBufferSize(Cardinal.SOUTH, 0);
        textEnumLiteralMappingToElement.setBufferSize(Cardinal.EAST, 0);
        textEnumLiteralMappingToElement.setBufferSize(Cardinal.WEST, 0);
        textEnumLiteralMappingToElement.setAlignment(Alignment.CENTER_ALIGN);
        textEnumLiteralMappingToElement.setPlaceholderText(Strings.PH_SELECT_ENUM);
        textEnumLiteralMappingToElement.setAutoMinimizes(true);
        this.addChild(textEnumLiteralMappingToElement);

        setLayout(new HorizontalLayoutVerticallyCentered(Fonts.FONTSIZE_COMPOSITION / 5));
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        String actionCommand = event.getActionCommand();

        if (ACTION_ENUM_LITERAL_MAPPING_DELETE.equals(actionCommand)) {
            HandlerFactory.INSTANCE.getMappingContainerViewHandler().deleteEnumLiteralMapping(myEnumLiteralMapping);
        }

    }

    /**
     * Returns the Enumn Literal Mapping.
     * 
     * @return {@link EnumLiteralMapping}
     */
    public EnumLiteralMapping getEnumLiteralMapping() {
        return myEnumLiteralMapping;
    }

}
