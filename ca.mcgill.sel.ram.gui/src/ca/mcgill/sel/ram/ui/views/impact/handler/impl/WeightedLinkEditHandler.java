package ca.mcgill.sel.ram.ui.views.impact.handler.impl;

import java.util.Arrays;

import ca.mcgill.sel.core.COREWeightedLink;
import ca.mcgill.sel.core.controller.COREControllerFactory;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamListComponent;
import ca.mcgill.sel.ram.ui.components.RamSelectorComponent;
import ca.mcgill.sel.ram.ui.components.listeners.AbstractDefaultRamSelectorListener;
import ca.mcgill.sel.ram.ui.components.listeners.RamListListener;
import ca.mcgill.sel.ram.ui.scenes.AbstractImpactScene;
import ca.mcgill.sel.ram.ui.views.OptionSelectorView;

/**
 * The handler for {@link COREWeightedLink} in edit mode.
 *
 * @author Romain
 *
 */
public class WeightedLinkEditHandler implements RamListListener<COREWeightedLink> {

    /**
     * The options to display when tap-and-hold is performed.
     */
    private enum OPTION {
        DELETE_MAPPING
    }

    @Override
    public void elementSelected(RamListComponent<COREWeightedLink> list, COREWeightedLink element) {
        // TODO Auto-generated method stub

    }

    @Override
    public void elementDoubleClicked(RamListComponent<COREWeightedLink> list, COREWeightedLink element) {
        // TODO Auto-generated method stub

    }

    @Override
    public void elementHeld(RamListComponent<COREWeightedLink> list, final COREWeightedLink weightedLink) {
        OptionSelectorView<OPTION> selector = new OptionSelectorView<OPTION>(Arrays.asList(OPTION.values()));

        ((AbstractImpactScene) RamApp.getActiveScene()).addComponent(selector, list.getCenterPointGlobal());

        selector.registerListener(new AbstractDefaultRamSelectorListener<OPTION>() {
            @Override
            public void elementSelected(RamSelectorComponent<OPTION> selector, OPTION element) {
                switch (element) {
                    case DELETE_MAPPING:
                        deleteMappingElement(weightedLink);
                        break;
                }
            }
        });
    }

    /**
     * Delete a {@link COREWeightedLink} from the model.
     *
     * @param weightedLink the {@link COREWeightedLink} to delete.
     */
    private static void deleteMappingElement(COREWeightedLink weightedLink) {
        COREControllerFactory.INSTANCE.getFeatureImpactController().deleteWeightedLink(weightedLink);
    }

}