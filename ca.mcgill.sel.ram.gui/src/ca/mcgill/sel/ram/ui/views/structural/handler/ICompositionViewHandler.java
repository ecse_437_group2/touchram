package ca.mcgill.sel.ram.ui.views.structural.handler;

import ca.mcgill.sel.ram.ui.events.listeners.ITapListener;
import ca.mcgill.sel.ram.ui.views.structural.CompositionView;

/**
 * This interface can be implemented by an handler which handles the operations of a composition view such as
 * classifier mapping or enum mapping adding/removing.
 * 
 * @author eyildirim
 */
public interface ICompositionViewHandler extends ITapListener {
    
    /**
     * This function is used to add a classifier mapping view.
     * 
     * @param compositionView the composition
     */
    void addClassifierMapping(CompositionView compositionView);
    
    /**
     * This function is used to add an enum mapping view.
     * 
     * @param compositionView the composition
     */
    void addEnumMapping(CompositionView compositionView);
    
   /**
     * Hides all mapping details.
     * 
     * @param compositionView the composition view
     */
    void hideMappingDetails(CompositionView compositionView);
    
    /**
     * Loads the external Aspect in full mode.
     * 
     * @param myCompositionView the composition
     */
    void showExternalAspectOfComposition(CompositionView myCompositionView);
    
    /**
     * Shows all classifier mapping details.
     * 
     * @param compositionView the composition view
     */
    void showMappingDetails(CompositionView compositionView);
    
    /**
     * Switches to the split view for mapping specifications.
     * 
     * @param myCompositionView the composition
     */
    void switchToSplitView(CompositionView myCompositionView);
    
}
