package ca.mcgill.sel.ram.ui.views.structural.handler;

import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.ram.Aspect;

/**
 * This interface can be implemented by a handler that can handle events for a
 * {@link ca.mcgill.sel.ram.ui.views.structural.CompositionContainerView} which is showing the list of
 * model compositions in a model.
 * 
 * @author eyildirim
 */
public interface ICompositionContainerViewHandler {

    /**
     * Deletes a model composition from its model.
     * 
     * @param modelComposition the {@link COREModelComposition} to delete
     */
    void deleteModelComposition(COREModelComposition modelComposition);

    /**
     * Loads the aspect browser.
     * 
     * @param aspect the current aspect
     */
    void loadBrowser(Aspect aspect);

}
