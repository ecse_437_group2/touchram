package ca.mcgill.sel.ram.ui.events;

import org.mt4j.AbstractMTApplication;
import org.mt4j.input.inputData.AbstractCursorInputEvt;
import org.mt4j.input.inputData.ActiveCursorPool;
import org.mt4j.input.inputData.InputCursor;
import org.mt4j.input.inputData.MTFingerInputEvt;
import org.mt4j.input.inputSources.AbstractInputSource;

import processing.core.PApplet;
import processing.event.KeyEvent;

/**
 * A general input source for the keyboard.
 * 
 * @see org.mt4j.input.inputSources.KeyboardInputSource
 * @author vbonnet
 */
public class RamKeyboardInputSource extends AbstractInputSource {
    
    private long lastUsedKeybID;
    
    private int locationX;
    private int locationY;
    
    private final PApplet applet;
//    private int fingerDownKeyCode;
    
    private boolean fingerDown;
    
    /**
     * Instantiates a new keyboard input source.
     * 
     * @param pa
     *            the pa
     */
    public RamKeyboardInputSource(final AbstractMTApplication pa) {
        super(pa);
        applet = pa;
        applet.registerMethod("keyEvent", this);
        
        locationX = 0;
        locationY = 0;
        
//        fingerDownKeyCode = KeyEvent.ALT;
        fingerDown = false;
    }
    
    /**
     * Handles the pressing of the finger down key.
     * 
     * @param event the key event
     */
    // CHECKSTYLE:IGNORE SuppressWarnings: This is hilarious :)
    @SuppressWarnings("unused")
    private void fingerDown(final KeyEvent event) {
        if (!fingerDown) {
            locationX = applet.mouseX;
            locationY = applet.mouseY;
            
            System.out.println("down! " + locationX + " " + locationY);
            
            final InputCursor m = new InputCursor();
            final MTFingerInputEvt touchEvt = new MTFingerInputEvt(this, locationX, locationY,
                    AbstractCursorInputEvt.INPUT_STARTED, m);
            
            lastUsedKeybID = m.getId();
            ActiveCursorPool.getInstance().putActiveCursor(lastUsedKeybID, m);
            
            // FIRE
            enqueueInputEvent(touchEvt);
        } else {
            final InputCursor m = ActiveCursorPool.getInstance().getActiveCursorByID(lastUsedKeybID);
            
            final MTFingerInputEvt te = new MTFingerInputEvt(this, locationX, locationY,
                    AbstractCursorInputEvt.INPUT_UPDATED, m);
            enqueueInputEvent(te);
        }
        fingerDown = true;
    }
    
    /**
     * Handles the release of the finger down key.
     * 
     * @param event the key event
     */
    // CHECKSTYLE:IGNORE SuppressWarnings: This is hilarious :)
    @SuppressWarnings("unused")
    private void fingerUp(KeyEvent event) {
        fingerDown = false;
        
        final InputCursor m = ActiveCursorPool.getInstance().getActiveCursorByID(lastUsedKeybID);
        final MTFingerInputEvt te = new MTFingerInputEvt(this, locationX, locationY,
                AbstractCursorInputEvt.INPUT_ENDED, m);
        
        enqueueInputEvent(te);
        
        ActiveCursorPool.getInstance().removeCursor(lastUsedKeybID);
        
    }
    
    /**
     * Key event.
     * 
     * @param event the processing key event
     */
    public void keyEvent(KeyEvent event) {
        
//        final int eventID = e.getAction();
//        final int keyCode = e.getKeyCode();
//        
//        if (eventID == KeyEvent.KEY_PRESSED) {
//            if (keyCode == fingerDownKeyCode) {
//                // fingerDown(event);
//            }
//        } else if (eventID == KeyEvent.KEY_RELEASED) {
//            if (e.getKeyCode() == fingerDownKeyCode) {
//                // fingerUp(event);
//            }
//        }
    }
    
}
