package ca.mcgill.sel.ram.weaver.structuralview;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.util.EcoreUtil;

import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.COREModelElementComposition;
import ca.mcgill.sel.core.COREModelExtension;
import ca.mcgill.sel.core.COREModelReuse;
import ca.mcgill.sel.core.weaver.util.WeavingInformation;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.Association;
import ca.mcgill.sel.ram.AssociationEnd;
import ca.mcgill.sel.ram.Classifier;
import ca.mcgill.sel.ram.ClassifierMapping;
import ca.mcgill.sel.ram.Operation;
import ca.mcgill.sel.ram.Parameter;
import ca.mcgill.sel.ram.util.Constants;
import ca.mcgill.sel.ram.util.RAMModelUtil;
import ca.mcgill.sel.ram.weaver.util.RAMReferenceUpdater;

/**
 * This class is in charge of handling the post processing phase after
 * weaving has occurred.
 *
 * @author walabe
 *
 */
public final class PostProcessor {

    /**
     * Default private constructor.
     */
    private PostProcessor() {

    }

    /**
     * Main method of this class. Will perform all necessary updates.
     *
     * @param base The aspect that has been woven into
     * @param externalAspect The aspect that was woven
     * @param weavingInformation The data structure containing all mapping information
     * @param composition The COREModelComposition woven into the base
     */
    public static void performUpdates(Aspect base, Aspect externalAspect, WeavingInformation weavingInformation,
            COREModelComposition composition) {
        updateCompositions(base, externalAspect, weavingInformation);

        updateReferences(base, weavingInformation);

        ArrayList<Classifier> duplicates = getDuplicateClasses(base, weavingInformation);

        mergeDuplicates(weavingInformation, duplicates);

        if (composition instanceof COREModelReuse) {
            processAssociations(base, (COREModelReuse) composition, weavingInformation);
        }
        
        updateReferences(base, weavingInformation);

        removeDuplicates(base, duplicates);
    }

    /**
     * This method will update the compositions by iterating through
     * all compositions in the external aspect and creating copies
     * and adding them to the woven aspect.
     *
     * @param base The aspect that has been woven into
     * @param externalAspect The aspect that was woven
     * @param weavingInformation The data structure containing all mapping information
     */
    private static void updateCompositions(Aspect base, Aspect externalAspect,
            WeavingInformation weavingInformation) {
        
        // If the external aspect has compositions (which could be the case when weaving top down),
        // it is necessary to copy the compositions to the base aspect and update the references.
        // Updating is done in a separate loop for all compositions (see below).
        for (COREModelExtension externalExtension : externalAspect.getModelExtensions()) {
            COREModelExtension extensionCopy = EcoreUtil.copy(externalExtension);
            base.getModelExtensions().add(extensionCopy);
        }
        for (COREModelReuse externalModelReuses : externalAspect.getModelReuses()) {
            COREModelReuse reuseCopy = EcoreUtil.copy(externalModelReuses);
            base.getModelReuses().add(reuseCopy);
        }

        // Update all instantiations. Do it for all, because an existing instantiation in the higher-level aspect
        // could contain a mapping to an element in another aspect, which could have been woven.
        // Then the mapping needs to be updated.
        // See issue #58.
        // This is now taken care of by the ReferenceUpdater (second step of the post processor).
    }

    /**
     * This method will perform the updating of references after weaving.
     *
     * @param aspect The woven aspect
     * @param weavingInformation The data structure containing all mapping information
     */
    private static void updateReferences(Aspect aspect, WeavingInformation weavingInformation) {
        RAMReferenceUpdater updater = RAMReferenceUpdater.getInstance();

        // Update the structural view.
        updater.update(aspect.getStructuralView(), weavingInformation);
        // Update all instantiations.
        updater.update(aspect.getModelExtensions(), weavingInformation);
        // Update the model reuses.
        // for (COREModelReuse modelReuse : aspect.getModelReuses()) {
        // for (COREModelCompositionSpecification<?> comp : modelReuse.getCompositions()) {
        // updater.update((Instantiation) comp, weavingInformation);
        // }
        // }
    }

    /**
     * This method will retrieve any duplicate classifiers as a result of weaving.
     * Must be called after updateReferences
     *
     * @param aspect The woven aspect
     * @param weavingInformation The data structure containing all mapping information
     * @return the list of duplicates
     */
    private static ArrayList<Classifier> getDuplicateClasses(Aspect aspect, WeavingInformation weavingInformation) {
        ArrayList<Classifier> duplicates = new ArrayList<Classifier>();
        for (Classifier currentClass : aspect.getStructuralView().getClasses()) {
            if (!duplicates.contains(currentClass)) {
                getDuplicates(aspect, currentClass, weavingInformation, duplicates);
            }
        }

        return duplicates;

    }

    /**
     * This class will retrieve all duplicate classifiers that match classToCheck.
     *
     * @param aspect The aspect we are searching
     * @param classToCheck The class to check for duplicates for
     * @param weavingInformation The data structure containing all mapping information
     * @param duplicates the data-structure to store duplicates in for removal later
     */
    private static void getDuplicates(Aspect aspect, Classifier classToCheck, WeavingInformation weavingInformation,
            ArrayList<Classifier> duplicates) {
        for (Classifier currentClass : aspect.getStructuralView().getClasses()) {
            // make sure not comparing a class with it self
            if (currentClass != classToCheck) {
                boolean match = RAMModelUtil.isNameEqual(classToCheck, currentClass);

                if (match) {
                    // if the names and the type parameters match then flag this class as a duplicate
                    // by adding it to the duplicates data structure and create a mapping
                    // between this duplicate class and the class we are comparing it to i.e the one
                    // we will be keeping in the end.
                    duplicates.add(currentClass);
                    weavingInformation.add(currentClass, classToCheck);
                    // now check to see if there are any mappings in which currentClass is the toElement
                    // for example if A -> currentClass and we just added a new mapping
                    // currentClass -> classToCheck. we need to create another new mapping
                    // A -> classToCheck and delete old mapping A -> currentClass in order for updating
                    // of references to be correct
                    List<Classifier> temp = weavingInformation.getFromElements(currentClass);
                    for (Classifier classifier : temp) {
                        weavingInformation.add(classifier, classToCheck);
                        weavingInformation.getToElements(classifier).remove(currentClass);
                    }

                }
            }
        }

    }

    /**
     * This method will merge all the duplicates into the element they are mapped to.
     *
     * @param weavingInformation The data structure containing all mapping information.
     * @param duplicates The datastructure containing the duplicate classifiers.
     */
    private static void mergeDuplicates(WeavingInformation weavingInformation, ArrayList<Classifier> duplicates) {
        for (Classifier duplicateClass : duplicates) {
            Classifier classToCheck = weavingInformation.getFirstToElement(duplicateClass);
            for (Operation currentOp : duplicateClass.getOperations()) {
                Operation opFromHigherLevel =
                        OperationsWeaver.getExistingOperation(classToCheck, currentOp, weavingInformation);
                if (opFromHigherLevel != null) {
                    weavingInformation.add(currentOp, opFromHigherLevel);
                } else {
                    opFromHigherLevel = EcoreUtil.copy(currentOp);
                    classToCheck.getOperations().add(opFromHigherLevel);
                    weavingInformation.add(currentOp, opFromHigherLevel);
                }
                // now check to see if there are any mappings in which currentOp is the toElement
                // for example if A -> currentOp and we just added a new mapping
                // currentOp -> opFromHigherLevel. we need to create another new mapping
                // A -> opFromHigherLevel and delete old mapping A -> currentOp in order for updating
                // of references to be correct
                List<Operation> temp = weavingInformation.getFromElements(currentOp);
                for (Operation op : temp) {
                    weavingInformation.add(op, opFromHigherLevel);
                    weavingInformation.getToElements(op).remove(currentOp);
                }

                createParameterWeavingInformation(currentOp, opFromHigherLevel, weavingInformation);
            }
        }
    }

    /**
     * Adds parameter mapping info to weaving information for given operations.
     *
     * @param fromOp The operation mapped to toOp
     * @param toOp The operation that fromOp is mapped to
     * @param weavingInformation The data structure containing all mapping information
     */
    private static void createParameterWeavingInformation(Operation fromOp, Operation toOp,
            WeavingInformation weavingInformation) {
        for (int index = 0; index < fromOp.getParameters().size(); index++) {
            Parameter oldParameter = fromOp.getParameters().get(index);
            Parameter newParameter = toOp.getParameters().get(index);
            weavingInformation.add(oldParameter, newParameter);
            // now check to see if there are any mappings in which oldParameter is the toElement
            // for example if A -> oldParameter and we just added a new mapping
            // oldParameter -> newParameter. we need to create another new mapping
            // A -> newParameter and delete old mapping A -> oldParameter in order for updating
            // of references to be correct
            List<Parameter> temp = weavingInformation.getFromElements(oldParameter);
            for (Parameter param : temp) {
                weavingInformation.add(param, newParameter);
                weavingInformation.getToElements(param).remove(oldParameter);
            }
        }
    }

    /**
     * This method will remove the duplicates from the aspect.
     *
     * @param aspect The aspect to remove the duplicates from
     * @param duplicates The list of duplicates to be removed
     */
    private static void removeDuplicates(Aspect aspect, ArrayList<Classifier> duplicates) {

        for (Classifier classifier : duplicates) {
            aspect.getStructuralView().getClasses().remove(classifier);
        }

    }

    /**
     * Processes associations if the given model reuse is of the Association Concern.
     *
     * If association has upper bound of 1:
     * - Delete the association from the concern that does not have a feature selection.
     * - Map association ends.
     *
     * If the upper bound is greater than 1:
     * - Navigable in one direction: Delete the association from Data --> Associated
     * - Navigable in both directions: Set navigability of the association end to false
     * - Rename the association going to the Collection with the name of the association that was going
     * to Associated.
     *
     * @param aspect The aspect that contains the associations to be processed.
     * @param modelReuse the model reuse woven into the aspect
     * @param weavingInformation The weaving information to map association ends
     */
    private static void processAssociations(Aspect aspect, COREModelReuse modelReuse,
            WeavingInformation weavingInformation) {
        // Only process if model reuse is of the association concern
        if (modelReuse == null
                || !Constants.ASSOCIATION_CONCERN_NAME.equals(modelReuse.getReuse().getReusedConcern().getName())) {
            return;
        }

        ClassifierMapping dataClassifierMapping = null;
        Association associationToRemove = null;

        // Find the class mapped to 'Data' and get the association end with the matching feature selection
        for (COREModelElementComposition<?> mec : modelReuse.getCompositions()) {
            ClassifierMapping classifierMapping = (ClassifierMapping) mec;
            
            if (Constants.DATA_CLASS_NAME.equals(classifierMapping.getFrom().getName())) {
                dataClassifierMapping = classifierMapping;
            }
        }

        for (AssociationEnd associationEnd : dataClassifierMapping.getTo().getAssociationEnds()) {
            if (associationEnd.getFeatureSelection() == null) {
                if (Constants.ASSOCIATION_END_NAME_ASSOCIATED.equals(associationEnd.getName())) {
                    // Remove Data --> (myAssociated) Associated
                    associationToRemove = associationEnd.getAssoc();
                }
            } else if (associationEnd.getFeatureSelection().getReuse() == modelReuse.getReuse()) {
                if (associationEnd.getUpperBound() == 1) {
                    // Map association end in the concern to the one in the aspect
                    AssociationEnd toMap = dataClassifierMapping.getFrom().getAssociationEnds().get(0);
                    weavingInformation.getToElements(toMap).remove(0);
                    weavingInformation.add(toMap, associationEnd);
                } else {
                    if (associationEnd.getOppositeEnd().isNavigable()) {
                        associationEnd.setFeatureSelection(null);
                        associationEnd.setNavigable(false);
                    } else {
                        // Remove Data --> (endName) Associated
                        associationToRemove = associationEnd.getAssoc();
                        
                        AssociationEnd toMap = dataClassifierMapping.getFrom().getAssociationEnds().get(0);
                        weavingInformation.add(associationEnd, toMap);
                    }

                    // Rename Data --> (collection) Collection to the endName.
                    for (Association association : aspect.getStructuralView().getAssociations()) {
                        for (AssociationEnd end : association.getEnds()) {
                            if (Constants.ASSOCIATION_END_NAME_COLLECTION.equals(end.getName())
                                    && Constants.ASSOCIATION_NAME_DATA_COLLECTION.equals(end.getAssoc().getName())) {
                                // Find Collection --> (elements) Associated end to update the multiplicity
                                end.setName(associationEnd.getName());
                            } else if (Constants.ASSOCIATION_END_NAME_ELEMENTS.equals(end.getName())) {
                                // Find Collection --> (elements) Associated end to update the multiplicity
                                end.setUpperBound(associationEnd.getUpperBound());
                                end.setLowerBound(associationEnd.getLowerBound());
                            }
                        }
                    }
                }

            }
        }

        // Delete the association between Data and Associated
        if (associationToRemove != null) {
            for (AssociationEnd associationEnd : associationToRemove.getEnds()) {
                associationEnd.getClassifier().getAssociationEnds().remove(associationEnd);
            }
            aspect.getStructuralView().getAssociations().remove(associationToRemove);
        }
    }

}
